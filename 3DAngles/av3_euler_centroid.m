function [euler_centroid alpha] = av3_euler_centroid(euler_in)
%AV3_EULER_CENTROID computes mean Euler angles from set of orientations.
%
%      INPUT
%       euler_in      3 x N list of Euler angles
%
%
%      OUTPUT
%       euler_centroid    Centroids backconverted in Eulerians
%
%      Example:
%
%       euler_centroid = av3_euler_centroid(euler_in);
%
%
%    See Also
%     KMEANS, AV3_PEAKNEIGHBOUR_EULER, ATAN2
%
%    07/2010 SP, FF
%
%   Copyright (c) 2010

%% convert to quaternions (according to R.Hegerl, 10.5.07)

quaternions = av3_eul2quat(euler_in);

q_c = mean(quaternions, 1);

%% re-normalize
q_centroid = [ ];
for ieul = 1:size(euler_in,1) / 3
    len = sqrt( (q_c(:,ieul * 4-3).^2 + q_c(:,ieul *4-2).^2 + q_c(:,ieul * 4-1).^2 + q_c(:,ieul * 4).^2));
    a = 1./len;
    a = [a a a a];
    q_centroid = [q_centroid  q_c(ieul * 4-3 : ieul * 4).* a ];
    
    clear a
end;
alpha = 180/pi*acos(len);
disp(['Semi-angle of angular distribution: ' num2str(alpha)]);

%% back-trans to Euler angles
euler_centroid = [ ];
for ieul = 1:size(euler_in,1) / 3
    q0 = q_centroid(:,ieul * 4-3  );
    qx = q_centroid(:,ieul * 4-2  );
    qy = q_centroid(:,ieul * 4-1  );
    qz = q_centroid(:,ieul * 4      );
    
    the = atan2( 2 .* sqrt((qx.^2 +qy.^2)  .* (q0.^2 + qz .^2) ) , (q0.^2- qx.^2- qy.^2+ qz.^2));
    psi = atan2( 2 .* (qx .*qz + q0 .*qy) ./ sin(the) , (-2 .*(qy .*qz - q0 .*qx) ./sin(the)));
    phi = atan2( 2 .* (qx .*qz - q0 .*qy) ./ sin(the) , ( 2 .*(qy .* qz + q0 .*qx) ./sin(the)));
    euler_centroid = [phi psi the] .* 180 ./pi;
    euler_centroid(:,1:2) = mod(euler_centroid(:,1:2), 360);
    euler_centroid(:,3) = mod(euler_centroid(:, 3), 180);
end


