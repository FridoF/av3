function [vol, motl] = av3_replicate_model_copy(euler_ang, shift, nreplicates, ref);
%   calculate replicates from neighbour euler angles and shift
%
%   [vol, motl] = av3_replicate_model(euler_ang, shift, nreplicates, ref);
%
%   INPUT
%    euler_ang
%    shift          shift in pixel (!)
%    nreplicates
%    ref            reference for replicates - take care: density should be
%                   positive
%   OUTPUT
%    vol            replicate volume
%    motl           motif list for replicates
%
%   07/12/2010 FF, SP
motl = zeros(20,nreplicates+1);

%% prepare 1st copy
eulertmp = [0 0 0];
shifttmp = [0,0,0];
%motl(17:19,1)=0;
dim1 = size(ref,1);
dim = (nreplicates+1)*dim1;
vol = zeros(dim,dim,dim);
%motl(11:13,1) = 0;
orig = ones(3,1)*(dim/2 - dim1/2 +1);
vol = tom_paste2(vol, ref, orig, 'max');
ref = vol;

%INSERT ref = tom_emread('average_small.em');ref=ref.Value;

% tom_dspcub(ref)
% tom_dspcub(refrot)
% nmpix=0.47;

%% paste replicates into vol
for ii = 2:nreplicates+1
    shifttmp = shifttmp + tom_pointrotate(shift, eulertmp(1), ...
        eulertmp(2), eulertmp(3));
    [phi, psi, the] = av3_mergerotations(eulertmp(1), eulertmp(2), eulertmp(3),...          %!!!!! eulertmp not euler_ang
        euler_ang(1), euler_ang(2), euler_ang(3));
    eulertmp = [phi, psi, the];
    refrot = tom_shift(tom_rotate(ref,eulertmp), shifttmp);
    motl(11:13,ii) = shifttmp;
    motl(17:19,ii) = eulertmp;
    vol = tom_paste2(vol, refrot, [0,0,0], 'max');
end