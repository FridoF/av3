function [n,x]=av3_imagehist(im,sampling)
% av3_imagehist computes histogram of an image
%
%PARAMETERS
%
%  INPUT
%   im          image
%   sampling    sampling, either number of channels or specific interval as
%               array
%

dims = size(im);
[n,x]=hist(reshape(im,[dims(1)*dims(2), 1]),sampling);
plot(x,n);
