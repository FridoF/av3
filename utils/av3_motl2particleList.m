function av3_motl2particleList(motl, plname, prefix_particlefiles)
% write motl as particlelist for pytom
%
% av3_motl2particleList(motl, plname)
%   
% INPUT
%   motl                    motif list
%   plname                  filename of particlelist
%   prefix_particlefiles    prefix of particle subtomograms. For example 
%                           'MyFolder/particle_'
%
% SEE ALSO
%   av3_readParticleList, av3_average_exact
%
%   UTRECHT University
%
% 27/03/2019 FF

%diary(plname);
fid = fopen(plname, 'w');
%diary on
fprintf(fid, '<ParticleList Path="particle_files_bin1/">\n');
%disp('<ParticleList Path="particle_files_bin1/">');
for i=1:size(motl,2)
    fprintf(fid, ['  <Particle Filename="', prefix_particlefiles, num2str(i), '.em">\n']);
    fprintf(fid, ['    <Rotation X="' num2str(motl(19,i)) '" Paradigm="ZXZ" Z1="' num2str(motl(17,i)) '" Z2="' num2str(motl(18,i)) '"/>\n']);
    fprintf(fid, ['    <Shift Y="' num2str(motl(12,i)) '" X="' num2str(motl(11,i)) '" Z="' num2str(motl(13,i)) '"/>\n']);
    fprintf(fid, ['    <PickPosition Origin="" Y="' num2str(motl(9,i)) '" Z="' num2str(motl(10,i)) '" X="' num2str(motl(8,i)) '"/>\n']);
    fprintf(fid, ['    <Score Type="xcfScore" Value="' num2str(motl(1,i)) '">\n']);
    fprintf(fid, '       <PeakPrior Smooth="-1.0" Radius="0.0" Filename=""/>\n');
    fprintf(fid, '     </Score>\n');
    fprintf(fid, '    <WedgeInfo Smooth="0.0" Angle1="30.0" CutoffRadius="0.0" TiltAxis="custom" Angle2="30.0">\n');
    fprintf(fid, '      <Rotation Z1="0.0" Z2="0.0" X="0.0"/>\n');
    fprintf(fid, '    </WedgeInfo>\n');
    fprintf(fid, ['    <Class Name="', num2str(motl(20,i)), '"/>\n']);
    fprintf(fid, '  </Particle>\n');
end
fprintf(fid, '</ParticleList>');
fclose(fid);
