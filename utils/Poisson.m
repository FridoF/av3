function E = Poisson(a)
% error of data and Poisson distribution
%
%   E = Poisson(a)
%
%   fit to
%       Y = a(1).*exp(-((x-a(2)).^2)/(2*(a(3).^2)));
%   start values for two symmetrical Gauss functions
%   
%   last change: 
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
%   12/20/04 FF - fixed sigma bug

global xData yData dy
x=xData(:); y=yData(:); 
Y = a(1).*exp(-((x-a(2)).^2)/(2*(a(3).^2)));
E = sum(((y-Y).^2)./dy.^2);
