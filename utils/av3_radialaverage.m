function radav = av3_radialaverage(invol)
%   calculate rotationally averaged volume
%
%   radav = av3_radialaverage(invol)
%
%   INPUT
%   invol     volume
%
%   OUTPUT
%   radav     radial average
%
%   SEE ALSO
%
%   AV3_PR
%
%   FF 08/16/10
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

cent = size(invol);
cent(1) = floor( cent(1) /2 + 1);
cent(2) = floor( cent(2) /2 + 1);
if cent(3) > 1
    ndim = 3;
else
    ndim = 2;
end;
if ndim > 2
    cent(3) = floor( cent(3) /2 + 1);
    radav = zeros(floor(sqrt(3)*size(invol,1)),1);
else
    radav = zeros(floor(sqrt(2)*size(invol,1)),1);
end;
for ix=1:size(invol,1)
    for iy=1:size(invol,2)
        for iz=1:size(invol,3)
            fhist = sqrt( (ix-cent(1))^2 + (iy-cent(2))^2 + (iz-cent(3))^2 ) +1;
            ilow = floor(fhist);
            ihigh = ceil(fhist);
            d = fhist - ilow;
            % linear interpolation
            if d > 0
                radav(ilow) = radav(ilow) + (1-d) * invol(ix,iy,iz);
                radav(ihigh) = radav(ihigh) + d * invol(ix,iy,iz);
            else
                radav(ilow) = radav(ilow) + invol(ix,iy,iz);
            end;
        end;
    end;
end;
