function [phi, psi, the] = av3_mergerotations( phi_1, psi_1, the_1, phi_2, psi_2, the_2)
%AV3_MERGEROTATIONS merges Euler angles from two rotations.
%
%   [phi, psi, the] =
%   av3_mergerotations(phi_1,psi_1,the_1,phi_2,psi_2,the_2)
%
%   two rotations around euler angles are combined into one. the new set of
%   Euler angles is returned.
%
%       r' = M(phi_2,psi_2,the_2) * M(phi_1,psi_1,the_1) * r
%          = M(phi, psi, the) * r
%
%PARAMETERS
%
%  INPUT
%   phi_1               Euler angle - in deg.
%   psi_1               Euler angle - in deg.
%   the_1               Euler angle - in deg.
%   phi_2               Euler angle - in deg.
%   psi_2               Euler angle - in deg.
%   the_2               Euler angle - in deg.
%  
%  OUTPUT
%   [phi, psi, the]     Euler angles of single rotation                   ...
%
%EXAMPLE
%   
%
%REFERENCES
%
%SEE ALSO
%   TOM_ROTATE3D, TOM_ROTATE2D
%
%   created by FF 07/2010
%
%   Nickell et al., 'TOM software toolbox: acquisition and analysis for electron tomography',
%   Journal of Structural Biology, 149 (2005), 227-234.
%
%   Copyright (c) 2004-2010
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute of Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom


phi_1 = phi_1/180*pi;
psi_1 = psi_1/180*pi;
the_1 = the_1/180*pi;
matr_1 = [cos(psi_1) -sin(psi_1) 0; sin(psi_1) cos(psi_1) 0;0 0 1];
matr_1 = matr_1*[1 0 0 ; 0 cos(the_1) -sin(the_1); 0 sin(the_1) cos(the_1)];
matr_1 = matr_1*[cos(phi_1) -sin(phi_1) 0; sin(phi_1) cos(phi_1) 0;0 0 1];

phi_2 = phi_2/180*pi;
psi_2 = psi_2/180*pi;
the_2 = the_2/180*pi;
matr_2 = [cos(psi_2) -sin(psi_2) 0; sin(psi_2) cos(psi_2) 0;0 0 1];
matr_2 = matr_2*[1 0 0 ; 0 cos(the_2) -sin(the_2); 0 sin(the_2) cos(the_2)];
matr_2 = matr_2*[cos(phi_2) -sin(phi_2) 0; sin(phi_2) cos(phi_2) 0;0 0 1];

matr = matr_2 * matr_1;

[phi psi the] = av3_matrix2angle(matr);