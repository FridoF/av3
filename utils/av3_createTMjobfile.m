function av3_createTMjobfile(jobname, tomogram, template, mask, wedge, angles)
%   creates jobfile for template matching by pyTom (localization.py)
%
% av3_createTMjobfile(jobname, tomogram, template, mask, wedge, angles)
%
% INPUT
%  jobname         name of jobfile (including path)
%  tomogram        name of tomogram to perform TM on (including path)
%  template        template file (including path)
%  mask            maskfile (including path)
%  wedge           opening angle of missing wedge, i.e. 90deg - the highest
%                  tiltangle
%  angles          path to 'angles.em' file (i.e., xxx/angles.em
%
% OUTPUT
%  <jobname>       input for pytom template matching (localization.py)
%
% SEE ALSO
%  av3_createFRMjobfile
%
%   UTRECHT University
%
% 22/10/2019 RE

fid = fopen(jobname, 'w');

fprintf(fid, '%s\n', ['<JobDescription>']);
fprintf(fid, '%s\n', [' <Volume Filename="' tomogram '"/>']);
fprintf(fid, '%s\n', [' <Reference Weighting="" File="' template '"/>']);
fprintf(fid, '%s\n', [' <Mask Filename="' mask '" Binning="1" isSphere="True"/>']);
fprintf(fid, '%s\n', [' <WedgeInfo Angle="' num2str(wedge) '" CutoffRadius="0.0" TiltAxis="custom">']);
fprintf(fid, '%s\n', ['  <Rotation Z1="0.0" Z2="0.0" X="0.0"/>']);
fprintf(fid, '%s\n', ['</WedgeInfo>']);
fprintf(fid, '%s\n', ['<Angles Type="FromEMFile" File="' angles '">']);
fprintf(fid, '%s\n', ['  <RefinementParameters Shells="6.0" Increment="10.0"/>']);
fprintf(fid, '%s\n', ['  <OldRotations/>']);
fprintf(fid, '%s\n', ['</Angles>']);
fprintf(fid, '%s\n', ['<Score Type="FLCFScore" Value="-100000000">']);
fprintf(fid, '%s\n', ['  <DistanceFunction Deviation="0.0" Mean="0.0" Filename=""/>']);
fprintf(fid, '%s\n', [' </Score>']);
fprintf(fid, '%s\n', ['</JobDescription>']);

fclose(fid);
