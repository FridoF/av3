function E = GauGau(a)
% GAUGAU error of data and Double Gaussian
%
%   E = GauGau(a)
%   fit to
%       Y = a(1)/sqrt(2*pi).*exp(-((x-a(2))/a(3)).^2) + a(4)/sqrt(2*pi).*exp(-((x-a(5))/a(6)).^2);
%   start values for two Gauss functions
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

global xData yData dy
N = size(xData,1);
sqrt2 = sqrt(2);
x=xData(:); y=yData(:); 
Y = 1/(sqrt(2*pi))* ( a(1)/a(3).*exp(-((x-a(2))/(sqrt2*a(3))).^2) + a(4)/a(6).*exp(-((x-a(5))/(sqrt2*a(6))).^2));
E = sum(((y-Y).^2)./dy.^2) / N;
