function im = av3_rotaverage(im)
% AV3_ROTAVERAGE rotationally averages 3D volume
%
%   im = av3_rotaverage(im)
%
%   AV3_ROTAVERAGE rotationally averages 3D volume IM 
%
% last change 03/31/05 FF - updated docu
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
spim = tom_cart2sph(im);
rspim = sum(sum(spim,2),3)/size(spim,2)/size(spim,3);
for indphi =1:size(spim,2)
    for indthe =1:size(spim,3)
        spim(:,indphi,indthe)=rspim;
    end;
end;
im = tom_sph2cart(spim);
