function vol = av3_remove_gradient(vol, radius)
%   vol = av3_rec_corrali(fname, append, shifts)
%
%   INPUT
%   fname        filename (e.g. 'ves1')
%   append       appendix of em files (e.g., '.em')
%
%   OUTPUT
%   vol          reconstructed volume (1x binned)
%
%   FF 08/18/09
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%


mask = squeeze(tom_spheremask(...
    ones(size(vol,1),size(vol,2),size(vol,3),'single'), ...
    radius, radius/10));
npix = sum(sum(sum(mask)));

%subtract mean from vol 
fmask   = conj(tom_fourier(mask));
fvol    = tom_fourier(vol);
meanvol = tom_ifourier(fvol.*fmask)/npix;

if size(class(vol),2) == 5
    if class(vol)=='int16'
        vol = vol - int16(meanvol);
    else
        vol = vol - meanvol;
    end;
else
    vol = vol - meanvol;
end;



