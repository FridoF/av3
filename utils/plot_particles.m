% Script to paste a reference volume into a box at coordinates described in a motl file
% Use in Matlab

motlfilename='motlfile.em';
volumefilename='volume.mrc';
pastedvolumefilename='pastedvolumes.em';
x=464;%dimensions of tomogram 
y=464;
z=464;

motl=tom_emread(motlfilename);motl=motl.Value;
%subtomo_vol=tom_emread('volume.em');subtomo_vol=subtomo_vol.Value;
subtomo_vol=tom_mrcread(volumefilename);subtomo_vol=subtomo_vol.Value;
mask=tom_classmask(motl,subtomo_vol,[x y z],0,0);
tom_emwrite([pastedvolumefilename ], mask);