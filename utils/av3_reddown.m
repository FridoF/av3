function vol = av3_reddown(vol, ibin)
% routine av3_reddown boxes out the center of a volume for a given factor
% ibin
%
%   vol = av3_reddown(vol, ibin)
%
%   FF 12/21/2006
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
cent= [floor(size(vol,1)/2)+1 floor(size(vol,2)/2)+1 floor(size(vol,3)/2)+1];
dims = size(vol);
vol = vol(cent(1)-ceil(dims(1)/2)/(2^ibin):cent(1)+ceil(dims(1)/2)/(2^ibin)-1, ...
          cent(2)-ceil(dims(2)/2)/(2^ibin):cent(2)+ceil(dims(2)/2)/(2^ibin)-1, ...
          cent(3)-ceil(dims(3)/2)/(2^ibin):cent(3)+ceil(dims(3)/2)/(2^ibin)-1);
