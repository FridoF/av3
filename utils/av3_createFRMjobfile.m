function av3_createFRMjobfile(jobname, lowband, highband, frequency, maxiterations, PeakOffset, reference, mask, pixsize, diameter, plname)
% creates jobfile for FRM alignment in pyTom
%
% av3_createFRMjobfile(jobname, lowband, highband, frequency, maxiterations, PeakOffset, reference, mask, pixsize, diameter, plname)
%
% INPUT
%   jobname         name of jobfile
%   lowband         lower band-limit of spherical harmonics expansion - typically 8
%   highband        higher band-limit of spherical harmonics expansion - must not exceed 2*dim. 
%                   for lower resolution can be signficantly smaller.
%   frequency       starting spatial frequency for lowpass filtering (in inverse voxels)
%   maxiterations   number of maximum iterations
%   PeakOffset      allowed peak offset
%   reference       path of reference file
%   mask            path of mask file
%   pixsize         pixelsize of
%   plname          path to particleList
%
% SEE ALSO
%   av3_motl2particlelist, av3_createTMjobfile
%
%   UTRECHT University
%
% 22/10/2019 RE

fid = fopen(jobname, 'w');
 fprintf(fid, '%s\n', ['<FRMJob Destination="." BandwidthRange="[' num2str(lowband) ',' num2str(highband) ']" Frequency="' num2str(frequency) '" MaxIterations="' num2str(maxiterations) '" PeakOffset="' num2str(PeakOffset) '" RScore="False" WeightedAverage="False">']);
 fprintf(fid, '%s\n', ['   <Reference PreWedge="" File="' reference '" Weighting=""/>']);
 fprintf(fid, '%s\n', ['   <Mask Filename="' mask '" Binning="1" isSphere="True"/>']);
 fprintf(fid, '%s\n', ['   <SampleInformation PixelSize="' num2str(pixsize) '" ParticleDiameter="' num2str(diameter) '"/>']);

 fprintf(fid, '%s\n\r', ['   <ParticleListLocation Path="' plname '"/>']);

fprintf(fid, '%s\n\r', ['</FRMJob>']);
fclose(fid);
