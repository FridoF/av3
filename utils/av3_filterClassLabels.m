function motlout = av3_filterClassLabels(motlin, classlabels, motlout, zheight, scaling, rawsize)
% filters particlelist obtained by template matching based on individually
% inspected true positive peaks from Chimera chooser file
%
% av3_filterClassLabels(motlin, classlabels, motlout, zheight, scaling,rawsize)
%
% INPUT
%  motlin        name of unfiltered input motlfile
%  classlabels   name of Classlabel file from Chimera chooser
%  motlout       name of output motlfile
%  zheight       if tomogram was cropped in z for TM, provide z height for
%                unbinned motl [optional]
%  scaling       scaling factor that was applied to tomogram for TM, provide
%                for unbinned motl [optional]
%  rawsize       size of unscaled raw micrographs; provide for unbinned motl [optional]
%
% OUTPUT
%  motlout       filtered motl - only those with class==1 will be retained. if zheight 
%                and scaling are specified, the adjusted motl is returned
%  files         motlout, motlout_unbinned.em
%
% SEE ALSO
%
%
%   UTRECHT University
%
% 22/10/2019 RE

motl = tom_emread(motlin);
motl=motl.Value;
yy = load(classlabels);
%% set class column to class label
for ii = 1:size(yy,1)
    motl(20,ii)=yy(ii);
end
%% filter motl only according to class 1
j=0;
for i=1:size(motl,2)
    if motl(20,i) == 1;
        j=j+1;
        motl(:,j)=motl(:,i);
    end
end
%% write out, re-assign particle indices and  class to 0 - not really required
motl=motl(:,1:j);
motl(4,:) = 1:size(motl,2);
motl(20,:)=0;
tom_emwrite(motlout, motl);
%% accomodate for z-shift and binning, if specified
if exist('zheight','var')
    motiflist_classified = tom_emread(motlout); motiflist_classified=motiflist_classified.Value;
    motl_new=motiflist_classified;
    for ii=1:size(motiflist_classified,2)
        motl_new(8:10,ii)=motiflist_classified(8:10,ii)+[0 0 zheight]';
    end;
    motl_new(8:10,:) = scaling*motl_new(8:10,:);
    for ii=1:size(motiflist_classified,2)
        motl_new(8:10,ii) = motl_new(8:10,ii) - [rawsize/2 rawsize/2 rawsize/2]';
    end;
    motl_new(5,:) = 1;
    tom_emwrite([motlout(1:end-3) '_unbinned.em'],motl_new);
    return motl_new
else
    return motl
end
%end
