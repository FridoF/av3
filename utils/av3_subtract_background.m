function background_subtracted = av3_subtract_background(invol, radius, rsmooth)
%   background_subtracted = av3_subtract_background(invol, radius, rsmooth)
%
% INPUT
%   invol                   input volume
%   radius                  radius of kernel for background subtraction
%   rsmooth                 smoothing width
%
% OUTPUT
%   background_subtracted   
%
% FF 09/2019

% data
dimx = size(invol,1);dimy = size(invol,2);dimz = size(invol,3);
% local filter
localfilt=ifftshift(tom_spheremask(ones(dimx,dimy,dimz),radius,rsmooth));
npoints=sum(sum(sum(localfilt)));
localfilt=localfilt/npoints;
% background through convolution with filter
background = tom_ifourier(tom_fourier(invol).*(tom_fourier(localfilt)));
background_subtracted=invol-background;