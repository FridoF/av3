function av3_sortTSbymdoc(numtomo, mdocdir, mdocprefix, rawframesuffix, framedir, projdir, dim, scale)     
% sorts tilt series according to tilt angles and aligns movies 
%
% copies and sorts motioncorrected micrographs from rawdirectory into target directory. 
% Sorting is done in ascending order based on mdoc files. Also writes the tiltangle into the
% fileheader, crops the micrograph to the specified dimensions  (pytom 0.xx
% and av3 require square images) and, if scale > 1, writes out a downscaled proj into 'projdir'_scale 
%
% av3_sortTSbymdoc(numtomo, mdocdir, framedir, tdir, dim)
%   
% INPUT
%   numtomo                 number of tomograms
%   mdocdir                 directory with all mdoc files
%   mdocprefix              prefix of your tomograms, e.g. 'tomo' for
%                           tomo1.mrc.mdoc
%   rawframesuffix          suffix of rawframes listed in the mdoc file; '.tif' or '.mrc'
%   frames                  directory with all motioncorrected .mrc frames
%   projdir                 target directory for projections in tomogram folder, e.g.
%                           sorted will write micrographs into 'tomogram/sorted'
%   dim                     dimension to which micrograph will be cropped,
%                           e.g. '3710' for K2 detector will 
%   scale                   downscaling factor; enter '0' if no scaling is desired;  images will be written into
%                           'projdir'_scalingfactor
%   
% OUTPUT
%   creates a number of directories and files
%   directories
%                           tomogram<index>/<projdir> : folder for tiltseries of tomogram i
%                           tomogram<index>/<projdir>_scaled<scale>: scaled/downsampled tiltseries
%   files
%                           tomogram<index>/tilts.txt: tilt angles
%                           tomogram<index>/<projdir>/sorted_xxx.em: frame-aligned projections, sorted by tilt angle
%                           tomogram<index>/<projdir>_scaled/sorted_xxx.em: frame-aligned 
%                                                                           scaled projections, sorted by tilt angle
%                           
%   
% SEE ALSO
%   
%
%   UTRECHT University
%
% 21/10/2019 RE

for i=1:numtomo

        mkdir(['tomogram' num2str(i) '/' projdir])
        if scale>1
        mkdir(['tomogram' num2str(i) '/' projdir '_scaled' num2str(scale)])
        end

        [xx tilts]= grep('-Q','TiltAngle', strcat([mdocdir,'/',mdocprefix,num2str(i),'.mrc.mdoc']));
        tilts = tilts.result;
        tilts=tilts(:,13:end);
        tilts = str2num(tilts);    
        
        frames={};
        [xx paths]= grep('-Q','SubFramePath', strcat([mdocdir,'/',mdocprefix,num2str(i),'.mrc.mdoc']));
        frame_paths = paths.result;
        for yy=1:size(frame_paths,1)
            ind=findstr(frame_paths(yy,:),'\');
            ind2=findstr(frame_paths(yy,:),rawframesuffix);
            frames{yy} = frame_paths(yy,(ind(end)+1):(ind2-1));
        end
        frames = frames.';              
        
        tiltpos=[];
        for j=1:size(tilts,1)
            [a,b]=min(tilts);
            tiltpos=[tiltpos
                     b a];
            tilts(b,:)=max(tilts)+1;
        end
        
        frames_sorted=[];
        for l=1:size(tiltpos,1)
            frames_sorted=[frames_sorted
                           strcat(frames(tiltpos(l,1)), '.mrc')];
        end
        
        diary(['tomogram' num2str(i) '/tilts.txt'])
        diary on
        for m=1:size(frame_paths,1)
            im_aligned = tom_mrcread(char(strcat(framedir,'/',frames_sorted(m,:))));
            im_aligned.Value = im_aligned.Value(1:dim,1:dim);
            im_aligned.Header.Tiltangle = tiltpos(m,2);
            im_aligned.Header.Size(1) = dim;
            tom_emwrite(['tomogram' num2str(i) '/' projdir '/sorted_' num2str(m) '.em'],im_aligned);
            xx=im_aligned.Value;
            if scale > 1
                im_aligned.Value=tom_bin(xx,sqrt(scale));
                tom_emwrite(['tomogram' num2str(i) '/'  projdir '_scaled' num2str(scale) '/sorted_' num2str(m) '.em'],im_aligned);
            end
            disp(im_aligned.Header.Tiltangle);
        end
        diary off     
        
end

