function normvol = av3_normInMask(vol, mask)
%   normalize volume or image under mask
%   
%  PARAMETERS
%  IN
%   VOL     volume or image
%   MASK    mask, under which normalization is done
%   
%  OUT
%   NOMRVOL volume after normlization
%
%SEE ALSO
%   TOM_NORM
%
%   02/13 FF
%
%   Copyright (c) 2005-2013
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%determine number of pixels
npix = sum(sum(sum(mask)));
% mean under mask
mvol = sum(sum(sum(vol.*mask))) / npix;
normvol = (vol - mvol).*mask;
stv2 = sqrt(sum(sum(sum(normvol.^2)))/npix);
normvol = normvol/stv2;
