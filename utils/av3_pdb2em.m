function emmap = av3_pdb2em(pdbdata, pixelsize, dim, sigma)
% emmap = av3_pdb2em(pdbdata, pixelsize, dim, sigma)
%
%  PARAMETERS
%   PDBDATA         expected as structure as derived from TOM_PDBREAD
%   PIXELSIZE       desired pixelsize in Angstrom
%   DIM             dimension of desired cube
%   SIGMA           smoothing parameter in Angstroem
%
% smoothing according to
%   A(k) = A_0K exp(- k^2/(sigma^2))
%           or
%   A(k) = A_0K exp(- bfac * k^2) - remains to be implemented
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
error(nargchk(3,4,nargin))
if (nargin < 4)
    sigma = 0;
end;
emmap = zeros(dim,dim,dim);
femmap = zeros(dim,dim,dim);
x = [pdbdata.ATOM.X]/pixelsize+ floor(dim/2);%interpolate coordinates on mesh of pixelsize 
y = [pdbdata.ATOM.Y]/pixelsize+ floor(dim/2);
z = [pdbdata.ATOM.Z]/pixelsize+ floor(dim/2);
[qx, qy, qz] = ndgrid(-floor(dim/2):1:-floor(dim/2)+dim-1,...
    -floor(dim/2):1:-floor(dim/2)+dim-1,-floor(dim/2):1:-floor(dim/2)+dim-1);
qx = -2*pi/dim*qx;qy = -2*pi/dim*qy;qz = -2*pi/dim*qz;
atom = reshape([pdbdata.ATOM.AtomName],4,size([pdbdata.ATOM.AtomName],2)/4);
for iatom = 1:size(x,2)
    atphase = x(iatom)* qx + y(iatom) * qy + z(iatom) * qz;
    if (strmatch(atom(2,iatom),'H'))
        femmap = femmap + exp(i * atphase );
    elseif (strmatch(atom(2,iatom),'C') )
        femmap = femmap + 6*exp(i * atphase );
    elseif (strmatch(atom(2,iatom),'N'))
        femmap = femmap + 7*exp(i * atphase );
    elseif (strmatch(atom(2,iatom),'O'))
        femmap = femmap + 8*exp(i * atphase );
    elseif (strmatch(atom(2,iatom),'P'))
        femmap = femmap + 15*exp(i * atphase );
    elseif (strmatch(atom(2,iatom),'S'))
        femmap = femmap + 16*exp(i * atphase );
    end;
end;

if (sigma > 0)
    sigma = sigma/pixelsize;
    qx = qx.^2 + qy.^2 + qz.^2;
    qx = exp(- qx/(2*sigma^2));
    femmap = femmap.*qx;
end;
emmap = real(tom_ifourier(ifftshift(femmap)));
