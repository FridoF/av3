function dose = av3_dose(image, pixelsize, conversion_rate)
%   dose = av3_dose(image, pixelsize, conversion_rate)
%
%   image             EM image
%   pixelsize         pix size in A
%   conversion_rate   counts / incident electron
[mnv,b,c,d,e]=tom_dev(image);
dose = mnv/conversion_rate/(pixelsize^2);
