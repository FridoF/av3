function [phi, psi, theta] = av3_matrix2angle(matr)
% AV3_MATRIX2ANGLE converts rotation matrix to angles
%
%   [phi psi theta] = av3_matrix2angle(matr)
%
%   routine determines Euler angles corresponding to 
%   a given 3x3 rotation matrix.
%
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
zz = matr(3,3);
if zz >1
    theta = 0.;
elseif zz < -1
    theta = pi;
else
    theta = acos(zz);
end;

yz = matr(3,2);
xz = matr(3,1);
zy = matr(2,3);
zx = matr(1,3);
sinthe = sin(theta);
if sin(theta) ~=0
    cosphi = yz/sinthe;
    sinphi = xz/sinthe;
    phi = acos(cosphi);
    %if necessary move to different acos
    if sinphi < 0
        phi = phi + 2*(pi-phi);
    end;
    cospsi = -zy/sinthe;
    sinpsi = zx/sinthe;
    psi = acos(cospsi);
    if sinpsi < 0
        psi = psi + 2*(pi-psi);
    end;
else
    disp('theta = 0 remains to be implemented ...');
end;
psi = psi*180/pi;phi = phi*180/pi;theta = theta*180/pi;
