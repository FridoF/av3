function I=av3_dddcorrect(J, interval, s)
%TOM_XRAYCORRECT   Corrects X-Ray defects on EM Images (2-D)
%   This function finds the coordinates of the X-Ray infected pixels
%   and corrects them, by applying a mask. The mean value of the pixels
%   contained in the mask is calculated and the value of the affected
%   pixel is replaced by this mean value. If the input image has no
%   X-Ray defects then the output is the same as the input.
% 
%PARAMETERS
%
%  INPUT
%   image               raw ddd image
%   range               [min, max]
%   s                   neighborhood in pixel
%  
%  OUTPUT
%   I           		...
%
%  Examples
% ----------
%
%   J=AV3_DDDCORRECT(I,[minv, maxv]) returns the corrected image J by using a 5x5
%                        neighborhood for the calculation of the mean
%                        value
%
%REFERENCES
%
%   See also : TOM_LIMIT, TOM_SORTSERIES, TOM_SETMARK 
%
%
%   Copyright (c) 2004-2007
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute of Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

%default value for neighborhood
if ~exist('s','var')
    s=5;
end;
if size(J,3) > 1
    disp('Skipping 3D data');
    return;
end

I=J;

minv = interval(1);
maxv = interval(2);

[x y]=find(J>maxv | J<minv);

if isempty(x)
    return;
else
    for i=1:size(x,1)
        if ((x(i)-s > 0) && (y(i)-s > 0) && (x(i)+s <= size(I,1)) ... % bug fixed FF
                && (y(i)+s <= size(I,2)) )
            I(x(i),y(i))=mask(I,x(i),y(i),interval,s);
        else
            I(x(i),y(i))=mean(mean(I));
        end;
    end
end
if size(x,1)>0
  %  disp(['values outside range found: ' num2str(size(x,1))]); %changed by SN
end;


%****** SubFunction mask ***********

function c=mask(A,xcoor,ycoor,interval,s)

minv = interval(1);
maxv = interval(2);

a=A(xcoor-s:xcoor+s,ycoor-s:ycoor+s);
t=find(a>maxv | a<minv);
if isempty(t)
    c=sum(sum(a))/((2*s+1)*(2*s+1));
elseif (size(a,1)*size(a,2) == size(t,1))%bug fixed FF
    c=0;
else
    a(t)=0;
    c=sum(sum(a))/(((2*s+1)*(2*s+1))-size(t,1));
end


