function av3_imagesc(imag, lowpass)
%  display image with lowpass
%
%   imag     image to be displayed
%   lowpass  lowpass filter in pixel
%
tom_imagesc(tom_bandpass(imag,0,lowpass,lowpass/10));