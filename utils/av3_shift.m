function im = tom_shift(im, delta, indxgridx, indxgridy, indxgridz)
%TOM_SHIFT(IMAGE, [DELTA_X DELTA_Y DELTA_Z], INDXGRIDX, INDXGRIDY, INDXGRIDZ) 
%   shifts an image by a vector
%   
%   im = tom_shift(im, delta, indxgridx, indxgridy, indxgridz)
%
%   The shift is performed in Fourier space, thus periodic 
%   boundaries are implicit. The shift vectors do not have to be
%   integer.
%   currently restricted to even dimensions of IMAGE
%
% INPUT
%   IM          1D, 2D or 3D array
%   DELTA       1D, 2D or 3D vector for shift
%   INDXGRIDX   grid containing indices for shift  - optional, if not
%               specified created on the fly
%
% OUTPUT
%   IM          shifted 1D, 2D or 3D array
%
% EXAMPLE
%   yyy = tom_sphere([64 64 64],10,1,[16 16 16]);
%   yyy = tom_shift(yyy, [1,2,3]);
%   tom_dspcub(yyy);
%
% SEE ALSO
%   TOM_MOVE, TOM_SHIFT_FFT
%
%   08/01/02 FF
%last change
%   01/05/07 FF - added option for precomputed indexgrid
%
%    Copyright (c) 2004
%    TOM toolbox for Electron Tomography
%    Max-Planck-Institute for Biochemistry
%    Dept. Molecular Structural Biology
%    82152 Martinsried, Germany
%    http://www.biochem.mpg.de/tom
if ( (nargin >2) && (nargin<5) )
    error('Specify all indxgrids (=3) or none ...');
end;
[dimx,dimy,dimz]=size(im);
if (nargin < 3)
    %MeshGrid with the sampling points of the image
    [indxgridx, indxgridy, indxgridz] = ...
        ndgrid( -floor(size(im,1)/2):-floor(size(im,1)/2)+(size(im,1)-1),...
        -floor(size(im,2)/2):-floor(size(im,2)/2)+size(im,2)-1, ...
        -floor(size(im,3)/2):-floor(size(im,3)/2)+size(im,3)-1);
end; 
indx = find([dimx,dimy,dimz] == 1);
delta(indx)=0;
delta = delta./[dimx dimy dimz];
indxgridx = delta(1)*indxgridx + delta(2)*indxgridy + delta(3)*indxgridz; 
%clear indxgridy; clear indxgridz;
im = fftshift(fftn(im));
im = real(ifftn(ifftshift(im.*exp(-2*pi*i*indxgridx))));