function fil = av3_filter_to_res(vol, pixsize, res)
%   filter volume to specified resolution
%
%   INPUT
%   vol       volume (cubic!)
%   pixsize   pixel size in A
%   res       resolution in A
%
nfreq = floor(size(vol,1)/2);
Ny = 1/(2*pixsize);
lowp = 1/res/Ny * nfreq;
fil = tom_bandpass(vol, 0, lowp,lowp/10);