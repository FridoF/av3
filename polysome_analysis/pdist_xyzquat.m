function Y  = pdist_xyzquat(nnd, wgt)

% PDIST_XYZQUAT calculates a [ n*(n-1) / 2 ,1 ] distance matrix Y between
% all n observations of neighbor configurations that are described by the NND
% (next neighbor distribution). The NND is a (n,7)-matrix of (x,y,z)-nearest
% neighbor distance vectors and relative angles as quaternions
% (q0,qx,qy,qz). The procedure calculates the dot product between pairs of 
% spatial vectors or quaternions as a measure for similarity. The length 
% difference of distance vectors (wgt), their directionality and the relative 
% angles can be weighted by WGT(3,1)
%
%   INPUT
%   nnd         next neighbor features (7xn matrix), 1:3: coord, 4:7
%                   quaternions
%   wgt         weights for clustering (3-dim vector: distance, direction,
%                   orientation) 
%
%   OUTPUT
%   Y           distance matrix (pdist for clustering input)
%
%
%
%   03/06/09 FBR
%   last change 03/06/09 FBR
%


if nargin < 2
    wgt =[ 1 1 1];
    display('No weighting');
else
    display(['Weighting of distance, direction, orientation: ' num2str(wgt) ]);
end

n = size(nnd,1);
Y = zeros(1,n*(n-1)./2);
k = 1;
len = sqrt( nnd(:, 1) .^2 + nnd(:, 2) .^2 + nnd(:, 3) .^2);

for j = 1:n-1
    xyzdot =  sum(repmat(nnd(j, 1:3), size(nnd,1)-j ,1) .* nnd((j+1):n, 1:3), 2 ) ./ ( len(j+1:n) * len(j)) ;
    %xyzdot(logical(xyzdot < 0)) = 0;
    kk=find( xyzdot>1);
    xyzdot(kk)=1;
    kk= find(xyzdot<-1);
    xyzdot(kk)=-1;
    xyzdot = acos(xyzdot);
    
    qdot = abs( sum( repmat(nnd(j, 4:7), size(nnd,1)-j ,1) .* nnd((j+1):n, 4:7), 2) );
    kk=find( qdot>1);
    qdot(kk)=1;
    kk= find(qdot<-1);
    qdot(kk)=-1;   
    qdot = acos(qdot);
    lenmeas = len(j+1:n) ./ len(j);
    %lenmeas = real(acos(len(j+1:n) ./ len(j)));
    %disp([num2str(lenmeas), num2str(xyzdot), num2str(qdot)]);
 
    %Y(k:(k+n-j-1)) =  wgt(1) .* real(acos(len(j+1:n) ./ len(j))) + wgt(2) .* xyzdot + wgt(3) .* qdot ;
    Y(k:(k+n-j-1)) =  wgt(1) .* lenmeas + wgt(2) .* xyzdot + wgt(3) .* qdot ;
%     tmp = zeros(size(lenmeas,1),3);
%     tmp(:,1) = lenmeas;
%     tmp(:,2) = xyzdot;
%     tmp(:,3) = qdot;
    k = k + (n-j);
end