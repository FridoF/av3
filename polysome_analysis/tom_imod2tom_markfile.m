function tom_imod2tom_markfile(tiltfile)

% TOM_IMOD2TOM_MARKFILE reads N fine aligned tiltangles from an imod .tlt file
% and generates a .em markerfile with N angles in its first dimension. 
% In tom_Rec3dGUI, generate a new project with aligned series from 
% tom_mrcstack2emseries and the markerfile, save project as .mat. Then use 
% function tom_imod2tom_rec3d to modify the projectfile in order to proceed
% reconstruction with Rec3d_GUI.
%
%   Example: tom_imod2tom_markfile('tomo1.tlt');
%
% last change
% FBR 07/12/19
%
%


ang = load(tiltfile);

% series = 'tomo1_';
% for im = 1:size(ang,1)
%     hed = tom_reademheader(char([ series num2str(im) '.em' ]))
%     hed.Header.Tiltangle = ang(im);
%     tom_writeemheader(char([ series num2str(im) '.em' ]), hed.Header);
%     display(char(['Tiltangle ' num2str(ang(im)) ' written in Header of ' series num2str(im) '.em']))
% end

mark = zeros(3,size(ang,1),3); mark(1,:,1) = ang' ;
tom_emwrite(char(['marker_' tiltfile '.em']), mark); 
display(char(['marker_' tiltfile '.em written.']))
