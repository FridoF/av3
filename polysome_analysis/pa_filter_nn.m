function [ indx ] = pa_filter_nn( nn_dist, dist_min, dist_max, nmpix )
%	PA_FILTER_NN Filters NN for minimal and maximal distance
%   
%   INPUT       nn_dist         distance of nn
%               dist_min        minimal distance to cover
%               dist_max        maximal distance to cover
%               nm_pix          pixel size in nm
%   
%   OUTPUT      indx            index containing particles to analyze
%
indx01=logical(nn_dist>dist_min/nmpix);
indx02=logical(nn_dist<dist_max/nmpix);
indx=indx01.*indx02;
indx=find(indx==1);


end

