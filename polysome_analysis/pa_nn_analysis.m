function [ nn_dist p_shift_out euler_out ] = pa_nn_analysis(motl, dist_mat)
%   NN_ANALYSIS finds next neighbours and calculates relative orientation
%   and position in BOTH directions (part1 -> part2; part2 -> part1)
%
%   INPUT       motl                motiflist
%               dist_mat            distance matrix of particles
%
%   OUTPUT      p_shift_out         shifts for all particles in both
%                                   directions
%               euler_out           euler angles for relative rotation
%                                   between particles for both directions
%


nn=zeros(size(motl,2),1);
nn_dist=zeros(size(motl,2),1);
for i=1:size(motl,2);
    [dist_temp nn_temp]=sort(dist_mat(:,i));
    nn(i,1)=(nn_temp(1));
    nn_dist(i,1)=(dist_temp(1)); 
end

% calculate shift and rotation for all ribosome pairs in BOTH "directions"

euler_out=zeros(size(motl,2),6);
p_shift_out=zeros(size(motl,2),6);

for i=1:size(motl,2);      %i=2

            % calculate relativ rotation between ribosome pairs for direction 1
            rotations = [ motl(17,i) motl(18,i) motl(19,i); -motl(18,nn(i)) -motl(17,nn(i)) -motl(19,nn(i))]; 
            [rots shift_out rot_mat]=tom_sum_rotation(rotations, [0 0 0 ; 0 0 0]);
            rots = mod(real(rots),360);
            euler_out(i,1:3)=rots;
            
            % calculate shift of center of mass for each coordinate and rotate to template orientation for direction 2
            shift = [motl(8,i)+motl(11,i)-motl(8,nn(i))-motl(11,nn(i)) motl(9,i)+motl(12,i)-motl(9,nn(i))-motl(12,nn(i)) motl(10,i)+motl(13,i)-motl(10,nn(i))-motl(13,nn(i))];
            shift = tom_pointrotate(shift, -motl(18,nn(i)), -motl(17,nn(i)), -motl(19,nn(i)));
            p_shift_out(i,1:3)=shift;
           
            % calculate relativ rotation between ribosome pairs for direction 2
            rotations = [ motl(17,nn(i)) motl(18,nn(i)) motl(19,nn(i)); -motl(18,i) -motl(17,i) -motl(19,i)]; 
            [rots shift_out rot_mat]=tom_sum_rotation(rotations, [0 0 0 ; 0 0 0]);
            rots = mod(real(rots),360);
            euler_out(i,4:6)=rots;
            
            % calculate shift of center of mass for each coordinate and rotate to template orientation for direction 1
 
            shift = [motl(8,nn(i))+motl(11,nn(i))-motl(8,i)-motl(11,i) motl(9,nn(i))+motl(12,nn(i))-motl(9,i)-motl(12,i) motl(10,nn(i))+motl(13,nn(i))-motl(10,i)-motl(13,i)];
            shift = tom_pointrotate(shift, -motl(18,i), -motl(17,i), -motl(19,i));
            p_shift_out(i,4:6)=shift;
 
end


end

