function Y = pdist_quat(quat)



n = size(quat,1);
Y = zeros(1,n*(n-1)./2);
k = 1;

 for i = 1:n-1
            
            qlambda = 1- abs( sum( repmat(quat(i, 1:4), size(quat,1)-i ,1) .* quat((i+1):n, 1:4), 2) ); 
            
            Y(k:(k+n-i-1)) =   qlambda ;
            
            k = k + (n-i);
 end