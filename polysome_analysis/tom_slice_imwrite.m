% PRELIMINARY HARD-CODED SCRIPT
% FBR 09/07/06

for isub = 1%:5
    vol = tom_emread(char(['rec_poly_' num2str(isub) '.vol' ])); volnorm = tom_filter(tom_norm(vol.Value, '3std'), 3, 'circ');
    tom_dspcub(volnorm)
end 
%%
vol = tom_emread('../subvol1.em.norm'); volnorm = tom_filter(tom_norm(vol.Value, '3std'), 2, 'circ'); 
volnorm = tom_norm(volnorm, 255);
dim = size(volnorm);
slice =33;
imwrite(uint8(volnorm(:,:,slice)'), char(['./subvol1_dimxy' num2str(dim(1)) '_' num2str(dim(2)) '_z' num2str(slice) '.tif']) , 'Resolution', [300 300]);
%tom_volxyz(volnorm)
figure;
tom_imagesc(tom_proj3d(single(volnorm(:,:,33)), [0 0]))

%slice= 70 ;
for slice = 1:dim(3)
    image=volnorm(:,:,slice); %tom_imagesc(image);
    %imwrite(uint8(image)', char(['subvol1_' num2str(isub) 'z' num2str(slice) 'hires.tif']) , 'Resolution', [500 500]);
    imwrite(uint8(image)', char(['./tiff_out/subvol1_dimxy' num2str(dim(1)) '_' num2str(dim(2)) '_z' num2str(slice) '.tif']) , 'Resolution', [300 300]);
    %imwrite(uint8(image)', char(['rec_poly_' num2str(isub) 'z' num2str(slice) 'hires.tif']) , 'Resolution', [500 500]);
    % imwrite(volnorm(:,:,slice), char(['rec_poly_' num2str(isub) 'z' num2str(slice) 'hires.tif']) , 'Resolution', [300 300]);
end