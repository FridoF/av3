% motl9 = tom_emread('MOTL_poly_bin1_9.em'); motl9 = motl9.Value;
% wedge = tom_emread('wedgelist.em'); wedge = wedge.Value;
% mask = tom_emread('mask4av3_bin1.em'); mask = mask.Value;
% [average9_1 average9_2 frc] = av3_resdet(motl9, './particles_bin1/particle',wedge,2,1,0.07);
% 
% motl1 = tom_emread('MOTL_poly_bin1_1.em'); motl1 = motl1.Value;
% [average1_1 average1_2 frc] = av3_resdet(motl1, './particles_bin1/particle',wedge, 59,1,0.07);
% 
% frc_9 = tom_compare(average9_1.*mask, average9_2.*mask, 59);
% hold on; plot(frc_corr(:,9),'r'); 
% hold on; plot(frc(:,1),frc(:,9),'.');

%% cycle through <iter> iterations
dim = 64; apix = 8.21 %5.56;
iter = [1 3 6 9 ];
frc_iter = zeros(dim ./2 -1, 11, length(iter));
tom_emread('wedgelist.em'); wedge = ans.Value;
tom_emread('mask4av3_64.em'); mask = ans.Value;
%%
for i = iter
    motl = tom_emread(char(['MOTL_full_all_' num2str(i) '.em'])); motl = motl.Value;
    motl = motl(:, find(motl(20,:) ~= 0));
    [average average2 frc] = av3_resdet(motl, './particles_bin1_64/particle',wedge, dim ./2 -1 ,1,0.0);
    
    frc_mask = tom_compare(average.*mask,average2.*mask,30);
    figure; 
    plot(frc(:,1) ./ (apix* dim), frc(:,9), '.');hold on; plot(frc_mask(:,1) ./ (apix* dim) , frc_mask(:,9), '+'); drawnow;
    frc_iter(:,:,i) = frc;
    clear frc motl average average2
end



