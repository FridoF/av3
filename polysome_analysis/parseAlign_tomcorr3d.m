resultPath = ('/fs/pool/pool-nickell/ml_processing/data/experimental/MspA_hoffmann/particles/particles_PV09_ctf_orientation/');
system(['rm -rf ' resultPath]);
system(['mkdir ' resultPath]);

tom_av3_parseAlign_tomcorr3d('/fs/pool/pool-nickell/ml_processing/data/experimental/MspA_hoffmann/results/20091122-130406_PV09_ctfcor_refined_8s_global/hoffmann_mspa0018_par_alignfile.txt', ...  
                             '/fs/pool/pool-nickell/ml_processing/data/experimental/MspA_hoffmann/results/20091122-130406_PV09_ctfcor_refined_8s_global/hoffmann_mspa0018_par_particles_list.txt', ...
                             '',...
                             resultPath);

%mask =  tom_bandpass(tom_cylinder(0, 28, [96 96 96]),0,7);%
mask = ones(96,96,96);
tom_av3_align_particle_stack([resultPath 'Align.mat'], resultPath,mask,0);
 