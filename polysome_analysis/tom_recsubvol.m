function tom_recsubvol(filename,coords)
%
%function save_subvol(filename,coords)
%
%Saves one or several subvolumes of a larger volume, and alters the struct
%saved by Rec3DGUI (Rec3dProject) to contain information on the offset of 
%the subvolume in the full tomogram.
%
%The coordinates for the subvolumes should be given on the form
%coords=[xmin1 xmax1 ymin1 ymax1 zmin1 zmax1; xmin2 xmax2 ymin2 ymax2 zmin2 zmax2]
%
%The struct from Rec3DGUI is assumed to be saved in rec.mat, the altered
%struct is saved in rec_sub.mat. The subvolumes are saved as subvol1.em,
%subvol2.em, ..., subvol(n).em.
%
%2008-04-25 LAC

n=size(coords,1);
load(filename)


%making an array of structs
for i=1:n
    Rec3dProject(i)=Rec3dProject(1);
end

%modifying the structs
for i=1:n
    Rec3dProject(i).DETAIL.DetailMode='av3';
    Rec3dProject(i).DETAIL.OverviewSizeZ=Rec3dProject(i).VOLUME.SizeZ;
    Rec3dProject(i).DETAIL.OverviewPreBinning=Rec3dProject(i).VOLUME.PreBinning;
    Rec3dProject(i).DETAIL.OverviewPostBinning=Rec3dProject(i).VOLUME.PostBinning;
    Rec3dProject(i).DETAIL.NumberOfDetails=1;
    Rec3dProject(i).DETAIL.DetailCoordinates=[[coords(i,1) coords(i,3) coords(i,5)]];
end

save(Rec3dProject, ['sub' filename])
