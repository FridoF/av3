function [out motl1_out] = tom_motlcomp(motl1, motl2, r)

%
%   
%   TOM_MOTLCOMP compares 2 peaks ccc_i, ccc_k from 2 different motivelists
%   mapping to similar coordinates within radius r. Thereby, CCCs of two 
%   reference structures matching the same density can be compared locally.
%   Reads MOTLs in .em format, finds for each peak 'i' (motl1) the closest 
%   neighbour 'k' (motl2), lists corresponding distance, ccc_i, and ccc_k.
%   
%   Syntax: 
%       [out motl1_out] = tom_motlcomp(motl1, motl2, r);
%   Input:
%       motl1, motl2     motivelist, either as variable, or as .em-file
%       r                radius as INT (px)
%   
%   Output: [ccc_i ccc_k i k d rots]
%       i   index of peak from motl1
%       k   index of closest peak from motl2
%       d   distance between i, k (px)
%       ccc_i, ccc_k    corresponding CCC values
%       
%   Remark: Only peaks of class '1' (or > 0) are considered, indexing of 
%    original motivelist is changed !
%
%   Example: 
%       out = tom_motlocomp('motivelist_reference1.em', 'motivelist_reference2.em', 10);
%
%    See Also
%     DIST, TOM_PEAKDISTANCES
%
%    06/06/06 FBR
%   last change 11/05/07 FBR
%
%   Copyright (c) 2006
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom



if ischar(motl1) ==1
    
    %motl1= 'MOTL_sub1_bin1_3.em';
    motl1 = tom_emread(motl1);
    indx = find(motl1.Value(20,:) > 0);
    motl1 = motl1.Value(:,indx);
end

if ischar(motl2) ==1
    %motl2='MOTL_sub1_bin1mirr_3.em';
    motl2 = tom_emread(motl2);
    indx2 = find(motl2.Value(20,:) > 0);
    motl2 = motl2.Value(:,indx2);
end

out = [ ];

for i = 1:size(motl1,2)
    d = dist([motl1(8,i) motl1(9,i) motl1(10,i)] , [motl2(8,:) ; motl2(9,:); motl2(10,:)]);
    next = [find(d == min(d)) min(d)];
    if min(d) >= r
        continue
    else
        %c = [motl1(4,i) motl2(4,next(1)) next(2) motl1(1,i) motl2(1,next(1))]';
        rot = tom_sum_rotation([-motl2(18,next(1)) -motl2(17,next(1))  -motl2(19,next(1)) ; (motl1(17:19,i))' ], [0 0 0; 0 0 0]);
        c = [motl1(1,i) ; motl2(1,next(1)) ; motl1(4,i) ; motl2(4,next(1)) ; (motl2(8:10,next(1))-motl1(8:10,i)); rot' ];
        out = [out c];
        clear c
    end
end



%if nargout > 1
    motl1_out = motl1;
    motl1_out(6,:) = motl1_out(4,:); motl1_out(4,:)=0; motl1_out(20,:)=0;
    for i = 1:size(out(1,:),2)
        motl1_out(4, find(motl1(4,:) == out(2,i))) = out(3,i);
        motl1_out(20, find(motl1(4,:) == out(2,i))) = 1; % motl2(20, find(motl2(4,:) == out(2,i)));
    end
    motl1_out = motl1_out(:,find(motl1_out(20,:) > 0));
%else
    %display('Give two output arguments to generate motl1 with corresponding indices of motl2.');
%end
    
    
    