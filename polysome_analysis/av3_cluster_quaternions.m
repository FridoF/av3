function [euler_cindx euler_centroid L] = av3_cluster_quaternions(euler_in, maxclust)

%AV3_KMEANS_QUATERNIONS converts a Euler angle set into quaternions and
%performs (Euclidean) kmeans clustering in quaternion space. Quaternion
%centroids are backconverted to Eulerians and plotted with input
%distribution in 3D. Used to classify relative angle distribution of
%neighbor-neighbor relations.
%
%       INPUT
%       euler_in      3 x N list of Euler angles
%       clusters         No. of kmeans clusters
%
%
%      OUTPUT
%       euler_indx           Cluster indices
%       euler_centroid    Centroids backconverted in Eulerians
%       L                           Linkage tree
%
%       Example:
%
%         [euler_cluster euler_class] = av3_cluster_quaternions(euler_in, 3);
%
%
%    See Also
%     KMEANS, AV3_PEAKNEIGHBOUR_EULER, ATAN2
%
%    05/12/05 FBR
%   last change 25/03/08 FBR
%
%   Copyright (c) 2008
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

%% convert to quaternions (according to R.Hegerl, 10.5.07)
%euler_in = euler_out;


if nargin <4
    seed = 'sample';
end
if nargin <3
    metric = 'sqEuclidean';
end


quaternions = av3_eul2quat(euler_in);

% quaternions = [ ];
% for ieul = 1:size(euler_in,1) / 3
%     phi = euler_rad(3 * ieul -2,:);
%     psi = euler_rad(3 * ieul -1,:);
%     the = euler_rad(3 * ieul,:);
%     
%     q0 = cos(the ./2) .* cos((psi + phi) ./ 2);
%     qx = sin(the ./2) .* cos((psi-phi) ./ 2);
%     qy = sin(the ./2) .* sin((psi -phi) ./2);
%     qz = cos(the ./2) .* sin((psi + phi) ./2);
%     quat = [q0' qx' qy' qz'];
%     quaternions = [quaternions quat];
% end
% clear quat ieul
% check:
% q0.^2 + qx.^2 + qy.^2 + qz.^2
%figure; [X,Y,Z] = sph2cart(phi,the,1); title('Input phi,the in Cartesian space');
%scatter3(X,Y,Z)


%% hierarchical clustering

pdistmat = pdist_quat(quaternions);
L = linkage(pdistmat, 'complete' );

%clust = 4:8;
euler_cindx = cluster(L,  'maxclust', maxclust );
imode = logical(euler_cindx == mode(euler_cindx));
clear pdistmat
% iclust =[ ]; euler_cindx_tmp = euler_cindx;
% while size(iclust,2) < clust
%    
%     iclust = [iclust mode(euler_cindx_tmp)];
%     euler_cindx_tmp(find(euler_cindx_tmp == mode(euler_cindx_tmp)) ) = [ ];
% end
% clear euler_cindx_tmp

%figure
%subplot(121), dendrogram(L); title('Dendrogram of Quaternion clusters');
%subplot(122), hist(euler_cindx, max(euler_cindx)); %title( [ num2str( size(find(imode ==1),1) ./ 

%determine center of main cluster
%[euler_indx q_c qsumd qd] = kmeans(quaternions(imode,:), 1, 'Distance', metric, 'Start', seed);
%clear euler_indx
% norm q_centroid so that q0^2+ qx^2+ qy^2+ qz^2 == 1;

% q_centroid = zeros(clust, size(quaternions,2));
% for imode = iclust
q_c = mean(quaternions(imode,:), 1);

q_centroid = [ ];
for ieul = 1:size(euler_in,1) / 3
    a = sqrt( 1 ./ (q_c(:,ieul * 4-3).^2 + q_c(:,ieul *4-2).^2 + q_c(:,ieul * 4-1).^2 + q_c(:,ieul * 4).^2));
    a = [a a a a];
    q_centroid = [q_centroid  q_c(ieul * 4-3 : ieul * 4).* a ];
    
    clear a
end

%check output
%q_centroid(:,1) .^2 + q_centroid(:,2) .^2 + q_centroid(:,3) .^2 + q_centroid(:,4) .^2

%% backconversion to eulers

euler_centroid = [ ];
for ieul = 1:size(euler_in,1) / 3
q0 = q_centroid(:,ieul * 4-3  ); 
qx = q_centroid(:,ieul * 4-2  );
qy = q_centroid(:,ieul * 4-1  ); 
qz = q_centroid(:,ieul * 4      );

 the = atan2( 2 .* sqrt((qx.^2 +qy.^2)  .* (q0.^2 + qz .^2) ) , (q0.^2- qx.^2- qy.^2+ qz.^2));
   psi = atan2( 2 .* (qx .*qz + q0 .*qy) ./ sin(the) , (-2 .*(qy .*qz - q0 .*qx) ./sin(the)));
   phi = atan2( 2 .* (qx .*qz - q0 .*qy) ./ sin(the) , ( 2 .*(qy .* qz + q0 .*qx) ./sin(the)));



euler_c = [phi psi the] .* 180 ./pi; 

euler_c(:,1:2) = mod(euler_c(:,1:2), 360);
euler_c(:,3) = mod(euler_c(:, 3), 180);

euler_centroid = [euler_centroid euler_c];
end

%for j = 1:maxclust
 %   euler_centroid(j,:) = tom_angles2euler(euler_centroid(j,:));
%end
    

%% plot 
% figure
% scatter3(euler_in(1,:), euler_in(2,:), euler_in(3,:), 'k', 'o', 'filled'), title('{\bf rel euler phi psi the}'), hold on;
% scatter3(euler_centroid(:,1) , euler_centroid(:,2) , euler_centroid(:,3) ,'o', 'MarkerFaceColor', [0.5 0.5 0.5], ...
%     'MarkerEdgeColor' ,[0.5 0.5 0.5],  'LineWidth', 6);
% set(gca, 'LineWidth',2, 'XLim', [-10 370], 'YLim', [-10 370], 'ZLim', [-10 190], 'FontSize', 16, 'Box', 'on');

