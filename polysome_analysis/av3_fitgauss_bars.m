function gaupars = av3_fitgauss_bars(gaupars0, xhist, yhist, data)
%   fit gaussian to histogram of correlation values
%
%   gaupars = av3_fitgauss(gaupars0, xhist, yhist)
%
%  INPUT
%   gaupars0     initial estimate for  3 parameters for Gauss fit: 
%                   [N_parts, mean, sigma]
%   xhist        x-values of histogram (=bins)
%   yhist        y-values of histogram (actual histogram)
%
%  OUTPUT
%   gaupars      fitted gauss parameters
%
%  EXAMPLE
%   motl = tom_emread('motl_80S_bin2_complete_r6.em');
%   xhist = [.45:.025:.7];
%   [yhist, xhist] = hist(motl.Value(1,:),xhist);
%   gaupars = av3_fitgauss([300, .5, .05], xhist, yhist, .5);
%
%  SEE ALSO
%   av3_Gaussian
%
%   03/2011 FF
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
xhist_complete = xhist;
yhist_complete = yhist;
if exist('xthres')
    ii = find(xhist > xthres);
    if size(ii,2) == 0
        error('Threshold too high - no values in interval')
    end;
    yhist = yhist(ii);
    xhist = xhist(ii);
end;
%gaupars0(1)=sum(yhist);
options = optimset('GradObj','off','LargeScale','on',...
        'TolFun',.0000001, 'MaxFunEvals',1000000000, 'TolX',.00000000001,...
        'MaxIter',1000);%,'Diagnostics','on');
gaupars = fminunc(@(gaupars) av3_GaussFit(gaupars, xhist, yhist), gaupars0, options);
Y = av3_gauss( xhist_complete, gaupars(1), gaupars(2), gaupars(3));
hist(data,20);hold on;
plot(xhist_complete,Y,'k-');
%estimate TPs and FPs
tp=cumsum(Y(size(Y,2):-1:1));
fp=cumsum(yhist_complete(size(Y,2):-1:1))-tp;
figure;plot(fp/max(tp)*100.,tp/max(tp)*100.,'k.');
xlabel('FP (% of total particle estimate)');
ylabel('TP (% of total particle estimate)');