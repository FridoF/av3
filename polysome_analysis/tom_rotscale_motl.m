function motl_rotscale = tom_rotscale_motl(motivelist, rot, apix, apix_new, newcenter)

%
% TOM_ROTSCALE_MOTL rotates the euler angles (17:19,:) from a motivelist by
% [rot], rescales coordinates (8:10,:) by apix/apix_new, and applies a shift of the
% reference template AFTER rotating [newcenter]. This can be used to bring motivelists
% from different templates / template orientations and different binning
% factors / object px sizes into agreement. The relative reference orientation [rot] 
% must be previously determined (i.e. with v4 from EMAN).
%
%  In EM (ROT )or Matlab (tom_rotate) : 
%
%			MOTL(:,17-19)	=           REF1 >>>  (phi, psi, the)  >>>> PART
%                                       REF1 <<< (-psi, -phi, -the) <<< PART
%	REF2 <<<  (phi2, psi2, the2)   <<<  REF1 
%	REF2 >>> (-psi2, -phi2, -the2) >>>  REF1
%
%	PART >>>   tom_sum_rot ( -psi, -phi, -the; phi2, psi2, the2 )  >>>  REF2
%								= (phi3, psi3, the3)
%   PART <<<<<<<<<<<<<<<<<<<<<< (-psi3, -phi3, -the3)  <<<<<<<<<<<<<<<  REF2
%   MOTL_mod(:,17-19)= (-psi3, -phi3, -the3) 
%
% (phi2, psi2, the2)= (-Az, -Phieman, -Alt) from v4 (EMAN)
%
%
%
%  PARAMETERS
%   motl            motivelist (see AV3_TRANS-ROT-ALIG for format)
%   rot             euler angles to add (phi2, psi2, theta2)
%   newcenter       coordinates in reference 1 of the new center in ref2 (xnc1, ync1, znc1)
%   ref1size        volume size of reference1
%   ibin            binning factor of MOTL coordinates with respect to
%                   reference 1
%                   
%
% Example: Alt=-70 Az=120.34 Phi=262.5
% motivelist=tom_emread('MOTL_unbinned_t611_100S_1.em');
% motivelist=motivelist.Value;
% motl_rotscale = tom_rotscale_motl(motivelist, [-120.34 -260.5 70], 4.789, 5.56, [10 0 0])
%
% See also: TOM_SUM_ROTATION
%
% JO / FBR 02.08.07: 

%%
if nargin <= 3 
    newcenter = [0 0 0]; apix = 1; apix_new = 1;
    display('Motivelist coordinates not rescaled.'); 
    display('No shift applied to reference. 5th argin required: [dx dy dz] ')
end;

if nargin >=3 & nargin < 5  
    newcenter = [0 0 0]; 
    display('No shift applied to reference: 5th argin required: [dx dy dz]')
end

if nargin < 3
    display('More input arguments needed: motl_rotscale = tom_rotscale_motl(motivelist, [rot], apix, apix_new, [newcenter])');
end

display(char(['Motivelist rotated with (phi,psi,the)= ' num2str(rot) ]));
display(char([' Motivelist Shifted by [dx dy dz]= ' num2str(newcenter)]));
display('Shift correction not implemented.');

%%

motl_rotscale = motivelist; 
% rot = [-120.34 -260.5 70]; apix = 4.789; apix_new = 5.56;
%euler_out = zeros(3,size(modmotl,2));
motl_rotscale(8:10,:) = motl_rotscale(8:10,:) .* (apix / apix_new);
shift_out = zeros(3,size(motl_rotscale,2));

for i = 1:size(motivelist,2)

    [euler_i, shift_i, rotM ]= tom_sum_rotation([-motivelist(18,i), -motivelist(17,i), -motivelist(19,i); rot(1) rot(2) rot(3)], [0 0 0; newcenter]);
    motl_rotscale(17:19,i) = [-euler_i(2) -euler_i(1) -euler_i(3)]';
    
    clear rotM euler_i shift_i

%     translations:
%     x= motivelist(8,i);y= motivelist(9,i); z= motivelist(10,i);
%     partcenter=[x, y, z];
%     newcenteroripart = tom_pointrotate(newcenterori2, euler_ref2part(1,1), euler_ref2part(1,2), euler_ref2part(1,3));
%     newcenteroripart = tom_pointrotate(newcenterori2, euler_ref2part(1,1), euler_ref2part(1,2), euler_ref2part(1,3))
%     [euler_out,shifts_out,rotmatrix]=tom_sum_rotation([0, 0, 0; -psi3,-phi3,-theta3], [0 10 0; 0 0 0]);
%     newcenteroritomo= partcenter + newcenteroripart;
%     read shift BEFORE rot
%     xshift = motivelist(14,i); yshift = motivelist(15,i); zshift = motivelist(16,i);
%     read shift AFTER rot
%     tshift(1:3) = motivelist(11:13,i)/(2^ibin);
% 
%     motl_rotscale(8,i)  = newcenteroritomo(1,1);
%     motl_rotscale(9,i)  = newcenteroritomo(1,2);
%     motl_rotscale(10,i) = newcenteroritomo(1,3);

end

