function tom_linescan(vol, p1, p2, width)


vol_1 = vol.Value(x1 : x2, y1:y2, z1:z2);


[dim1 dim2 dim3]=size(volume);  
switch (axis)
 
case 'xy'

    m=sum(volume,3);
case 'yz'
    volume=permute(volume,[3 2 1]);
    m=sum(volume,3);
case 'xz'
    volume=permute(volume,[3 1 2]);
    m=sum(volume,3);
otherwise
       error('Unknown orientation'); 
end


rot = tom_rotate(vol_1, -52);
close all; figure;
subplot(121), plot(sum(rot(20:40,20:50)))
subplot(122), tom_imagesc(rot);

prj = tom_project2d(rot, 'xz');

figure; 
subplot(121), tom_imagesc(prj)
subplot(122), plot(prj(5,:)), title('Linescan, col 5');

figure; 
subplot(121), tom_imagesc(prj)
subplot(122), plot(prj(5,:)), title('Linescan, col 5');