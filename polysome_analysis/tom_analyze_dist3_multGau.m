function [a shift_norm h d yfit E] = tom_analyze_dist3_multGau(shifts, nmpix, a0, d_lim)

%   TOM_ANALYZE_DIST3_MULTGAU fits and plots a histogram of distance vectors 
%     to a double gaussian distribution. 
%   
%   IN:     shifts  3 X N matrix
%           nmpix   pixel size in nm
%            a0        [init- I_max, x_middle, sigma-decay]
%
%   OUT:    a           I_max, x_middle, sigma-decay
%           shift_norm  list of normed distance vectors
%           h           handle to figure
%           d           distribution of distances
%           yfit        fit to distribution
%           E           error of fit
%
%   Example: 
%       [a shift_norm] = tom_analyze_dist(shift_out);
%
%    See Also
%     TOM_PEAK_SUMROT, TOM_ANALYZE_DIST3
%
%   07/02/10 SP & FF
%
%   Copyright (c) 2010
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

if nargin <4
    d_lim = 0;
end

if nargin < 3
    a0=[.5, 31, 2, .5, 29, 1];
end

if nargin < 2
    nmpix = 1; display('No pixel size given. Default value: 1 ');
else 
    display(char ([' Px size: ' num2str(nmpix) ]));
end

shift_norm = [];
for i = 1:(size(shifts,2))
    shift_norm = [shift_norm norm(shifts(:,i))];
end
shift_norm = shift_norm * nmpix;
shift_norm = shift_norm(shift_norm > d_lim);

[dfreq d] = hist(shift_norm(:),120); % plot(d,dfreq);
% single Gaussian - use only shifts > 'r' px 
% ind = find(d>2*r);
global xData yData dy
xData=d';yData=dfreq';dy=1;
tot=sum(yData);yData = yData/tot;
% a0=[100, 20, 1]; % init- I_max, x_middle, sigma-decay
options = optimset('MaxFunEvals',10000000,'MaxIter',1000000, 'TolFun', .0000000000001);
a = fminsearch(@GauGau,a0,options); %fit to Gaussian
E = GauGau(a);
x=0:.1:100;
sqrt2 = sqrt(2);
yfit =1/(sqrt(2*pi))* ( a(1)/a(3).*exp(-((x-a(2))/(sqrt2*a(3))).^2) + a(4)/a(6).*exp(-((x-a(5))/(sqrt2*a(6))).^2));
h = figure; screen= get(0, 'ScreenSize');
set(gcf, 'Position', [screen(3)/4 screen(4)/4 320 320 ], 'Color', [1 1 1]);
%plot(d,dfreq,'-+');
bar(xData,yData); h = findobj(gca,'Type','patch'); set(h,'FaceColor', [0.4 0.4 0.4] ,'EdgeColor','k')
hold on;
plot(x,yfit,'k', 'LineWidth', 2); % title(char([ 'initial I-max, x-mean, sigma-decay = ' num2str(a)]));

set(gca, 'Box', 'on', 'LineWidth', 2, 'FontSize', 16, 'FontName', 'Arial', 'TickLength', [0.02 0.0125] );
title( [ ' c1=' num2str(a(1)) ' m1=' num2str(a(2)) ' s1=' num2str(a(3)) ...
    ' \newline c2=' num2str(a(4)) ' m2=' num2str(a(5)) ' s2=' num2str(a(6)) ...
    ' \newline \chi^2 =' num2str(E)] );
pct1 = sum(1/(sqrt(2*pi))* ( a(1)/a(3).*exp(-((x-a(2))/(sqrt2*a(3))).^2) ));
pct2 = sum(1/(sqrt(2*pi))* ( a(4)/a(6).*exp(-((x-a(5))/(sqrt2*a(6))).^2) ));
allp = sum(yData);
allfit = sum(yfit);
disp(['Particles in distribution 1: ' num2str(pct1/allfit)]);
disp(['Particles in distribution 2: ' num2str(pct2/allfit)]);
set(gca, 'Box', 'on', 'LineWidth', 1, 'FontSize', 16, 'XLim', [0 70] , 'YLim', [0 .1] );
tot_theo = sum(yfit);disp(['N particles analyzed: ' num2str(round(tot_theo))]);
display(char([ 'Initial parameters [I_max, x_mean, sigma-decay]:  ' num2str(a0)])); 
display(char([ 'Fitted parameters [I_max, x_mean, sigma-decay]:  ' num2str(a)])); 
disp(['Gaussian mean distance [nm] : ' num2str(a(2))]);

%% for comparison fit to single Gaussian
a0 = [a(1)+a(4), (a(2)+a(5))/2, (a(3)*a(6))/2];
asing = fminsearch(@Gau, a0, options); %fit to Gaussian
ysing = asing(1)/(sqrt(2*pi)*asing(3)) .*exp(-((x-asing(2)).^2)/(sqrt(2)*(asing(3).^2)));
Esing = Gau(asing);
figure; screen= get(0, 'ScreenSize');g=gcf;
set(g, 'Position', [screen(3)/4 screen(4)/4 320 320 ], 'Color', [1 1 1]);
bar(xData,yData); g = findobj(gca,'Type','patch'); set(g,'FaceColor', [0.4 0.4 0.4] ,'EdgeColor','k')
hold on;
plot(x,ysing,'k', 'LineWidth', 2);
set(gca, 'Box', 'on', 'LineWidth', 2, 'FontSize', 16, 'FontName', 'Arial', 'TickLength', [0.02 0.0125] );
title( [ ' c1=' num2str(asing(1)) ' m1=' num2str(asing(2)) ' s1=' num2str(asing(3)) ...
    ' \newline \chi^2 =' num2str(Esing)] );
set(gca, 'Box', 'on', 'LineWidth', 1, 'FontSize', 16, 'XLim', [0 70] , 'YLim', [0 .1] );


