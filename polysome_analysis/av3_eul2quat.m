function quaternions = av3_eul2quat(euler_in)

%AV3_EUL2QUAT converts M Euler angle sets into quaternions according to zxz
%rotation convention.
%
%       INPUT
%       euler_in        3 x N x M list of Euler angles
%      
%
%      OUTPUT
%       quaternions     N x 4 x M [q0 qx qy qz]
%
%       Example:
%
%         quaternions= av3_cluster_quaternions(euler_in);
%
%
%    See Also
%     AV3_QUAT2EUL, AV3_CLUSTER_QUATERNIONS, ATAN2
%
%    05/12/05 FBR
%   last change 25/03/08 FBR
%
%   Copyright (c) 2008
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

euler_rad = euler_in  .*pi ./180 ;


% define pivot
% the = 1;psi =0; phi = 0;
% 
% q0_p = cos(the ./2) .* cos((psi + phi) ./ 2);
% qx_p = sin(the ./2) .* cos((psi-phi) ./ 2);
% qy_p = sin(the ./2) .* sin((psi -phi) ./2);
% qz_p = cos(the ./2) .* sin((psi + phi) ./2);
    

quaternions = [ ];
for ieul = 1:size(euler_in,1) / 3
    phi = euler_rad(3 * ieul -2,:);
    psi = euler_rad(3 * ieul -1,:);
    the = euler_rad(3 * ieul,:);
    
    q0 = cos(the ./2) .* cos((psi + phi) ./ 2);
    qx = sin(the ./2) .* cos((psi-phi) ./ 2);
    qy = sin(the ./2) .* sin((psi -phi) ./2);
    qz = cos(the ./2) .* sin((psi + phi) ./2);
    quat = [q0' qx' qy' qz'];
    
%     dp = q0_p*q0 + qx_p*qx + qy_p*qy + qz_p*qz;
%     
%     if dp < 0
%         quat = -quat;
%     end
    
    quaternions = [quaternions quat];
    
    
    
end
clear quat ieul