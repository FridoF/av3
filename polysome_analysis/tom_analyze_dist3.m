function [a shift_norm h d yfit] = tom_analyze_dist3(shifts, nmpix, a0, d_lim)

%   TOM_ANALYZE_DIST3 fits and plots a histogram of distance vectors 
%     to a gaussian distribution. 
%   
%   IN:     shifts  3 X N matrix
%           nmpix   pixel size in nm
%            a0        [init- I_max, x_middle, sigma-decay]
%   OUT:    a       I_max, x_middle, sigma-decay
%           shift_norm  list of normed distance vectors
%
%   Example: 
%       [a shift_norm] = tom_analyze_dist(shift_out);
%
%    See Also
%     TOM_PEAK_SUMROT
%
%    01/02/07 FBR
%   last change 01/02/07 FBR
%
%   Copyright (c) 2007
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

if nargin <4
    d_lim = 0;
end

if nargin < 3
    a0=[40, 30, 1];
end

if nargin < 2
    nmpix = 1; display('No pixel size given. Default value: 1 ');
else 
    display(char ([' Px size: ' num2str(nmpix) ]));
end

shift_norm = [];
for i = 1:(size(shifts,2))
    shift_norm = [shift_norm norm(shifts(:,i))];
end
shift_norm = shift_norm * nmpix;
shift_norm = shift_norm(shift_norm > d_lim);

[dfreq d] = hist(shift_norm(:),120); % plot(d,dfreq);
% single Gaussian - use only shifts > 'r' px 
% ind = find(d>2*r);
global xData yData dy
xData=d';yData=dfreq';dy=1;
tot=sum(yData);
% a0=[100, 20, 1]; % init- I_max, x_middle, sigma-decay
options = optimset('MaxFunEvals',1000000,'MaxIter',100000);
a = fminsearch(@Gau,a0,options); %fit to Gaussian
E=Gau(a);
yfit = a(1).*exp(-(((d-a(2))).^2)/(2*a(3).^2));
h = figure; screen= get(0, 'ScreenSize'); set(gcf, 'Position', [screen(3)/4 screen(4)/4 320 320 ]);
%plot(d,dfreq,'-+');
hist(shift_norm, 50); h = findobj(gca,'Type','patch'); set(h,'FaceColor', [0.4 0.4 0.4] ,'EdgeColor','k')
hold on;
plot(d,yfit,'k', 'LineWidth', 2); % title(char([ 'initial I-max, x-mean, sigma-decay = ' num2str(a)]));

set(gca, 'Box', 'on', 'LineWidth', 2, 'FontSize', 16, 'FontName', 'Arial', 'TickLength', [0.02 0.0125] );

tot_theo = sum(yfit);disp(['N particles analyzed: ' num2str(round(tot_theo))]);
display(char([ 'Initial parameters [I_max, x_mean, sigma-decay]:  ' num2str(a0)])); 
display(char([ 'Fitted parameters [I_max, x_mean, sigma-decay]:  ' num2str(a)])); 
disp(['Gaussian mean distance [nm] : ' num2str(a(2))]);
