function tom_Rec3dModifyPath(rec3dproject, prjpath, name)

% Reads Rec3dProject and modifies pathnames and filenames of projections.
% Used to switch between absolute and relative pathnames of project quickly.
%
%
% Example:      tom_Rec3dModifyPath('rec_full.mat', './hist/');
%
%
% FBR 2008/03/03
%
%
%

load(rec3dproject)

if nargin > 2
    for nfile = 1:size(Rec3dProject.PARAMETER.ProjectionsName,2);
        Rec3dProject.PARAMETER.ProjectionsName(nfile) = char([name '_' num2str(nfile) '.em']);
        Rec3dProject.PARAMETER.ProjectionsNameOriginal(1) = char([name '_' num2str(nfile) '.em']);
    end
end

for nfile = 1:size(Rec3dProject.PARAMETER.ProjectionsName,2);
    Rec3dProject.PARAMETER.ProjectionsPath(nfile) = {};
    Rec3dProject.PARAMETER.ProjectionsPath(nfile) = {prjpath};
    Rec3dProject.PARAMETER.ProjectionsPathOriginal(nfile) = { } ;
    Rec3dProject.PARAMETER.ProjectionsPathOriginal(nfile) = {prjpath};
end

rec3dproject_path = char([ rec3dproject(1:end-4) '_path.mat']);
display(char([ 'TESTRUN: ' rec3dproject ' with modified pathname ' prjpath ' saved to ' rec3dproject_path ]))

% save


