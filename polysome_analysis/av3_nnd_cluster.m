%motivelist = './MOTL_70S_bin1_2';
motivelist = './MOTL_1class_009'; 
motl_in = tom_emread(char([motivelist '.em'])); motl_in = motl_in.Value;
motl = motl_in;
nmpix =   0.821 ; %2.85;
dim = 60;
p1 = [0 0 0 ]; 
p2= [33 14 30] - [dim/2+1 dim/2+1 dim/2+1]; % mRNA entry
p3=[9 32 33] - [dim/2+1 dim/2+1 dim/2+1]; % L9 ?
p = [p1; p2; p3]; clear p1 p2 p3 dim
p = p .* 0.556 / nmpix;
next = 2;
 direction = 'uni';
[p_shift_out euler_out rott_out] =  av3_neighbor(motl, p, next, direction);


%choose only particles with a next neighbor closer than <limit>
[a shift_norm] = tom_analyze_dist3(p_shift_out(:,1:3)', nmpix, [80, 20, 1] ); title( [ 'm=' num2str(a(2)) 's=' num2str(a(3)) ] );
set(gca, 'Box', 'on', 'LineWidth', 1, 'FontSize', 16, 'XLim', [15 65] , 'YLim', [0 800] ); 
clear a
indx = logical(shift_norm >= 20) & (shift_norm <= 40  );

%% multidimensional hierarchical clustering
% relative distance vectors and relative neighbor quaternions are clustered
% with euclidean metric.
X = [p_shift_out(indx,:) ];
quat = av3_eul2quat(euler_out(:, indx));
%for multidimensional clustering
%Xstd =  X ./ std2(X);
Xstd = reshape(X', size(X,2) /next, size(X,1) * next)';

quatstd = reshape(quat', size(quat,2) /next, size(quat,1) * next)';

nnd = [ Xstd(:,1:3) quatstd ] ;
pdistmat = pdist_xyzquat(nnd, [1 1 1]);
%pdistmat = pdist( nnd , 'seuclidean');

L = linkage(pdistmat, 'average');
%inconsistent(L);
%clust = 8;
C = cluster(L, 'cutoff', 0.7 , 'criterion', 'distance'  );
clust =[ ]; Ctemp = C;
while size(clust,2) < 6
    clust = [clust mode(Ctemp)];
    Ctemp(find(Ctemp == mode(Ctemp)) ) = [ ];
end
clear Ctemp pdistmat
C = reshape(C',next, size(C,1) / next  )';

%%


NNDcluster = struct( 'date', datestr(now), 'motl', motl_in, 'indx', indx , 'iptcl', [ ],'direction', direction, 'centr',[ ], 'd', [ ], 'sumd', [ ], 'gamma', zeros([2 2]), 'var', [ ], 'quatcentr', [ ], 'iptclquat', [ ], 'av' , [ ]);
dist3fig = figure;
distfig =figure;
centr = zeros(size(clust,2), 3);
subplot(321), dendrogram(L) ;set(gca,'Box', 'on', 'LineWidth', 1, 'YTick', [1.2 1.5 1.8 2.1], 'FontSize', 16, 'FontName', 'Arial');
subplot(322), hist(C, max(max(C)), clust); set(gca, 'LineWidth', 1, 'XLim', [0 max3d(C)+1], 'XTick', [0 50 100 150], 'FontSize', 16,  'FontName', 'Arial' );
clustfig = figure;

% lim.x = [ -30 30; -30 30;  -30 30; -30 30; -30 30; -30 30 ];
% lim.y = [ -30 30; -30 30;  -30 30; -30 30; -30 30 ; -30 30 ];
% lim.z =[ -30 30; -30 30;  -30 30; -30 30; -30 30 ; -30 30 ]; 

lim.x = [ -40 0; -10 30;  -20 20; -40 0; 0 40; 0 40 ];
lim.y = [ -30 10; -40 0;  -40 0; -30 10; -40 0 ; -40 0 ];
lim.z = [-30 10; -30 10;  -10 30; -30 10; -20 20; -40 0 ];

for c = 1:size(clust,2)
    cindx = logical(C(:,1) == clust(c));
    figure(distfig)
    subplot(3,2,3:6), scatter3(X(cindx,1) .* nmpix,X(cindx,2) .* nmpix,X(cindx,3) .* nmpix,40, 'o', 'filled'), hold on;
    set(gca,'Box', 'on', 'LineWidth', 1, 'FontSize', 16, 'FontName', 'Arial')
    [n, xout] = hist(C(cindx,2), [1:max(max(C))] ); 
    figure(clustfig)
    subplot(3,2,c), bar(xout,n ./ sum(n), 'k' ), title([ num2str(sum(n)) ' 2nd neighbors of cluster ' num2str(clust(c)) ])
    set(gca, 'YLim', [0 0.2], 'XLim', [ 0 max3d(C)+1 ] );
    
    figure(dist3fig)
    subplot(3,2,c), scatter3(X(cindx,1) .* nmpix,X(cindx,2) .* nmpix,X(cindx,3) .* nmpix,20, 'ok', ...
        'MarkerFaceColor', [0 0 0],'MarkerEdgeColor',[0 0 0 ], 'SizeData', 20 ) , hold on;
    subplot(3,2,c), scatter3(X(cindx,4) .* nmpix,X(cindx,5) .* nmpix,X(cindx,6) .* nmpix,20, 'o', ...
         'MarkerFaceColor', [1 0.8 0.2],'MarkerEdgeColor',[1 0.8  0.2], 'SizeData', 20 ), hold on;
    subplot(3,2,c), scatter3(X(cindx,7) .* nmpix,X(cindx,8) .* nmpix,X(cindx,9) .* nmpix,20, 'o', ...
        'MarkerFaceColor', [0.6706 0.7882 0.8902],'MarkerEdgeColor',  [0.6706 0.7882 0.8902], 'SizeData', 20 );
    set(gca, 'Box', 'on', 'LineWidth', 1, 'FontSize', 16, 'FontName', 'Arial' , 'XLim', lim.x(c,:) , 'YLim', lim.y(c,:) , 'ZLim', lim.z(c,:), ...
        'XTick',  lim.x(c,1):10:lim.x(c,2), 'YTick',  lim.y(c,1):10:lim.y(c,2), 'ZTick',  lim.z(c,1):10:lim.z(c,2), ...
        'View', [0 0], 'LineWidth',1);

    %subplot(3,2,3:6), scatter3(X(cindx,10) .* nmpix,X(cindx,11) .* nmpix,X(cindx,12) .* nmpix,40, 'o', 'filled'), hold on;
    %[p_cluster centroid sumd d] = kmeans(X(cindx,1:3) , 1, 'distance', 'Sqeuclidean');  
    %centr(c,:) = centroid;
    centr(c,:) = mean(X(cindx,1:3));
    NNDcluster(c).centr = centr(c,:);
    %NNDcluster(c).d = d;
    %NNDcluster(c).sumd = sumd;
    %NNDcluster(c).var = var(d);
    %NNDcluster(c).gamma = gamfit(d);
    cindxmotl = cindx .* find(indx ==1)';
    for ptcl = 1:size(cindxmotl,1)
        if cindxmotl(ptcl) ~= 0
            NNDcluster(c).iptcl = [NNDcluster(c).iptcl cindxmotl(ptcl)];
        end
    end 
   display( [ num2str(size(NNDcluster(c).iptcl,2)) ' of ' num2str(size(cindx,1)) ' ptcls in cluster ' num2str(c) ]);
end
figure(distfig), legend(num2str( clust' ), 'Location', 'Northeast');
 set(gca, 'XLim', [-40 40], 'YLim', [-40 40], 'ZLim', [-40 40], 'View', [0 90]); hold on
 scatter3(centr(:,1).* nmpix ,centr(:,2).* nmpix,centr(:,3).* nmpix, 200, 'kx', 'LineWidth', 2); hold off
clear  c p_cluster centroid  d sumd
%%

angfig = figure;
histfig = figure;
cmap = colormap('lines');

for c = 1:size(clust,2)
    [euler_indx euler_centroid] = av3_cluster_quaternions(euler_out(1:3,NNDcluster(c).iptcl), 5);
    
    %subplot(121), dendrogram(L); title('Dendrogram of Quaternion
    %clusters');
    figure(histfig)
    subplot(3,2,c), hist(euler_indx, max(euler_indx) ); %title( [ num2str( size(find(imode ==1),1) ./
    NNDcluster(c).quatcentr = euler_centroid;
    NNDcluster(c).iptclquat = euler_indx;
    %cindx = logical(C == c);
    figure(angfig)

    subplot(3,2,c), scatter3(euler_out(1,NNDcluster(c).iptcl), euler_out(2,NNDcluster(c).iptcl), euler_out(3,NNDcluster(c).iptcl), euler_indx, 'o', ...
        'MarkerFaceColor',cmap(c,:) ,'MarkerEdgeColor', cmap(c,:) , 'SizeData', 18 ); hold on;
    %subplot(3,2,c), scatter3(euler_out(4,NNDcluster(c).iptcl), euler_out(5,NNDcluster(c).iptcl), euler_out(6,NNDcluster(c).iptcl),euler_indx', 'og', 'filled', 'SizeData', 18 ); hold on;
    subplot(3,2,c), scatter3(euler_centroid(1), euler_centroid(2), euler_centroid(3), 'xk', 'LineWidth', 2, 'SizeData', 64 ); hold on; %title(['rel angles cluster ' num2str(clust(c))]);
    %subplot(3,2,c), scatter3(euler_centroid(4), euler_centroid(5), euler_centroid(6), 'cx', 'SizeData', 54 ); %title(['rel angles cluster ' num2str(clust(c))]);
    %subplot(3,2,c), scatter3(euler_centroid(4), euler_centroid(5),
    %euler_centroid(6), 'r', 'o', 'filled', 'SizeData', 54 ), 
    set(gca, 'LineWidth',1, 'XLim', [-10 370], 'YLim', [-10 370], 'ZLim', [-10 190], 'FontSize', 16, 'Box', 'on','View', [0 90]);
    title(['rel angles cluster ' num2str(clust(c))]);
    
    % [motl_out] = av3_nnd_replicate(NNDcluster, c, 12, 0.821);
    clear motl_out
    
end

%save NNDcluster_2euclCompl_2std.mat NNDcluster

%%

motl(20,:) = 0;
wedgelist = tom_emread('../../wedgelist.em'); wedgelist = wedgelist.Value;
for c = 1:4
    cindx  = find(NNDcluster(c).iptclquat == mode(NNDcluster(c).iptclquat));
    iptcl = NNDcluster(c).iptcl(cindx);
    motl(20,iptcl) = c;
    av = av3_average_exact(motl,'../../particles/particle', wedgelist,0.0,c);
    NNDcluster(c).av = av;
    %av = NNDcluster(c).av;
    
    tom_emwrite(['av_NNDclust_' num2str(c) '_filt3.em'], tom_filter(av, 3, 'circ'));
    figure
    tom_dspcub(tom_filter( NNDcluster(c).av(:,:,25:3:70), 2, 'circ') );
    set(gca, 'XTickLabel', [ ], 'YTickLabel', [ ] , 'XColor', 'k', 'YColor', 'k');
end
for c= 1
    motl_out = av3_nnd_replicate(NNDcluster, c, 12, 0.821, 'motl');
end

save NNDcluster_2dot_average0.7.mat NNDcluster

