function [ dist_mat ] = pa_create_distmat( motl )
%   PA_CREATE_DISTMAT Create distance matrix from motl
%   
%   INPUT:      motl                       motiflist
%
%   OUTPUT:     distance matrix            dist_mat
%

part_positions=zeros(size(motl,2),3);
for i=1:size(motl,2);
    part_positions(i,1)=motl(8,i)+motl(11,i);
    part_positions(i,2)=motl(9,i)+motl(12,i);
    part_positions(i,3)=motl(10,i)+motl(13,i);
end


% calculate distances
dist_mat=zeros(size(motl,2),size(motl,2));
for i=1:size(motl,2);
    for j=1:size(motl,2);
        if motl(5,i)==motl(5,j);
            dist_mat(i,j)=sqrt((part_positions(i,1)-part_positions(j,1))^2+(part_positions(i,2)-part_positions(j,2))^2+(part_positions(i,3)-part_positions(j,3))^2);
            if dist_mat(i,j)==0;
            dist_mat(i,j)=NaN;
            end
        else
            dist_mat(i,j)=NaN;
        end
    end
end

end

