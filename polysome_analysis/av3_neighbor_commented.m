function [p_shift_out, euler_out, rott_out] = av3_neighbor(motl, p, next, direction)
%
%
% AV3_NEIGHBOR calculates shifts of different reference points P,
% relative euler angles and rotation matrices between any particle in MOTL
% and its NEXT neighbours. The tomogram number is expected
% in line 5 of MOTL. 
%
%             INPUT
%               motl            20 x N motivelist
%               p                  3 x Q matrix with reference points, default [0 0 0]
%               next             number of neighbours to analyze, default 1
%               direction      directionality of next neighbors ('uni' or 'bi')
%
%             OUTPUT
%               p_shift_out  N x 3*Q matrix in pixels
%               euler_out     3 x N matrix in degrees
%               rott_out      N x 3*3 matrix)
%
%               Example:
%                   [p_shift_out euler_out rott_out] =
%                   av3_peakneighbour(motl, [0 0 0; 2 -17 -1;-22 1 2], 1);
%
%       
%
%    See Also
%           AV3_PEAKNEIGHBOUR_KMEANS, AV3_PEAKNEIGHBOUR_EULER,
%           AV3_PEAKNEIGHBOUR_QUATERNIONS, DIST
%
%   last change 25/03/08 FBR
%
%   Copyright (c) 2008
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

%% checks of input arguments

if nargin < 4
    direction = 'bi';
end
if nargin < 3
    next = 1;
    display('Default: 1 next neighbor is analyzed'),
end
if nargin < 2
    p = [ 0 0 0];
    disp(char(['No shift given for reference point outside center of particle. Set to default: ' num2str(p) ]) )
end

%w = waitbar(0);


%% code starts here

p_shift_out = zeros(size(motl,2) , size(p,1) * size(p,2)* next);        
% size of p_shift_out is initalized: for each particle and N next particles shift of each coordinate of each reference point
% for each particle: for N next neighbours: for n reference points (e.g. center, big su, small su): SHIFT x_coordinate, y_coordinate, z_coordinate

rott_out = zeros(size(motl,2) , 3 * 3* next);
% initialize rott_out: for each particle for each neighbour: 3*3 rotation matrix corresponding to the angles saved in euler _out


euler_out = zeros(3* next, size(motl,2) );
% initalize euler_out: 
%for each particle: for N neighbours: phi,psi,the for rotation of neighbour respectively to orientation of central particle=orientation of template

for j = 1:size(motl,2)  % for all particles
    j_tom = logical(motl(5,:) == motl(5,j));        %set index j_tom = 1 for each particle in same tomogram as particle j
     d= j_tom .* dist([motl(8,j)+motl(11,j) motl(9,j)+motl(12,j) motl(10,j)+motl(13,j)] ...     %input of coordinates + shift in subvolume of particle j (exact position of particle j)
         , [motl(8,:) + motl(11,:) ; motl(9,:) + motl(12,:); motl(10,:)+motl(13,:) ]);  %input of coordinates + shift in subvolume of ALL particles (exact position of all particles)
     % multiplication with j_tom sets distance of particles in other tomograms = 0
     % d = distance of particle j to other particles in pixels
     
     d(d==0)=NaN;   % set 0 = NaN for sorting ascending procedure
     [d, j_next] = sort(d,2);   % sort distances in ascending manner and save index of next neighbour in j_next
     next_motl = [motl(:,j) motl(:,j_next(1:next))]; % write next_motl with particle j and 2 next neighbours
     
     for k = 1:next     % for next next neighbours
         if k> 1
             switch lower(direction)        % strings to lower cas string
                 case 'bi'                  % analysis case 'bi'
                     rots = [ next_motl(17,k+1) next_motl(18,k+1) next_motl(19, k+1); -next_motl(18,1) -next_motl(17,1) -next_motl(19,1)];
                     [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]);
                     rot = mod(real(rot),360);
                     euler_out( (k*3-2):(k*3), j) = rot';
                     rott_out( j , (k*9-8):(k*9) ) = rott(:)';
                     clear rots rot rott shift
                     
                     shift_out = [ ];
                     for q = 1:size(p,1)
                         p_rot = tom_pointrotate(p(q,:), next_motl(17,k+1), next_motl(18,k+1), next_motl(19, k+1));
                         shift = [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                         shift = tom_pointrotate(shift, -next_motl(18,1), -next_motl(17,1), -next_motl(19,1));
                         shift_out = [shift_out shift];
                         clear shift
                     end
                     
                 case 'uni'                  % analysis case 'uni'
                     rots = [ next_motl(17,1) next_motl(18,1) next_motl(19, 1); -next_motl(18,k+1) -next_motl(17,k+1) -next_motl(19,k+1)];
                     [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]);
                     rot = mod(real(rot),360);
                     euler_out( (k*3-2):(k*3), j) = rot';
                     rott_out( j , (k*9-8):(k*9) ) = rott(:)';
                     clear rots rot rott shift
                     
                     shift_out = [ ];
                     for q = 1:size(p,1)
                         p_rot = tom_pointrotate(p(q,:), next_motl(17,1), next_motl(18,1), next_motl(19, 1));
                         shift = - [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                         shift = tom_pointrotate(shift, -next_motl(18,k+1), -next_motl(17,k+1), -next_motl(19,k+1));
                         shift_out = [shift_out shift];
                         clear shift
                     end
             end
         else
             rots = [ next_motl(17,k+1) next_motl(18,k+1) next_motl(19, k+1); -next_motl(18,1) -next_motl(17,1) -next_motl(19,1)];  
             % rots contains angles of next neigbour of particle j and negative rots of particle j ????? PSI, PHI, the for j ????
             
             [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]); 
             % function gives back rotation of particle j to neighbour in rot (euler angles), empty shift (=0) and rotation_matrix rott for rotation
             
             rot = mod(real(rot),360);      % modulo calculation to fix angles between 0 and 360 degrees
             euler_out( (k*3-2):(k*3), j) = rot';       % save phi, psi, the at pos 1-3 in euler_out for particle j, neighbour 1
             rott_out( j , (k*9-8):(k*9) ) = rott(:)';       % save rotation matrix at pos 1-9 in rott_out for particle j, neighbour 1
             clear rots rot rott shift
             
             shift_out = [ ];           % initialize shift_out
             for q = 1:size(p,1)        % for each reference point
                 p_rot = tom_pointrotate(p(q,:), next_motl(17,k+1), next_motl(18,k+1), next_motl(19, k+1)); 
                 % rotate coordinates of reference point to orientation of next neighbour
                 shift = [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                 % calculate coordinate shift of centers of particle j and next neighbour and add shifts resulting from rotation of next neighbour
                 shift = tom_pointrotate(shift, -next_motl(18,1), -next_motl(17,1), -next_motl(19,1));
                 % include rotation of particle j in calculation (rotate shifts)
                 shift_out = [shift_out shift];
                 % save shift of coordinates of refernce point k between particle j and next neighbour 1 to shift out
                 clear shift
             end
         end
         %size of p can vary, indices of p_shift_out must be adapted with q*3-q*3+1
         p_shift_out(j, (k*q*3-q*3+1):(k*q*3)  ) = shift_out;
         % save shifts of coordinates of refernce points between particle j and next neighbour k to p_shift_out
         clear shift_out q p_rot
         
     end
     clear j j_next j_tom k d next_motl
     
end

