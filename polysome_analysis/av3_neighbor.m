function [p_shift_out, euler_out, rott_out] = av3_neighbor(motl, p, next, direction)
%
%
% AV3_NEIGHBOR calculates shifts of different reference points P,
% relative euler angles and rotation matrices between any particle in MOTL
% and its NEXT neighbours. The tomogram number is expected
% in line 5 of MOTL. 
%
%             INPUT
%               motl            20 x N motivelist
%               p                  3 x Q matrix with reference points, default [0 0 0]
%               next             number of neighbours to analyze, default 1
%               direction      directionality of next neighbors ('uni' or 'bi')
%
%             OUTPUT
%               p_shift_out  N x 3*Q matrix in pixels
%               euler_out     3 x N matrix in degrees
%               rott_out      N x 3*3 matrix)
%
%               Example:
%                   [p_shift_out euler_out rott_out] =
%                   av3_peakneighbour(motl, [0 0 0; 2 -17 -1;-22 1 2], 1);
%
%       
%
%    See Also
%           AV3_PEAKNEIGHBOUR_KMEANS, AV3_PEAKNEIGHBOUR_EULER,
%           AV3_PEAKNEIGHBOUR_QUATERNIONS, DIST
%
%   last change 25/03/08 FBR
%
%   Copyright (c) 2008
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

if nargin < 4
    direction = 'bi';
end
if nargin < 3
    next = 1;
    display('Default: 1 next neighbor is analyzed'),
end
if nargin < 2
    p = [ 0 0 0];
    disp(char(['No shift given for reference point outside center of particle. Set to default: ' num2str(p) ]) )
end

%w = waitbar(0);

p_shift_out = zeros(size(motl,2) , size(p,1) * size(p,2)* next);
rott_out = zeros(size(motl,2) , 3 * 3* next);
euler_out = zeros(3* next, size(motl,2) );

for j = 1:size(motl,2)
    %waitbar(j / size(motl,2), w, char(['Particle ' num2str(j)]));
    % j_tom chooses only particles + coordinates of the same tomogram
    j_tom = logical(motl(5,:) == motl(5,j));
     d= j_tom .* dist([motl(8,j)+motl(11,j) motl(9,j)+motl(12,j) motl(10,j)+motl(13,j)] ...
         , [motl(8,:) + motl(11,:) ; motl(9,:) + motl(12,:); motl(10,:)+motl(13,:) ]);
     
     d(d==0)=NaN;
     [d, j_next] = sort(d,2);
     next_motl = [motl(:,j) motl(:,j_next(1:next))];
     
     for k = 1:next
         % if more than 1 neighbors are assessed, the directionality of the
         % 2nd NND can be reversed ('bi' (default) NND(j-->k), 'uni':
         % NND(k-->j)
         if k> 1
             switch lower(direction)
                 case 'bi'
                     rots = [ next_motl(17,k+1) next_motl(18,k+1) next_motl(19, k+1); -next_motl(18,1) -next_motl(17,1) -next_motl(19,1)];
                     [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]);
                     % rot = [atan2(rott(1,3),-rott(2,3)) acos(rott(3,3)) atan2(rott(3,1), rott(3,2))];
                     rot = mod(real(rot),360);
                     euler_out( (k*3-2):(k*3), j) = rot';
                     rott_out( j , (k*9-8):(k*9) ) = rott(:)';
                     clear rots rot rott shift
                     
                     shift_out = [ ];
                     for q = 1:size(p,1)
                         p_rot = tom_pointrotate(p(q,:), next_motl(17,k+1), next_motl(18,k+1), next_motl(19, k+1));
                         shift = [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                         shift = tom_pointrotate(shift, -next_motl(18,1), -next_motl(17,1), -next_motl(19,1));
                         shift_out = [shift_out shift];
                         clear shift
                     end
                     
                 case 'uni'
                     rots = [ next_motl(17,1) next_motl(18,1) next_motl(19, 1); -next_motl(18,k+1) -next_motl(17,k+1) -next_motl(19,k+1)];
                     [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]);
                     % rot = [atan2(rott(1,3),-rott(2,3)) acos(rott(3,3)) atan2(rott(3,1), rott(3,2))];
                     rot = mod(real(rot),360);
                     euler_out( (k*3-2):(k*3), j) = rot';
                     rott_out( j , (k*9-8):(k*9) ) = rott(:)';
                     clear rots rot rott shift
                     
                     shift_out = [ ];
                     for q = 1:size(p,1)
                         p_rot = tom_pointrotate(p(q,:), next_motl(17,1), next_motl(18,1), next_motl(19, 1));
                         shift = - [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                         shift = tom_pointrotate(shift, -next_motl(18,k+1), -next_motl(17,k+1), -next_motl(19,k+1));
                         shift_out = [shift_out shift];
                         clear shift
                     end
             end
         else
             rots = [ next_motl(17,k+1) next_motl(18,k+1) next_motl(19, k+1); -next_motl(18,1) -next_motl(17,1) -next_motl(19,1)];
             [rot shift rott]= tom_sum_rotation(rots, [0 0 0 ; 0 0 0]);
             % rot = [atan2(rott(1,3),-rott(2,3)) acos(rott(3,3)) atan2(rott(3,1), rott(3,2))];
             rot = mod(real(rot),360);
             euler_out( (k*3-2):(k*3), j) = rot';
             rott_out( j , (k*9-8):(k*9) ) = rott(:)';
             clear rots rot rott shift
             
             shift_out = [ ];
             for q = 1:size(p,1)
                 p_rot = tom_pointrotate(p(q,:), next_motl(17,k+1), next_motl(18,k+1), next_motl(19, k+1));
                 shift = [next_motl(8,k+1)-next_motl(8,1) next_motl(9,k+1)-next_motl(9,1) next_motl(10,k+1)-next_motl(10,1)] + p_rot;
                 shift = tom_pointrotate(shift, -next_motl(18,1), -next_motl(17,1), -next_motl(19,1));
                 shift_out = [shift_out shift];
                 clear shift
             end
         end
         %size of p can vary, indices of p_shift_out must be adapted with q*3-q*3+1
         p_shift_out(j, (k*q*3-q*3+1):(k*q*3)  ) = shift_out;
         clear shift_out q p_rot
         
     end
     clear j j_next j_tom k d next_motl
     
end

