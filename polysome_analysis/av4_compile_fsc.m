function [res] = av4_compile_fsc(avlog, apix, R)

% AV4_COMPILE_FSC
% FBR 2009

if nargin < 3
    R = 50;
end


for c = 1:size(avlog,2)
    figure;
    fscinterp = interp(avlog(c).fsc(:,2),R); freqinterp =  interp(avlog(c).fsc(:,1),R);
    ihalf= find(abs(fscinterp -0.5) == min(abs(fscinterp - 0.5))); 
    res =   apix / freqinterp(ihalf) ;
    plot(avlog(c).fsc(:,1), avlog(c).fsc(:,2),'-+', 'LineWidth',2  );
    legend(['FSC class ' num2str(c) ] );
    title(['Average class ' num2str(c) ' Resolution at 0.5 FSC: ' num2str(round(res)) ' A' ]);
end