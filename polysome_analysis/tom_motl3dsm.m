function tom_motl3dsm(in, tom)

%   TOM_MOTL3DSM writes a motivelist.txt in -ASCII to read in 3DSmax. If given, only ptcls from tomogram <itom> (line 5) are used. Columns are
%   converted to lines.
%   
%   IN:     in(str)    motivelist name, omit '.em'
%           tom(num)   tomogram number 
%
%
%   Example: 
%       tom_motl3dsm('MOTL_70S_3.em',1);
%
%   last change 08/08/07 FBR
%
%   Copyright (c) 2007
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom


%in = 'subMOTL_x71_49_y100_58';
motl = tom_emread(char([in '.em']));
motl = motl.Value;
motl = motl(:, find (motl(20,:) ~= 0));

if nargin < 2
    itom = 1:size(motl,2); tom = 99;
    display('All tomograms included.');
else
    itom = find(motl(5,:) == tom);
    if size(itom,2) <1
        error('Error message: Tomogram running no. not found in motivelist.');
    end
end

motl = motl(:,itom)';
out = char([in '_' num2str(tom) '.txt' ]);
save(out, 'motl', '-ascii')
disp(['ascii file saved:  ' out]);