function euler_new = av3_quat2eul(quat)

%AV3_EUL2QUAT converts S quaternions s to Euler angle set according to zxz
%rotation convention.
%
%       INPUT
%       quat     N x (4 x S) [q0 qx qy qz]
%      
%      OUTPUT
%       euler        (3x S) x N  list of Euler angles%
%
%       Example:
%
%         euler  = av3_quat2eul(quaternions);
%
%
%    See Also
%     AV3_QUAT2EUL, AV3_CLUSTER_QUATERNIONS, ATAN2
%
%    05/12/05 FBR
%   last change 16/04/09 FBR
%
%   Copyright (c) 2008
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom



euler_new = [ ];

for iS = 1: size(quat,2) / 4
    
    q0 = quat(:,iS * 4-3  );
    qx = quat(:,iS * 4-2  );
    qy = quat(:,iS * 4-1  );
    qz = quat(:,iS * 4      );
 
   the = atan2( 2 .* sqrt((qx.^2 +qy.^2)  .* (q0.^2 + qz .^2) ) , (q0.^2- qx.^2- qy.^2+ qz.^2));
   psi = atan2( 2 .* (qx .*qz + q0 .*qy) ./ sin(the) , (-2 .*(qy .*qz - q0 .*qx) ./sin(the)));
   phi = atan2( 2 .* (qx .*qz - q0 .*qy) ./ sin(the) , ( 2 .*(qy .* qz + q0 .*qx) ./sin(the)));
    


    euler = [phi psi the] .* 180 ./pi;
    
    euler(:,1:2 ) = mod(euler(:,1:2), 360);
    euler(:, 3) = mod(euler(:, 3), 180); 
    euler_new = [euler_new; euler'];
    
end

