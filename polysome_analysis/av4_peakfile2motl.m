function av4_peakfile2motl(iter, motl)

%AV4_PEAKFILE2MOTL parses av4 alignment parameters and particle indices
%from result files and extracts shifts of iteration <iter>, angles and class indizes to a pre-alignment motivelist <motl> containing original coordinates from tomograms.
%Writes a new motivelist <motl>_av4<iter>.em
%
%       IN:   iter          iteration no. STRING !
%              motl         filename
%
%
%
% Example:
%           
%               av4_peakfile2motl('009', 'MOTL_full_all.em')
%
% FBR 2008
%
%%
%motivelist = './MOTL_class2_009.em';
motivelist = '../../MOTL_full_all.em';
motl_in = tom_emread(motivelist); 
motl_in = motl_in.Value;
% particles reset to class 0
iter = '009';
path = './results/';
[avlog, res ] = av4_parse_avg(path, iter, 8.21, 100);
%load('../../rec_sub.mat');
%wedgelist = zeros(3, max(motl_in(5,:)) );
%wedgelist(1,:) = min(motl_in(5,:)) : max(motl_in(5,:));
%wedgelist(2,:) = ceil(min(Rec3dProject.PARAMETER.Tiltangles));
%wedgelist(3,:) = ceil(max(Rec3dProject.PARAMETER.Tiltangles));

wedgelist = tom_emread('../../wedgelist.em'); wedgelist = wedgelist.Value;

%%
[peakfile]= tom_mex_parse_av4('parse_peakfile', ['./results/' iter '_peakfile'], 2, size(find(motl_in(20,:) ==1), 2 ) );
 %al
%in case indexes in particle file are not in same order as MOTL, use this:
% fid = fopen('particles_list4matlab.txt');
% C = textscan(fid, ' ./particles_48/particle_pflip_%s');
% fclose(fid)
% C = strrep(C{:}, '.em', '');
% all_ptcl = [ ];
% for jj = 1:size(C,1)
%     all_ptcl = [all_ptcl str2num(C{jj})];
% end
% clear C fid jj
%%
for c = 1:size(avlog,2)
    %z =0;
    motl_out = motl_in(:, logical(motl_in(20,:) ~= 0) );
    motl_out(20,:) =0;
    c_indx = avlog(c).use_idx +1;
    %zufall = c_indx( randi([1 size(c_indx,2)], 100, 1));
    s = 0;
    for iptcl = 1:size(peakfile,2)
        motl_out(1,iptcl) = peakfile(c, iptcl).ccval;
        xyz = peakfile(c, iptcl).shift';
        eul = peakfile(c,iptcl).angles';
        motl_out(11:13, iptcl) = xyz;
        motl_out(17:19, iptcl) = eul;
    end
    %    c_indx = c_indx(101:200);
    % for iptcl = all_ptcl(c_indx)
    motl_out(20,c_indx) = c;
    
    av = av3_average_exact(motl_out ,'../../particles_48/particle', wedgelist,0.0,c);
    figure; tom_dspcub(av)
    %figure; tom_dspcub(s)
    tom_emwrite(['ref_' num2str(iter) '_class' num2str(c) '.em'], av)
    tom_emwrite(['MOTL_class' num2str(c) '_' num2str(iter) '.em'], motl_out);
   
end


