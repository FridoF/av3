function mask3d = tom_proj2d3d(vol, axis);

%create_vol_with_mask.inp
%project vol to 'axis' plane, backproject mask
%into 3d volume. Suitable to create a volume from an experimental MOTL
%to randomly distribute XCF peaks for a simulated MOTL
% This function projects the 3-D Volume in a axis-oriented projection. In
% other words one can aquire the sum of the volume in the xy,xz or yz
% direction.
%
%   Example
%   ----------
% 
%   x=randn(3,3,3);
%   prj=tom_project2d(x,'xy');
%
%
%   14/11/05  AL
%
%   Copyright (c) 2004
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom

[dim1 dim2 dim3]=size(vol);

switch (axis)

    case 'xy'

        m=sum(vol,3); clear vol
        mask3d = repmat(m, [1 1 dim3]);

    case 'yz'
        vol=permute(vol,[3 2 1]);
        m=sum(vol,3); clear vol
        mask3d = permute(repmat(m, [1 1 dim1]),[3 2 1]) ;

    case 'xz'
        vol=permute(vol,[3 1 2]);
        m=sum(vol,3); clear vol
        mask3d = repmat(m, [1 1 dim2]);

    otherwise
        error('Unknown orientation');
end

end

