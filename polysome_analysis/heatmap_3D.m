function heatmap = heatmap_3D(shift_data, class_data, class, cube_size, nmpix)

X = shift_data;
C = class_data;

cube_dist=zeros([round(300/cube_size) round(300/cube_size) round(300/cube_size)]);


for i=1:size(X,1)
    
    if C(i)==class
    
    if X(i,1)<0
        
        x=floor(X(i,1)*nmpix/cube_size)+size(cube_dist,1)/2+1;
       
    else
        
        x=ceil(X(i,1)*nmpix/cube_size)+size(cube_dist,1)/2;
        
    end
    
    
    if X(i,2)<0
        
       y=floor(X(i,2)*nmpix/cube_size)+size(cube_dist,1)/2+1;
       
    else
        
       y=ceil(X(i,2)*nmpix/cube_size)+size(cube_dist,1)/2;
        
    end
    
    
    if X(i,3)<0
        
       z=floor(X(i,3)*nmpix/cube_size)+size(cube_dist,1)/2+1;
       
    else
        
       z=ceil(X(i,3)*nmpix/cube_size)+size(cube_dist,1)/2;
        
    end
    
    cube_dist(x,y,z)=cube_dist(x,y,z)+1;
    
    end

end



%% Standardise

for x=1:size(cube_dist,1);
    
   for y=1:size(cube_dist,1);
    
        for z=1:size(cube_dist,1);
    
                cube_dist(x,y,z)=cube_dist(x,y,z)/size(X,1);
    
               
    
        end
    
    
    end
    
end


%% rotate cube_dist to xy-plane

cube_dist_single=single(cube_dist);
angles = [-3 0 -55];
cube_dist_rot=tom_rotate(cube_dist_single, angles, 'linear');
heatmap = cube_dist_rot;

end