function tom_detail_imwrite( volume, offset, increment, detail)

% TOM_DETAIL_IMWRITE writes .tiff images from subtomogram limited by
% coordinates in detailstruct.
%
%    IN             volume          filename.em
%                    offset            range of slices, fraction of total
%                    increment      increment of slices
%                    detail             detailstruct
%     OUT
%                   .tiff images written in same directory as <volume>
%                    
%
%
%
%  Example:
%                   tom_detail_imwrite('../subvol1.em.norm', 0.9, 1);
%
%
% FBR 2009
%
%
%
%
%

vol = tom_emread(volume);
vol = vol.Value;

if nargin < 4
    detail.x1= 1 ; detail.x2 = size(vol,1) ;
    detail.y1 =1 ; detail.y2 =  size(vol,2) ;
    detail.z1 =1 ; detail.z2 = size(vol,3) ;  
end



for j = 1:size(detail,2)
    vol_detail = vol(detail(j).x1 : detail(j).x2, detail(j).y1 : detail(j).y2, detail(j).z1: detail(j).z2);
    vol_detail = tom_norm(tom_filter(tom_norm(vol_detail, '3std'), 2, 'circ'), 255);
    zsize = floor(size(vol_detail,3) / 2) +1;
    slice = zsize-floor((zsize-1) * offset) : increment: zsize + floor((zsize-1) * offset);
    
    for islice = 1:size(slice,2)
        
        image=vol_detail(:,:,slice(islice));% tom_imagesc(image);
        if nargin < 4
            imwrite(uint8(image)', char([ volume '_z' num2str(slice(islice)) 'hires.tif']) , 'Resolution', [500 500]);
            display(  [ volume '_z' num2str(slice(islice)) 'hires.tif written.' ] );
        else
            imwrite(uint8(image)', char([ volume '_detail' num2str(j) '_z' num2str(slice(islice)) 'hires.tif']) , 'Resolution', [500 500]);
            display(  [ volume '_detail' num2str(j) '_z' num2str(slice(islice)) 'hires.tif written.' ] );
        end
    end
end