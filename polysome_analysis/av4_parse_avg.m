 function [avlog, res] = av4_parse_avg(path, iter, apix, R)

% AV4_COMPILE_FSC
% FBR 2009

if nargin < 4
    R = 50;
end

[avlog]= tom_mex_parse_av4('parse_average_log', [path iter '_log_average']);

for c = 1:size(avlog,2)
    figure;
    [avlog0]= tom_mex_parse_av4('parse_average_log', [path '000_log_average']);
    fscinterp = interp(avlog(c).fsc(:,2),R); freqinterp =  interp(avlog(c).fsc(:,1),R);
    ihalf= find(abs(fscinterp -0.5) == min(abs(fscinterp - 0.5))); 
    res =   apix / freqinterp(ihalf) ;
    plot(avlog0(c).fsc(:,1), avlog0(c).fsc(:,2),'--k', 'LineWidth',2  ); hold on
    plot(avlog(c).fsc(:,1), avlog(c).fsc(:,2),'-+k', 'LineWidth',2  ); hold on
    plot( [ freqinterp(ihalf) freqinterp(ihalf)] , [0 0.5], '--r');
    set(gca, 'XTick', apix/10 ./ [20 10  6 4 3 2]  , 'XTickLabel', [20 10  6 4 3 2]  )
    legend( 'FSC iteration 000', ['FSC iteration ' num2str(iter)]   );
    title(['Average class ' num2str(c) ' Resolution at 0.5 FSC: ' num2str(round(res)) ' A' ]);
end