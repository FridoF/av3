function [motl_out] = av3_nnd_replicate_woplot(NNDcluster, clust, iter, refname, refnmpix, flag)


%   AV3_NND_REPLICATE reads a next neighbor distribution struct (NNDstruct,
%   MAT-file), reads coordinate and angle cluster centroids (c no.) and
%   replicates the relative configurations n times (iter). Give nmpix to
%   adjust px distance to physical value.
%
%
%   Example:
%   [motl_out] = av3_nnd_replicate('NNDcluster', 1, 1, '../avg4mtfbp3n.em', 0.279, 'vol');
%
%   Feb 2009 FBR
%   last change 03/03/09 FBR
%
%   Copyright (c) 2009
%   TOM toolbox for Electron Tomography
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/tom
%%

if nargin < 6
    flag = 'none';
end

% if nargin < 4
%     nmpix =1;
% end

if ischar(NNDcluster) ==1
    load(NNDcluster);
end

%
% testing arguments
%NNDcluster = 'NNDcluster_2euclCompl_2std';
%iter =12;
%refnmpix = 0.821;
%clust = [1 2];
%rots_in = [96.55  351.7  48.25];
%shift_in = [ -27.5144     -13.9917     -7.91043]  *0.821;
nmpix = NNDcluster(clust).nmpix;
motl_out = zeros(20, iter .* size(clust,2));
motl_out(1,:) = 1;
motl_out(4,1) =1;
motl_out(20,1) =1;
%fid = fopen('biomt.txt','w');
%fprintf(fid,'REMARK 350 BIOMOLECULE: 1 \n');
if strcmp(flag, 'vol') == 1
    if strcmp(refname(end-2:end), 'mrc') == 1
        ref = tom_mrcread(refname); ref  = tom_norm(ref.Value, 1);
    else
        ref = tom_emread(refname); ref = tom_norm(-ref.Value,1);
    end
    dims = size(ref);
    vol = zeros(dims * (iter+1)); vol(:) = ref(1);
    refrot = ref .* tom_sphere(size(ref), floor(size(ref,1) ./ 2 -1), 3);
    vol = tom_paste2(vol, refrot, ceil(size(vol)./2 +1 + motl_out(8:10,1)' -  dims/2 +1), 'max');
end


for it = 1:iter
    for c = 1:size(clust,2)
        imotl = (it-1) .* size(clust,2) + c  +1 ;
        rot_in = NNDcluster(clust(c)).quatcentr(1,:);
        shift_in = NNDcluster(clust(c)).centr(1,:) .* nmpix ./ refnmpix;
        eulers = [motl_out(17:19,imotl -1)';  rot_in];
        %shifts = [ motl_out(8:10,imotl -1);  shift_in];
        %[euler_out shift_out rott_out]=tom_sum_rotation(eulers, shifts );
        
        [euler_out shift_discard rott_out]=tom_sum_rotation(eulers, zeros(size(eulers)) );
        clear shift_discard
        motl_out(17:19,imotl) = euler_out';
        shift_out = [ motl_out(8:10,imotl-1)' + tom_pointrotate(shift_in, motl_out(17,imotl-1), motl_out(18,imotl-1), motl_out(19,imotl-1) )] ;
        motl_out(4, imotl) = imotl;
        motl_out(5,imotl) = it;
        motl_out(8:10,imotl) = shift_out; 
        motl_out(20,imotl) = clust(c);
        
        %mat_out(:,:,imotl) = rott_out;
        if strcmp(flag, 'vol') == 1
            refrot = tom_rotate(ref,  motl_out(17:19,imotl)') .*  tom_sphere(size(ref), floor(size(ref,1) ./ 2 -1), 3);
                vol = tom_paste2(vol, refrot, ceil(size(vol)./2 +1 + motl_out(8:10,imotl)' -  dims/2 +1), 'max');
        end
%      fprintf(fid,'REMARK 350   BIOMT1 %3.0g %8.6f %8.6f %8.6f %8.6f \n',[ it mat_out(1,:,it) motl_out(8,it)]);
%      fprintf(fid,'REMARK 350   BIOMT2 %3.0g %8.6f %8.6f %8.6f %8.6f \n',[ it  mat_out(2,:,it) motl_out(9,it)]);
%      fprintf(fid,'REMARK 350   BIOMT3 %3.0g %8.6f %8.6f %8.6f %8.6f \n',[ it mat_out(3,:,it) motl_out(10,it)]);
    end
end
%fclose(fid);
%
%!cat biomt.txt ref_009_class2.pdb > ref_009_class2new.pdb
% figure;
% plot3(motl_out(8,:), motl_out(9,:),motl_out(10,:), '-ok' );

switch flag
    case 'none'
        disp('No output generated. Choose "motl" or "vol" as output flag.')
    case 'motl'
        tom_emwrite( ['MOTL_' num2str(clust) '_replicate' num2str(iter) '.em'], motl_out);
        tom_motl3dsm(  ['MOTL_' num2str(clust) '_replicate' num2str(iter) ] );
    case 'vol'
        tom_emwrite( ['MOTL_' num2str(clust) '_replicate' num2str(iter) '.em'], motl_out);
        tom_motl3dsm(  ['MOTL_' num2str(clust) '_replicate' num2str(iter) ] );
        %if strcmp(ref(end-2:end), 'mrc') == 1
            tom_mrcwrite( single(vol), 'name', ['vol_clust' num2str(clust) '_replicate' num2str(iter) '.mrc' ]);
            disp( ['vol_clust' num2str(clust) '_replicate' num2str(iter) '.mrc written in current directory.' ])
%         else
%             tom_emwrite(  ['vol_clust' num2str(clust) '_replicate' num2str(iter) '.em' ], vol);
        end
end



    