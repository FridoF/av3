%%  script to update paths in Rec3dStruct

load rec_poly.mat

Rec3dProject.PROJECT.MarkerfilePath1 = [pwd '/'];
Rec3dProject.PROJECT.ProjectionsPath1 = [pwd '/sort/'];

for indx = 1:size(Rec3dProject.PARAMETER.ProjectionsPath, 2)
    Rec3dProject.PARAMETER.ProjectionsPath{indx} =  [pwd '/sort/'];
    Rec3dProject.PARAMETER.ProjectionsPathOriginal{indx} =  [pwd '/sort/'];
end

save rec_poly.mat Rec3dProject