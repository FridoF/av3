function tom_imod2tom_rec3d(projectfile)

% TOM_IMOD2TOM_REC3D reads a Rec3D .mat project file nad modifies struct
% to proceed with reconstruction in tom_Rec3dGUI.
%
%
%
%   Example: tom_imod2tom_rec3d('rec_full.mat');
%
% last change
% FBR 07/12/19

load(projectfile);

prj = size(Rec3dProject.PARAMETER.ProjectionsName,2);
Rec3dProject.PARAMETER.Tiltangles = Rec3dProject.PROJECT.Markerfile1.Value(1,:,1);
Rec3dProject.PARAMETER.Tiltaxis = zeros(1,prj);
Rec3dProject.PARAMETER.ProjDir = zeros(1,prj);
Rec3dProject.PARAMETER.tx = zeros(1,prj);
Rec3dProject.PARAMETER.ty = zeros(1,prj);
Rec3dProject.PARAMETER.isoscale = ones(1,prj);
Rec3dProject.ProjectStatus = 'aligned';

save(projectfile, 'Rec3dProject')
display(char([ projectfile ' modified and written.']))
