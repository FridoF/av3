function align_paras = set_alig_paras(bandpass, smooth, radius, coords, ...
                        tiltaxis, lweight, anglescan)
%   align_paras = set_alig_paras(bandpass, smooth, radius, coords, tiltaxis, lweight)
%
%   bandpass   [low hi]
%   smooth     smmothing of bandpass
%   radius     radius of feature
%   coords     coordinates of feature in 0-deg projection
%   tiltaxis   tiltaxis
%   lweight    weighting of reconstructed vol (0=false, 1=true)
%   anglescan  scan tiltaxis by +/- anglescan in 0.25 deg increments
if nargin < 7
    anglescan = 0;
end;
if nargin < 6
    lweight = 0;
end;
if nargin < 5
    tiltaxis = -74;
end;
if nargin < 5
    tiltaxis = -74;
end;
if nargin < 4
    coords = [1 2048 1 2048];
end;
if nargin <3
    smooth = 10;
end;
if nargin <2
    smooth = 512;
end;
if nargin < 1
    bandpass = [0,.5];
end;
align_paras = struct('bandpass',bandpass,'radius',radius, 'smooth',smooth,...
                'coords',coords,'tiltaxis',tiltaxis,'lweight',lweight, ...
                'anglescan',anglescan);




