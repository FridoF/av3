function out = av3_shrink(in, idim, newlen)
%
fy = tom_fourier(in);
if idim ==1    
    newarr = fy(1:newlen/2,:);
    newarr(newlen/2+1,:)=0;
    newarr(newlen/2+2:newlen,:)=fy(size(fy,1)-newlen/2+2:size(fy,1),:);
end;
out = tom_ifourier(newarr); 
out = real(out)
