function [shifts, sumccc, vol] = av3_proj_alig_iter(fname, append, ...
                                    izerotilt, align_paras, prevshifts)
%   [shifts, sumccc, vol] = av3_proj_alig(fname, append, izerotilt,...
%                               align_paras)
%   INPUT
%   fname        filename (e.g. 'ves1')
%   append       appendix of em files (e.g., '.em')
%   izerotilt    file index of zero tilt projection
%   align_paras  alignment parameters (from set_alig_paras)
%
%   OUTPUT
%   shifts       array with shifts
%   sumccc       sum of all correlations
%   vol          reconstructed volume (1x binned)
%
%   FF 07/27/09
global vol;global mask; global bandpass; 
global shifts;global mask2d;global cumproj;
global nvox; global npix;global ccmask;

shifts=prevshifts;
%prepare masks 
dim = 2*(align_paras.radius+int16(align_paras.radius/5));
imdim = 1024;
bandpass = tom_spheremask(ones(imdim,imdim,1),...
            double(align_paras.bandpass(2)*align_paras.radius),...
            double(align_paras.smooth*align_paras.radius));
bandpass = bandpass - tom_spheremask(ones(imdim,imdim,1),...
            double(align_paras.bandpass(1)*align_paras.radius),...
            double(align_paras.smooth*align_paras.radius));
mask2d = tom_spheremask(ones([imdim,imdim,1],'single'),...
            double(align_paras.radius)+double(align_paras.radius)/10,...
            double(align_paras.radius)/15);
npix = sum(sum(mask2d));
ccmask = tom_spheremask(ones([imdim,imdim],'single'),imdim/7,imdim/28);
bandpass = ifftshift( squeeze(bandpass));
mask = tom_spheremask(ones([dim,dim,dim],'single'),align_paras.radius,...
            align_paras.radius/15);
nvox=sum(sum(sum(mask)));
%%%%%%%%%%%%

%read zero deg projection
proj0 = tom_emread([fname,'_',num2str(izerotilt),append]);
proj0 = tom_bin(proj0.Value);
proj0 = tom_smooth(proj0, 60,50,'zero');
proj = tom_rotate(proj0, [90+align_paras.tiltaxis 0 0]);
proj = tom_shift(proj, ...
        [imdim/2+1-align_paras.coords(1),imdim/2+1-align_paras.coords(2)]);
proj = mask2d.*proj;mn = sum(sum(proj))/npix;proj = proj-mn*mask2d;
tom_imagesc(proj);

%re-initialize volume and cumproj
cumproj= tom_paste(zeros([imdim,imdim],'single'),proj,...
    [int16((imdim-size(proj))/2)+1, ...
    int16((imdim-size(proj))/2)+1]);
if align_paras.lweight == 1
    proj = tom_ifourier(ifftshift(tom_weight3d('analytical', ...
        fftshift(tom_fourier(proj)))));
end;
vol = zeros(dim,dim,dim,'single');
tom_backproj3d(vol,single(proj), 0 , 0, [0 0 0]);

%%%%%% loop over tilts
nmiss=0;
sumccc=0;
for ii=izerotilt-1:-1:-999
    [fid, ccc] = proj_alig(fname, append, ii, izerotilt, align_paras);
    sumccc=sumccc+ccc;
    if fid==-1
        nmiss=nmiss+1;
        if nmiss >5
            break
        end;
    else
        nmiss=0;
    end;
end;
nmiss=0;
for ii=izerotilt+1:999
    [fid, ccc] = proj_alig(fname, append, ii, izerotilt, align_paras);
    sumccc=sumccc+ccc;
    if fid==-1
        nmiss=nmiss+1;
        if nmiss >5
            break
        end;
    else
        nmiss=0;
    end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fid, ccc] = proj_alig(fname, append, ii, izerotilt, align_paras)
%   
global vol;global mask; global bandpass; 
global shifts;global mask2d;global cumproj;
global nvox; global npix;global mask2d;global ccmask;

tname = [fname,'_',num2str(ii),append];
fid = fopen(tname,'r','ieee-be');
ccc = 0;
if fid~=-1
    fclose(fid);
    nmiss=0;
    proj = tom_emread(tname);
    tiltangle = proj.Header.Tiltangle;%choose tilt || y
    proj = tom_bin(proj.Value);
    proj = tom_smooth(proj, 50,40,'zero');
    disp([num2str(tiltangle), ' deg: file ',tname]);
    proj = tom_rotate(proj, [90+align_paras.tiltaxis 0 0]);
    oldshift = [shifts(ii,1)-size(proj,1)/2+1,...
            +shifts(ii,2)-size(proj,2)/2+1];
        
%     proj=tom_shift(proj,-oldshift);
    cp_proj = proj;
%     proj = mask2d.*proj;
%     mn = sum(sum(proj))/npix;
%     proj = proj-mn*mask2d;
    simproj = squeeze(sum(tom_rotate(mask.*vol,[90 -90 tiltangle]),3));
    simproj = mask2d.*tom_paste(zeros(size(proj,1),size(proj,2)),simproj,...
                [int16((size(proj,1)-size(simproj))/2)+1, ...
                 int16((size(proj,2)-size(simproj))/2)+1]);
%     mn = sum(sum(simproj))/npix;simproj=simproj-mn*mask2d;
    ccf = av3_local_corr(simproj, proj, mask2d, bandpass);
%     ccf = ccmask.*real(ifftshift(tom_ifourier( ...
%             bandpass.*(tom_fourier(proj).*conj(tom_fourier(simproj))) ...
%             )))/(size(simproj,1)*size(simproj,2));
    figure(1);tom_imagesc(simproj);
    figure(3);imagesc(ccf');
    [c ccc] = tom_peak(ccf,'spline');
%     shifts(ii,1)=c(1)+oldshift(1);shifts(ii,2)=c(2)+oldshift(2);
    newshift = [c(1)-size(proj,1)/2+1,c(2)-size(proj,2)/2+1];
    disp(['CCC=', num2str(ccc), '; new shift ', num2str(newshift(1)),...
            ' ', num2str(newshift(2)), '; old shift:',...
            num2str(oldshift(1)),' ',num2str(oldshift(2))]);
    if align_paras.lweight == 1
        ft_proj = tom_fourier(cp_proj);
        ft_proj = tom_shift_fft(ft_proj,-newshift);
        cumproj = cumproj + mask2d.*real(tom_ifourier(ft_proj));
        ft_proj = ifftshift(tom_weight3d('analytical',fftshift(ft_proj)));
        cp_proj = real(tom_ifourier(ft_proj)).*mask2d;
    else
        cp_proj = mask2d.*tom_shift(cp_proj, -newshift);
        cumproj = cumproj + cp_proj;
    end;
    figure(4);tom_imagesc(cumproj);
    tom_backproj3d(vol, single(cp_proj), 0, tiltangle, [0 0 0]);
end;