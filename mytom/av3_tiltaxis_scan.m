function tiltaxis = av3_tiltaxis_scan(fname, append, izerotilt, ...
                                    align_paras)
%   tiltaxis = av3_tiltaxis_scan(fname, append, izerotilt, ...
%                                   align_paras)
%   
%   INPUT
%   fname        filename (e.g. 'ves1')
%   append       appendix of em files (e.g., '.em')
%   izerotilt    file index of zero tilt projection
%   align_paras  alignment parameters (from set_alig_paras)
%
%   OUTPUT
%   shifts       array with shifts
%   sumccc       sum of all correlations
%   vol          reconstructed volume (1x binned)
%
%   FF 07/27/09

global vol;global mask; global bandpass; 
global shifts;global mask2d;global cumproj;
global nvox; global npix;global projmask;global nproj;

%prepare masks 
dim = 2*align_paras.radius+int16(align_paras.radius/5);
imdim = 1024;
bandpass = tom_spheremask(ones(imdim,imdim,1),...
            double(align_paras.bandpass(2)*align_paras.radius),...
            double(align_paras.smooth*align_paras.radius));
bandpass = bandpass - tom_spheremask(ones(imdim,imdim,1),...
            double(align_paras.bandpass(1)*align_paras.radius),...
            double(align_paras.smooth*align_paras.radius));
mask2d = tom_spheremask(ones([imdim,imdim,1],'single'),...
            double(align_paras.radius)+double(align_paras.radius)/10,...
            double(align_paras.radius)/15);
npix = sum(sum(mask2d));
bandpass = ifftshift( squeeze(bandpass));
mask = tom_spheremask(ones([dim,dim,dim],'single'),align_paras.radius,...
            align_paras.radius/15);
nvox=sum(sum(sum(mask)));
projmask = squeeze(tom_spheremask(ones([imdim, imdim,1],'single'),...
            double(align_paras.radius)+double(align_paras.radius)/10,...
            double(align_paras.radius)/15));
nproj=sum(sum(projmask));
%%%%%%%%%%%%

%read zero deg projection
proj0 = tom_emreadc([fname,'_',num2str(izerotilt),append],'subregion',...
            [align_paras.coords(1) align_paras.coords(3) 1],  ...
            [align_paras.coords(2)-1 align_paras.coords(4)-1 0]);
proj0 = tom_bin(proj0.Value);

shifts=[0,0];
cclist=zeros(2,9);
%%%%%% scan tilt angle
if align_paras.anglescan(3) > 0
    for tiltang=align_paras.tiltaxis+align_paras.anglescan(1):...
                align_paras.anglescan(3):...
                align_paras.tiltaxis+align_paras.anglescan(2)
        sumccc=0;
        align_paras.tiltaxis = tiltang;
        proj = tom_rotate(proj0, [90+tiltang 0 0]);
        cumproj= tom_paste(zeros([imdim,imdim],'single'),proj,...
            [int16((imdim-size(proj))/2)+1, ...
            int16((imdim-size(proj))/2)+1]);
        if align_paras.lweight == 1
            proj = tom_ifourier(ifftshift(tom_weight3d('analytical', ...
                fftshift(tom_fourier(proj)))));
        end;
        mn = sum(sum(proj))/npix;
        proj = tom_paste(zeros(size(cumproj,1),size(cumproj,2)),proj,...
            [int16((size(cumproj,1)-size(proj))/2)+1, ...
            int16((size(cumproj,2)-size(proj))/2)+1]) - ...
            mn*mask2d;
        vol = zeros(dim,dim,dim,'single');
        tom_backproj3d(vol,single(proj), 0 , 0, [0 0 0]);
        for ii=izerotilt-1:-1:izerotilt-4
            [fid, ccc] = proj_alig2(fname, append, ii, izerotilt, ...
                            align_paras, 0);
            sumccc=sumccc+ccc;
        end;
        for ii=izerotilt+1:izerotilt+4
            [fid, ccc] = proj_alig2(fname, append, ii, izerotilt, ...
                align_paras, 0);
            sumccc=sumccc+ccc;
        end;
        cclist(2,ii)=sumccc;
        cclist(1,ii)=tiltang;
        disp(['>>>>>>>>>> Tiltangle ',num2str(tiltang), ': Sum_cc=',...
            num2str(sumccc),' <<<<<<<<<<<<<']);
    end;
    [Y,I] = max(cclist(2,:));
    tiltaxis = cclist(1,I);
    align_paras.tiltaxis = tiltaxis;
else
    disp('Make sure align_paras.anglescan > 0');
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fid, ccc] = proj_alig2(fname, append, ii, izerotilt, ...
                        align_paras, ldisp)
%   
global vol;global mask; global bandpass; 
global shifts;global mask2d;global cumproj;
global nvox; global npix;global projmask;global nproj;

if nargin < 6
    ldisp=1;
end;
tname = [fname,'_',num2str(ii),append];
fid = fopen(tname,'r','ieee-be');
ccc = 0;
if fid~=-1
    fclose(fid);
    nmiss=0;
    proj = tom_emread(tname);
    tiltangle = proj.Header.Tiltangle;%choose tilt || y
    proj = tom_bin(proj.Value);
    disp([num2str(tiltangle), ' deg: file ',tname]);
    proj = tom_rotate(proj, [90+align_paras.tiltaxis 0 0]);
    cp_proj = proj;
    proj = mask2d.*proj;
    mn = sum(sum(proj))/npix;
    proj = proj-mn*mask2d;
    simproj = squeeze(sum(tom_rotate(mask.*vol,[90 -90 tiltangle]),3));
    simproj = projmask.*tom_paste(zeros(size(proj,1),size(proj,2)),simproj,...
                [int16((size(proj,1)-size(simproj))/2)+1, ...
                 int16((size(proj,2)-size(simproj))/2)+1]);
    mn = sum(sum(simproj))/nproj;simproj=simproj-mn*projmask;
    if ldisp ==1         
        figure(1);tom_imagesc(simproj);
        figure(2);tom_imagesc(proj);
    end;
    ccf = real(ifftshift(tom_ifourier( ...
            bandpass.*(tom_fourier(proj).*conj(tom_fourier(simproj))) ...
            )))/(size(simproj,1)*size(simproj,2));
    if ldisp ==1
        figure(3);imagesc(ccf');
    end;
    [c ccc] = tom_peak(ccf,'spline');
    shifts(ii,1)=c(1);shifts(ii,2)=c(2);
    if align_paras.lweight == 1
        ft_proj = tom_fourier(cp_proj);
        ft_proj = tom_shift_fft(ft_proj,-[c(1)-size(proj,1)/2+1,c(2)-size(proj,2)/2+1]);
        cumproj = cumproj + mask2d.*real(tom_ifourier(ft_proj));
        ft_proj = ifftshift(tom_weight3d('analytical',fftshift(ft_proj)));
        cp_proj = real(tom_ifourier(ft_proj)).*mask2d;
    else
        cp_proj = mask2d.*tom_shift(cp_proj,-[c(1)-size(proj,1)/2+1,c(2)-size(proj,2)/2+1]);
        cumproj = cumproj + cp_proj;
    end;
    if ldisp ==1
        figure(4);tom_imagesc(cumproj);
    end;
    disp(['CCC=', num2str(ccc), '; proj. shifted projection by ',... 
            num2str(-c(1)+size(proj,1)/2+1),...
            ' ', num2str(-c(2)+size(proj,2)/2+1)]);
    tom_backproj3d(vol, single(cp_proj), 0, tiltangle, [0 0 0]);
end;
