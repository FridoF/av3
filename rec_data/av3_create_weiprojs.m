function alignment = av3_create_weiprojs(projection_file, myext, weightedfile_name, ...
                filter, weighting, marker_file, alignopts, ibin, pixsize)
%   av3_create_weiprojs writes weighted projections to files
%
% av3_create_weiprojs(projection_file, myext, weightedfile_name, ...
%               filter, weighting, marker_file, alignopts, ibin, ...
%               pixsize)
%
%
%PARAMETERS
%
%   INPUT
%    projection_file            name of tiltseries [string]
%    myext                      extension (of file), e.g. '.em' [string]
%    weightedfile_name          Name of the weighted file
%    filter                     radius of low-pass filter in Nyquist
%                                   (max=1)
%    weighting                  estimate for diameter of object - 
%                                   if 0 'analytical' weighting is done,
%                                   if >0 'exact' according to
%                                   specified diameter (in pixels)
%                                   if < 0 no weighting
%    marker_file                name of markerfile [string]
%    alignopts                  alignment options (from av3_alignsetup)
%    ibin                       binning (default: 0)
%    pixsize                    pixel size of projections in nm (default:
%                                   0.5)
%
%   OUTPUT
%    alignment                  alignment of tilt series
%
%
%EXAMPLES
%
%REFERENCES
%
%SEE ALSO
%   av3_align_tiltseries, av3_alignsetup, tom_calc_weight_function
%
%   FF - 02/03/2011
%   last change: 
%       03/17/2011: exact weighting functional - FF
%
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%   check arguments
%error(nargchk(6,12,nargin));
narginchk(6, 13)
if ~exist('ibin','var')
    ibin=0;
end;
if ~exist('pixsize','var')
    pixsize=0.5;
end;
file=tom_emread(marker_file);
Matrixmark=file.Value;
ntilt=size(Matrixmark,2);

% exact weighting 
ProjDirResult = 0;
Tiltangles = 0;
if weighting > 0
    ProjDirResult(1:ntilt) = 0;
    Tiltangles=zeros(1,ntilt);
    Tiltangles(1,:) = squeeze(Matrixmark(1,:,1));
    %all_angles = double([ProjDirResult; Tiltangles]');
end;

hh=tom_emread(strcat(projection_file,'_','1', myext));
imdim=size(hh.Value,1);

if alignopts.ireftilt == 0
    [val, alignopts.ireftilt]=min(abs(Matrixmark(1,:)));
end;
if alignopts.r(1) == 0
    alignopts.r = av3_get_coords_of_referencemarker(Matrixmark,...
                    alignopts.irefmark,alignopts.imdim) + imdim/2 + 1;
end;


if alignopts.finealig
    disp('############################################');
    disp('Crude alignment before fine-alignment of marker points:')
    tom_alignment3d(Matrixmark, alignopts.irefmark, alignopts.ireftilt, ...
        alignopts.r);
    %compute dimensions of box for fiducial fit - approx 2x15 nm
    dim = floor(10/ pixsize)*2;
    Matrixmark = av3_finelocate(Matrixmark, projection_file, myext, dim);
    tom_emwrite([marker_file,'.finealig'],Matrixmark);
    disp('############################################');
    disp('Crude alignment after fine-alignment of marker points:')
    tom_alignment3d(Matrixmark, alignopts.irefmark, alignopts.ireftilt, ...
        alignopts.r);
    disp('');disp('');
    if alignopts.finealigfile
        tom_emwrite(alignopts.finealigfile, Matrixmark);
        disp(['wrote refined Markerfile ', alignopts.finealigfile, ' ...']);
    end;
end;

alignment = av3_align_tiltseries(Matrixmark, alignopts);
if (alignopts.xml_alig)
    disp('writing xml alignment file')
    av3_create_tomoaligfile(alignment, projection_file, myext, alignopts.xml_alig);
end;

av3_write_weighted_and_aligned_projs(alignment, alignopts, ...
        projection_file, myext, weighting, weightedfile_name, filter, ...
        ProjDirResult, Tiltangles, ibin);
