function matrixmark = av3_readPyTomMarkers(pytommarkerfile)
%   matrixmark = av3_readPyTomMarkers(pytommarkerfile)
%
% INPUT
%   pytommarkerfile     markers in pytom file format
%
% OUTPUT
%   matrixmark          matrix containing markers in standard tom format
%
% SEE ALSO
%   tom_alignment3d, av3_align_tiltseries, av3_alignsetup
%
% FF - 2020
matrixmark = zeros(12,1,1);
pytommark=importdata(pytommarkerfile, ' ', 5);

for ii=1:size(pytommark.data,1)
    imark = pytommark.data(ii,1);
    tiltangle = pytommark.data(ii,2);
    xpos = pytommark.data(ii,3);
    ypos = pytommark.data(ii,4);
    if imark == 0
        matrixmark(1,ii,1)= tiltangle;
        indx = ii;
    else
        % find index for tilt angle
        indx = find(matrixmark(1,:,1) == tiltangle);
    end
    matrixmark(2,indx,1+imark)=xpos;
    matrixmark(3,indx,1+imark)=ypos;
end


