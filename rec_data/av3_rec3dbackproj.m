function vol = av3_rec3dbackproj(projfile, append, last, voldims, offset, first)
%   reconstruct volume from aligned & weighted projections
%   
%   vol = av3_rec3dbackproj(projfile, append, last, offset, first)
%
%  INPUT
%   projfile   projection file names (<projfile>.<index>.<append>)
%               - assume tilt axis = y
%   append     append
%   last       index of last projection
%   voldims    dimensions of volume
%   offset     offset for reconstruction
%   first      index of first projection (default: 1)
%
%  OUTPUT
%   vol        3d volume
%
%   28/03/2011 - FF
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

vol = single(zeros(voldims(1), voldims(2), voldims(3)));
if ~exist('first')
    first = 1;
end;

for iproj=first:last
    projname = [projfile  num2str(iproj)];
    try
        proj = tom_emread([projname append]);
    catch exception
        disp(['projection ', projname, ' not found ...']);
        continue;
    end;
    angle_the = proj.Header.Tiltangle;
    tom_backproj3d(vol,single(proj.Value), 0, angle_the, offset);
end;