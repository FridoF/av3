function av3_prepprojs(projection_file, myext, marker_file, ...
                out_file, r, finaldim, lowpass, ibin, irefmark, ireftilt)
%av3_prepprojs writes out aligned micrographs centered to feature
%
%   av3_prepprojs(projection_file, myext, marker_file, ...
%               out_file, r, finaldim, lowpass, ibin, irefmark, ireftilt)
%
%   The procedure shifts a feature specified by r to the center of the
%   tilted images. The resulting images can be used to perfrom a detailed
%   reconstruction of the feature of interest, e.g. using SIRT.
%
%PARAMETERS
%
%   INPUT
%    projection_file            name of tiltseries [string]
%    myext                      extension (of file), e.g. '.em' [string]
%    marker_file                name of markerfile [string]
%    out_file                   name output files
%    r[3]                       coordinates of feature you would like to be
%                               in the center
%    finaldim                   dimension of final micros
%    lowpass                    radius of low-pass filter in pix (entire
%                               picture!)
%    ibin                       binning of projections (default =0)
%    irefmark                   index of reference marker
%    ireftilt                   number of reference tiltangle - default:
%                                0 degree projection
%   
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%   check arguments
error(nargchk(5,10,nargin));

%% prep filenames
file=strcat(projection_file,  num2str(1), myext);
file=tom_reademheader(file);
imdim = file.Header.Size(1);
file=tom_emread(marker_file);
Matrixmark=file.Value;
ntilt=size(Matrixmark,2);
%hh=tom_emreadc(strcat(projection_file,'1', myext));

%% determine lowest tiltangle projection
if (nargin < 10)
    [dummy, imintilt]=min(abs(Matrixmark(1,:,1)));
else
    imintilt=ireftilt;
end;

%% reference marker
if (nargin < 9)
    irefmark = 1;
end;

%% binning
if (nargin < 8)
    ibin = 0;
end;

%% lowpass 
if (nargin < 7)
    lowpass = 0;
end;

%% dimension of aligned micros
if nargin < 6
    finaldim = imdim;
end;

%% calculate new coordinates of marker so that feature is in center of image
% coordinates of reference marker irefmark
cent = imdim/2+1;
rmark_old = [Matrixmark(2,imintilt,irefmark) Matrixmark(3,imintilt,irefmark) cent];
% shift
delta = rmark_old - r;
% new coords
rmark = [cent cent cent] + delta;

%% calculate tilt axis and shifts of projection images
[Matrixmark, beta, sigma, x, y, z]  = tom_alignment3d(Matrixmark, irefmark, imintilt, rmark, imdim);
betaindeg = beta.*180/pi; % perform rotations by beta
psiindeg = betaindeg+90;  % angle between tilt axis and y-axis
psi=psiindeg*pi/180.0; % in radians

%% loop over images
for itilt = 1:ntilt
    % read the projection files
    file=strcat(projection_file, num2str(itilt), myext);
    image=tom_emreadc(file);
    orig_header=image.Header;
    % lowpass if specified
    if lowpass > 0
        image.Value = tom_bandpass(image.Value,0,lowpass,lowpass/10);
    end;
    if (ibin >=1)
        image=tom_bin(image.Value,ibin);    
        image=double(image);
    else
        image = image.Value;
    end;
    % normalize to contrast
    immean = mean(mean(image));
    scf = 1/immean;
    image = double(image)*scf;  %divide by mean
    % rotation to tilt axis
    image=tom_rotate(image,-psiindeg,'linear',[floor(size(image,1)./2) floor(size(image,1)./2)]);
    
    % shifts 
    shift_tx = Matrixmark(7,itilt,1)./(2^ibin);
    shift_ty = Matrixmark(8,itilt,1)./(2^ibin); 
    shiftx = cos(psi)*shift_tx + sin(psi)*shift_ty;
    shifty = -sin(psi)*shift_tx + cos(psi)*shift_ty;
    % apply int shifts in real(move) and the rest in fourier space to
    % prevent periodic artifacts
    image=tom_move(image,[-floor(shiftx) -floor(shifty)]);
    fimage = tom_fourier(image);
    fimage = tom_shift_fft(fimage, [-(shiftx-fix(shiftx)) -(shifty-fix(shifty))] );
    image = real(tom_ifourier(fimage));
    
    % write out projs
    tmp = strcat(out_file,num2str(itilt),'.em');
    olddim = size(image,1);
    topleft = round((olddim-finaldim)/2)+1;
    image = tom_red(image,[topleft, topleft],[finaldim,finaldim]);
    imt = tom_emheader(image);
    imt.Header=orig_header;
    imt.Header.Size(1)=size(imt.Value,1);
    imt.Header.Size(2)=size(imt.Value,2);
    imt.Header.Size(3)=1; % projection images are two-dimensional
    imt.Header.Objectpixelsize=orig_header.Objectpixelsize.*2.^(ibin);
    imt.Header.Tiltaxis=0;
    imt.Header.Magic(4)=5; % write floats
    image = imt.Value;%put in to fill header correctly, FF
    tom_emwrite(tmp,imt);
    disp([' ... wrote micrograph ' num2str(itilt) ' of ' num2str(ntilt)]);
    tom_imagesc(image);refresh(gcf);
end;

