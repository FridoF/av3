function av3_tilt_lines(alig,ref,pkt)
%  AV3_TILT_LINES plots marker points 
%   AV3_TILT_LINES(ALIG,REF,PKT) 
%
%   alig=   Alignment array (from marker file)
%   ref=    Reference index marker
%   pkt=    compared refernce marker (if identical to ref, all will be
%           plotted)
%
%  EXAMPLE
%
txt=1;
all=0;
if ref==pkt
    all=1;
    txt=0;
end
[s1,s2,s3]=size(alig);
if all==0
  plot(0,0,'r+');hold on;zoom on;
  inds=find((alig(2,:,ref) > -1) & (alig(3,:,ref) > -1) & (alig(2,:,pkt) > -1) & (alig(3,:,pkt) > -1));
  alig_x=alig(2,inds,ref)-alig(2,inds,pkt);
  alig_y=alig(3,inds,ref)-alig(3,inds,pkt);
  for kk=1:size(inds,2)
    plot(alig_x(kk),alig_y(kk),'r+');
    if txt==1
      text('Position',[alig_x(kk)+1 alig_y(kk)+1],'String',alig(1,inds(kk),1)); 
    end
  end
grid off;
hold on;
end
if all==1
  clf; plot(0,0,'r+');hold on;
  for ref=1:s3
    for pkt=ref+1:s3
        inds=find((alig(2,:,ref) > -1) & (alig(3,:,ref) > -1) & (alig(2,:,pkt) > -1) & (alig(3,:,pkt) > -1));
        alig_x=alig(2,inds,ref)-alig(2,inds,pkt);
        alig_y=alig(3,inds,ref)-alig(3,inds,pkt);
        plot(alig_x,alig_y,'+');
        text('Position',[alig_x(1) alig_y(1)],'String',ref,'Fontsize',20);   
        text('Position',[alig_x(1)+40 alig_y(1)-40],'String',pkt,'Fontsize',20);   
    end
  end
end