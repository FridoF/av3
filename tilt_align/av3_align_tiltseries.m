function alignment = av3_align_tiltseries(Matrixmark,alignopts)
%
%   alignment = av3_align_tiltseries(Matrixmark,alignopts)
%
%  INPUT
%   Matrixmark    markers
%   alignopts     alignment options
%
%  OUTPUT
%   alignment     alignment parameters (structure)
%                  - psi:      angle of tilt axis
%                  - drot:     focus-induced additional tilt
%                  - dmag:     focus-induced magnification change
%                  - trans:    translations of micrographs
%                  - mark3d:   3D coordinates of markers
%                  - dbeam:    beam inclination angle
%                  - dmagnfoc: incline for x-dependent magnification change
%                  - drotfoc:  incline for x-dependent rotation change
%
%  SEE ALSO
%   av3_alignsetup, tom_alignment3d
%
%   last change
%   11/09/13 - FF: add projection indices to output alignment 
%
%   Copyright (c) 2005-2013
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

if alignopts.irefmark == 0
    alignopts.irefmark=1;
end;
if alignopts.ireftilt == 0
    [val, alignopts.ireftilt]=min(abs(Matrixmark(1,:)));
end;
if alignopts.r(1) ~= 0
    [Matrixmark, psi, sigma, x, y, z]  = tom_alignment3d(Matrixmark, ...
        alignopts.irefmark, alignopts.ireftilt, ...
        alignopts.r, alignopts.imdim, alignopts.handflip);
else
    [Matrixmark, psi, sigma, x, y, z]  = tom_alignment3d(Matrixmark, ...
        alignopts.irefmark, alignopts.ireftilt, [0,0,0], alignopts.imdim, ...
        alignopts.handflip);
end;

ntilt      = size(Matrixmark,2);
mark3d     = [x',y',z'];
dmag       = zeros(ntilt,1);
drot       = zeros(ntilt,1);
dmagnfoc   = 0;
drotfoc    = 0;
dbeam      = 0;
trans      = zeros(ntilt,2);
trans(:,1) = squeeze(Matrixmark(7,:,1))';
trans(:,2) = squeeze(Matrixmark(8,:,1));

residual = av3_aliresidual(Matrixmark, alignopts.cent, psi, ...
                mark3d, ...
                trans, dmag, drot, ...
                dbeam, dmagnfoc, drotfoc, false);
disp('-----------------------------------------------------');
disp(['Residual before further optimization = ',num2str(sqrt(residual))]);

%% optimize residual
alpars0 = av3_alpars_create(Matrixmark, alignopts, psi, mark3d, ...
                trans, dmag, drot, dbeam, dmagnfoc, drotfoc);
nunknowns   = size(alpars0,1);
kk = find(Matrixmark(2:3,:,:)>-1);
ndatapoints = size(kk,1);
disp(['Ratio datapoints / unknowns: ', num2str(ndatapoints/nunknowns)]);
tic;
if strcmp(alignopts.optimizer, 'UNC')
    % pre-optimization using steepest descent
%     options = optimset('GradObj','off','LargeScale','off',...
%         'TolFun',.0000001, 'MaxFunEvals',1000000000, 'TolX',.00000000001,...
%         'MaxIter',100, 'HessUpdate','steepdesc');
%     alpars = fminunc(@(alpars) av3_aliscore(alpars, Matrixmark, alignopts), alpars0, options);
    options = optimset('GradObj','off','LargeScale','off',...
        'TolFun',.0000001, 'MaxFunEvals',1000000000, 'TolX',.00000000001,...
        'MaxIter',alignopts.MaxIter);%,'Diagnostics','on');
    alpars = fminunc(@(alpars) av3_aliscore(alpars, Matrixmark, alignopts), alpars0, options);
elseif strcmp(alignopts.optimizer, 'UNCGRAD')
    options = optimset('GradObj','on',...
        'TolFun',.0000001, 'MaxFunEvals',1000000000, 'TolX',.0000000000000001,...
        'MaxIter',alignopts.MaxIter,'NonlEqnAlgorithm','lm','Diagnostics','on');%,'DerivativeCheck','on');
    alpars = fminunc(@(alpars) av3_aliscore(alpars, Matrixmark, alignopts), alpars0, options);
elseif strcmp(alignopts.optimizer, 'FMIN')
    options = optimset('GradObj','off','LargeScale','off',...
        'TolFun',.0000001, 'MaxFunEvals',1000000000, 'TolX',.00000000001,...
        'MaxIter',alignopts.MaxIter);
    alpars = fminsearch(@(alpars) av3_aliscore(alpars, Matrixmark, alignopts), alpars0, options);
elseif strcmp(alignopts.optimizer, 'MINFUNC')
    options.Display = 'none';
    options.Method = 'CG';
    options.numDiff = 1;
    options.cgSolve = 1;
    options.MAXFUNEVALS = 3000;
    options.MAXITER = 1000;
    
    %options.DerivativeCheck = 'on';
    alpars = minFunc(@(alpars) av3_aliscore(alpars, Matrixmark, alignopts), alpars0, options);
else    
    error(['optimizer ', alignopts.optimizer, ...
        ' not known. Use "UNC", "UNCGRAD", "MINFUNC" or "FMIN"']);
end;
toc;
[psi, mark3d, trans, dmag, drot, dbeam, dmagnfoc, drotfoc] = av3_alparsresolve(alpars, Matrixmark, alignopts);
residual = av3_aliresidual(Matrixmark, alignopts.cent, psi, ...
                mark3d, trans, dmag, drot, dbeam, dmagnfoc, drotfoc, false);
disp(['Residual after optimization = ', num2str(sqrt(residual))]);
disp('-----------------------------------------------------');
alignment.psi = psi;
alignment.dpsi = drot;
alignment.dmag = dmag;
alignment.trans = trans;
alignment.mark3d = mark3d;
alignment.dbeam = dbeam;
alignment.dmagnfoc = dmagnfoc;
alignment.drotfoc = drotfoc;
alignment.projindices = squeeze(Matrixmark(11,:,1));
% check that projindices were set in the first place
if mean(alignment.projindices == 0)
   alignment.projindices = 1:size(alignment.projindices,2);
end;
