function out=av3_transform(varargin)
%av3_transform performs a 2d or 3d transformation, depending on the input
%
%   out=av3_transform(varargin)
%   out=av3_transform(in,[shift],[euler_angles],s,[center])
%
%NOTE
%   This function works as an interface. The actual computation is done
%   in the C-Function av3_transformc
%
%PARAMETERS
%
%  INPUT
%   in                  image or Volume as single!
%   shift               shift vector
%   euler_angles        rotation matrixs [phi, psi, the] (rot X1 X3 Z2)
%   s                   magnification
%   center(optional)    in=image  [centerX centerY]       
%                       in=Volume [centerX centerY centerZ] 
%                       
%                       WARNING: Convention for rotation center
%                       Matlab / TOM : Because Matlab starts counting array elements at 1,
%                       rotation center will be size / 2 +1 by default
%
%                       C++ / Python: That equals size / 2 for real
%                       computer languages which start counting elements at 0 
%
%                       ( => Beware, Matlab is no real computer language. Is this a feature? )
%
%  
%  OUTPUT
%   out                 Image rotated
%
%ROTATION INFORMATION
% 
% av3_transformc.c uses the following 3d Euler rotation 
% (first index: line, second index: column):
% 
% rm00=cospsi*cosphi-costheta*sinpsi*sinphi;
% rm10=sinpsi*cosphi+costheta*cospsi*sinphi;
% rm20=sintheta*sinphi;
% rm01=-cospsi*sinphi-costheta*sinpsi*cosphi;
% rm11=-sinpsi*sinphi+costheta*cospsi*cosphi;
% rm21=sintheta*cosphi;
% rm02=sintheta*sinpsi;
% rm12=-sintheta*cospsi;
% rm22=costheta;
% 
%  This is a 3d Euler rotation around (zxz)-axes
%  with Euler angles (psi,theta,phi)
%  
%  For more information about 3d Euler rotation visit:
%  wiki -> Definitions -> Overview 3d Euler rotation
%  
%  To produce the same result as in EM use nx/2, ny/2
%  and nz/2, not !!!! +1
%
%       X-axis  corresponds to  phi=0     psi=0   the=alpha
%       Y-axis  corresponds to  phi=270   psi=90  the=alpha
%       Z-axis  corresponds to  phi=alpha psi=0   the=0
%
%EXAMPLE
%   im=tom_emread('pyrodictium_1.em');
%
%REFERENCES
%
%SEE ALSO
%   tom_rotate, tom_rotatec
%
%   created by FF 12/02/11
%
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

switch nargin
    case 5,
        center = varargin{5};
    case 4,
        center = [ceil(size(varargin{1})./2)];
    otherwise
        disp('wrong number of Arguments');
        out=-1; return;
end;
%parse inputs
in = varargin{1};
trans = varargin{2};
euler_angles=varargin{3};
mag = varargin{4};

% allocate some momory
out = single(zeros(size(in)));

% call C-Function to do the calculations
av3_transformc(single(in), out, single(trans), single([euler_angles]), ...
    mag, single([center]));

out = double(out);






















