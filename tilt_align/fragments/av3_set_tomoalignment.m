function tomoalignment = av3_set_tomoalignment(varargin)
%
%   unfinished fragment - think more about proper structure
%
for iarg=1:2:nargin
    switch lower(varargin{iarg})
        case 'alignment'
            alignment = varargin{iarg+1};
        case 'filename'
            filename = varargin{iarg+1};
        case 'psi'
            psi = float(varargin{iarg+1});
        case 'dpsi'
            dpsi = varargin{iarg+1};
        case 'trans'
            trans = varargin{iarg+1};
            if size(trans,2) ~= 2
                error('Dimension mismatch - trans is ntilt x2 array');
            end;
        case 'dmag'
            dmag = varargin{iarg+1};
            if size(dmag,2) ~= 1
                error('Dimension mismatch - dmag is ntilt x 1 array');
            end;
            
    end;
end;

            
alignment.psi = psi;
alignment.dpsi = drot;
alignment.dmag = dmag;
alignment.trans = trans;
alignment.mark3d = mark3d;
alignment.dbeam = dbeam;
alignment.dmagnfoc = dmagnfoc;
alignment.drotfoc = drotfoc;


tomoalignment.psi = psi;
tomoalignment.dbeam = 0;
for imark=1:nmark
    tomoalignment.marker(imark).coords(1:3) = ;
end;

ntilt = 0;
if iscell(filename)
    ntilt = size(filename,2);
end;
    

if exist(alignment)
    nmark = size(alignment.mark3d,1);
    for imark=1:nmark
        tomoalignment.marker(imark).coords(1:3) = alignment.mark3d(imark,:);
    end;
    tmp = size(alignment.dmag(1));
    if ntilt > 0
        if tmp ~= ntilt
            error('Dimension mismatch - N_tilt in filenames and alignmentpars differ');
        end;
    else
        ntilt = tmp;
    end;    
    tomoalignment.psi = alignment.psi;
    tomoalignment.dbeam = alignment.dbeam;
    for ii=1:ntilt
        tomoalignment.projection(ii).index = ii;
        if iscell(filename)
            tomoalignment.projection(ii).filename = filename(ii);
        else
            tomoalignment.projection(ii).filename = [filename, '_', num2str(ii), '.em'];
        end;
        tomoalignment.projection(ii).trans(1) = ;
        tomoalignment.projection(ii).trans(2) = ;
        tomoalignment.projection(ii).dmag = ;
        tomoalignment.projection(ii).dpsi = ;
        %tomoalignment.projection(ii).dmagnfoc = 0;
        %tomoalignment.projection(ii).drotfoc = 0;
    end;
end;

