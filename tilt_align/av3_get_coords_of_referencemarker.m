function coords = av3_get_coords_of_referencemarker(Matrixmark,imark, imdim, iref)
%   gett coordinates of chosen reference marker for tilt alignment
%
%   coords = av3_get_coords_of_referencemarker(Matrixmark,imark, imdim, iref)
%
%  INPUT
%   Matrixmark   alignment markers (12 column list)
%   imark        index of marker used for alignment
%   imdim        dimension of images
%
%  OUTPUT
%   coordinates  3D coordinates of reference marker
%
%   
%   copyright: Forster lab
%
%   05/21/2019 FF

if ~exist('imdim', 'var')
    imdim = 2048;
end;
cent = floor(imdim/2)+1;
%reference proj
if ~exist('iref', 'var')
    [~, iref]=min(abs(Matrixmark(1,:)));
end
coords=zeros(3,1);
coords(1)=Matrixmark(2,iref,imark)-cent;
coords(2)=Matrixmark(3,iref,imark)-cent;
coords(3)=0;

