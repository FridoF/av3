function Matrixmark = av3_delete_projection_in_markerfile(Matrixmark, indices)
%   Delete specified projections from marker matrix
%
%   Matrixmark = av3_delete_projection_in_markerfile(Matrixmark, indices)
%
%  INPUT
%   Matrixmark  matrix containing markers
%   indices     indices of projection files you want to ommit from
%               alignment and reconstruction
%
%  OUTPUT
%   Matrixmark  matrix without deleted projections
%
%   09/2011 - FF
%
%  SEE ALSO
%   av3_align_tiltseries, av3_alignsetup, av3_aliscore, tom_alignment3d
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
indices = sort(indices);
for ii=1:max(size(indices))
    n = size(Matrixmark,2);
    ind = find(Matrixmark(11,:,1) == indices(ii));
    tMatrixmark = Matrixmark(:,1:ind-1,:);
    tMatrixmark(:,ind:n-1,:) =  Matrixmark(:,ind+1:n,:);
    Matrixmark = tMatrixmark;
end;