function av3_imodstack2projs(imodstackname, emfilename)
% converts imod stack to single projections
%
%   av3_imodstack2projs(imodstackname, emfilename)
%
%
%   FF 20??
imodstack = tom_mrcreadimod([imodstackname, '.st']);
ntilt = size(imodstack.Value,3);
tltangs = load([imodstackname, '.tlt']);
for itilt=1:ntilt
    proj = tom_emheader(tom_xraycorrect(imodstack.Value(:,:,itilt)));
    proj.Header.Tiltangle = tltangs(itilt);
    proj.Header.Voltage = 300.;
    tom_emwrite([emfilename, '_', num2str(itilt), '.em'], proj);
end;