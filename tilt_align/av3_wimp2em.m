function [Marker] = av3_wimp2em(wimpfile,tltfile,fout,bin)
%   convert imad markerfile to em marker stack
%
%   [Marker] = wimp2em(wimpfile,tltfile,fout,bin)
%
%  INPUT
%   wimpfile      IMOD markerfile
%   tltfile       tilt angles from IMOD
%   fout          output name for EM markerfile
%   bin           binning of coordinates in input wimpfile
%
%  OUTPUT
%   Marker        EM markerfile
%
%   probably EV
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

clear Marker


% Get the tilt axes
ftilt = fopen(tltfile);

%if (ftilt > 0 ) 
    
    tline = fgetl(ftilt);
    tilt = [];

    while ischar(tline) 
        tilt = [tilt str2num(tline)];
        tline = fgetl(ftilt);
    end

    fclose(ftilt);

%else 
%return;    
%end    

imagenum = size(tilt,2);

fid = fopen(wimpfile);

% First three lines are not used
tline = fgetl(fid);
tline = fgetl(fid);
tline = fgetl(fid);


% Second line contains the number of objects
tline = fgetl(fid);

if (strfind(tline, '# of object') )
    A = sscanf(tline, '%s%s%s%d');
    totobj = A(size(A,1));
end


Data = zeros(12,imagenum,totobj);

for i=1:totobj
    Data(1,:,i) = tilt;
    Data(2,:,i) = -1;
    Data(3,:,i) = -1;
end


while ischar(tline)

    if (strfind(tline, 'Object #'))
        A = sscanf(tline, '%s%s%d');
        objnum = A(size(A,1));
        tline = fgetl(fid);
        coords = [];
        frame = [];
        A = sscanf(tline, '%s%s%s%d');
        framenum = A(size(A,1));
        tline = fgetl(fid);
        tline = fgetl(fid);
        for f=1:framenum
            tline = fgetl(fid);
            line = sscanf(tline, '%f%f%f%f%f');
            x = line(2);
            y = line(3);   % somehow the em images are flipped in y :-P
            z = int16(line(4)) + 1;  %  marker file has Z running 0 
            Data(2,z,objnum) = ceil(x *bin);
            Data(3,z,objnum) = ceil(y *bin);
        end
    end
    tline = fgetl(fid);
end


fclose(fid);

Marker = tom_emheader(Data);
 
tom_emwrite(fout,Marker);

