function alpars = av3_alpars_create(Matrixmark, alignopts, psi, mark3d, ...
                    trans, dmag, drot, dbeam, magnfoc, rotfoc)
% resolve alignment parameters from input vector
%
%   alpars = av3_alpars_create(Matrixmark, psi, mark3d, trans, dmag, ...
%                   drot, dbeam, magnfoc, rotfoc)
%
%   Coordinates of reference marker irefmark are fixed, either to
%   alignopts.r or to default (x,y-coordinates of reference projection
%   ireftilt, z=0). s(ireftilt) and dmag(ireftilt) are fixed to 1 and 0,
%   respectively.
%   Number of alignment parameters for fit depends on setting of alignopts.
%   For example, alpars include N_tilt-1 values for dmag(itilt) if
%   alignopts.drot = true.
%
%  INPUT
%   Matrixmark   alignment markers (12 column list)
%   alignopts    
%   psi          angle of tilt axis
%   mark3d       marker coordinates (3D)
%   trans        translations of projections
%   dmag         Delta magnification in each image
%   drot         Delta rotation in each image
%   dbeam        beam inclination
%   magnfoc      constant for focus-dependent magnification
%   rotfoc       constant for focus-dependent rotation
%
%  OUTPUT
%   alpars       alignment parameters stored in array
%
%  SEE ALSO
%   av3_alparsresolve, av3_align_tiltseries, av3_alignsetup
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
    
nmark = size(Matrixmark,3);
ntilt = size(Matrixmark,2);
alpars(1,1) = psi;

ii = 2;
scaletrans = 1;
scalemag   = 1;

%% marker 3D coords 
% reference marker is fixed to standard value
for imark=1:alignopts.irefmark-1 
    alpars(ii:ii+2,1) = mark3d(imark,:);
    ii = ii + 3;
end;
for imark=alignopts.irefmark+1:nmark %reference (1st) marker is fixed to standard value
    alpars(ii:ii+2,1) = mark3d(imark,:);
    ii = ii + 3;
end;

%% translations of images
for itilt=1:ntilt
    alpars(ii:ii+1,1) = scaletrans*trans(itilt,:);
    ii = ii + 2;
end;

%% focus-dependent magnification and rotation
% magification and rotation of reference tilt is fixed
if alignopts.drot  
    for itilt=1:alignopts.ireftilt-1
        alpars(ii,1) = dmag(itilt);
        alpars(ii+1,1) = drot(itilt);
        ii = ii + 2;
    end;
    for itilt=alignopts.ireftilt+1:ntilt
        alpars(ii,1) = dmag(itilt);
        alpars(ii+1,1) = drot(itilt);
        ii = ii + 2;
    end;
end;

%% beam inclination
if alignopts.dbeam
    alpars(ii,1)=dbeam;
    ii = ii + 1;
end;

%% focus gradient 
if alignopts.magnfoc
    alpars(ii,1) = scalemag*magnfoc;
    ii = ii + 1;
    alpars(ii,1) = rotfoc;
end;