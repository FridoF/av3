function Matrixmark = av3_convertIMOD2TomMatrix(Marker)
%   function converts Marker structure to Matrix for tom_alignment3d etc
%
nmark = size(Marker,2);
ntilt = size(Marker(1).Coords,1);

Matrixmark = zeros(12,ntilt,nmark);
Matrixmark(1,:,1) = 1:ntilt;
for imark=1:nmark
    marks = Marker(imark).Coords;
    Matrixmark(2:3,:,imark) = marks';
end;