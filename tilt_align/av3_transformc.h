

/* switch between mex and C */
#define MATLAB  
/*#define ANSI_C*/

#define	 PI ((double)3.14159265358979323846264338327950288419716939937510)

/* Input Arguments */
#ifdef MATLAB  
	#define    INP    prhs[0]
	#define    OUT    prhs[1]
    #define    TRANS  prhs[2]
	#define    EULER  prhs[3]
    #define    MAG    prhs[4]
	#define    CENT   prhs[5]
#endif


/* void transform2d (float *image, float *rotimg,long sx,long sy, 
        float tx, float ty, 
        float *p_phi, float s, float px,float py,int euler_dim); */
/*void transform3d ( float *image, float *rotimg, long sx, long sy, long sz,
        float tx, float ty, float tz, float *p_euler_A, float s, 
        float px, float py, float pz, int euler_dim);*/