function Marker = av3_apply_prexg(prexgfile,markerfile,fout,np_unbinned)
% Get the shifts for the markers
%
% shifts = read_prexf(prexfile)
%
% From the prexf documentation: 
%  Each linear transformation in a transform file is specified by a line with 
%  six numbers:
%    A11 A12 A21 A22 DX DY
%  where the coordinate (X, Y) is transformed to (X', Y') by:
%    X' = A11 * X + A12 * Y + DX
%
%  NOTE THAT ONE MUST USE THE PREXG FILE, NOT PREXF, since the former has 
% the global alignment, while the latter is simply the shifts wrt the
% previous slice!
%
%  Note that the markerfile, pressumably created with wimp2em, already is
%  unbinned!
%  Note that the prexg file writes the shifts for the unbinned projections,
%  irrespective of how you determined them (xcorr, midas, raptor) <-CHECK!
%
% JO. 11.09.13
% np_unbinned =  size in pixels of unbinned projections


fid = fopen(prexgfile);
tline = fgetl(fid);
nl = 1; 

while ischar(tline)
    line = sscanf(tline, '%f%f%f%f%f%f');
    shiftx(nl) = line(5);
    shifty(nl) = line(6);
    tline = fgetl(fid);
    nl = nl+1;
end

fclose(fid)

shifts = [shiftx' shifty'];

Marker = tom_emread(markerfile);

for i=1:size(Marker.Value,2)
    for m = 1:size(Marker.Value,3)
        if Marker.Value(2:3,i,m) > 0
            Marker.Value(2,i,m) =  Marker.Value(2,i,m)  - shifts(i,1);
            Marker.Value(3,i,m) =  Marker.Value(3,i,m)  - shifts(i,2);
        end
        % modification JO;
        Marker.Value(11,i,m) = i;
        Marker.Value(12,i,m) = np_unbinned;
        % end JO;
    end
end

tom_emwrite(fout,Marker);

