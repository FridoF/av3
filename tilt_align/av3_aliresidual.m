function  varargout = av3_aliresidual(Matrixmark, cent, psi, ...
            mark3d, trans, dmag, drot, dbeam, dmagnfoc, drotfoc, lgrad, lplot)
%   calculate residual of a marker model given the marker coords
%
%   residual = av3_aliresidual(Matrixmark, psi, dmag, drot)
%
%   The reported residual is the mean SQUARE deviation between the projected
%   coordinates of the model and the clicked marker points. Please note,
%   that in TOM_ALIGNMENT3D the ROOT mean squared deviation is reported.
%   Thus, you need to take the square of the residual of you want to
%   compare these entities.   
%
%  INPUT
%   Matrixmark  marker coordinates, tilt angles
%   cent        lateral center (e.g., [1025,1025])
%   psi         tilt axis
%   mark3d      cartesian coordinates of marker points
%   trans       translation vectors (vector; 2 x ntilt)
%   dmag        delta magnification (vector; dim: ntilt)
%   drot        delta image rotations (vector; dim: ntilt)
%   dbeam       beam inclination (scalar)
%   dmagnfoc    linear magnification change as function of defocus
%   drotfoc     linear image rotation change as function of defocus
%   lgrad       calculate gradients if true
%   lplot       plot markers
%
%  OUTPUT
%   residual            error of fit
%   dresidual_dpsi      partial derivative of residual with respect to psi
%   dresidual_dtrans    -"- to trans
%   dresidual_dmag      -"- to dmag
%   dresidual_drot      -"- to drot
%   dresidual_dmagnfoc  -"- to dmagnfoc
%   residual_drotfoc
%
%   tilt geom:
%   
%   [cos(the_itilt)  0  -sin(the_itilt) ]  [ x_imark ]   
%   [ 0              1             0    ]  [ y_imark ]
%                                          [ z_imark ]
%
%
%   = [ cos(-psi+dpsi_itilt-pi/2) sin(-psi+dpsi_itilt-pi/2)] s_itilt [markx_itilt,imark] - [dx_itilt]
%     [-sin(-psi+dpsi_itilt-pi/2) cos(-psi+dpsi_itilt-pi/2)]         [marky_itilt,imark]   [dy_itilt]
%
%   03/2011 - FF
%
%  SEE ALSO
%   av3_align_tiltseries, av3_alignsetup, av3_aliscore
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

error(nargchk(10,12,nargin));
if exist('lgrad')
    if lgrad ~= true
        lgrad = false;
    end;
else
    lgrad = false;
end;
if exist('lplot')
    if lplot ~= true
        lplot = false;
    end;
else
    lplot = false;
end;
ntilt = size(Matrixmark,2);
nmark = size(Matrixmark,3);

% calculate deviations
psi = -(psi + pi/2);
cpsi = cos(psi);
spsi = sin(psi);
cdbeam = cos(dbeam);
sdbeam = sin(dbeam);
ndif =0;

%% intialize partial derivatives
if lgrad
    dresidual_dpsi = 0;
    dresidual_dtrans = zeros(ntilt,2);
    dresidual_dmark3d = zeros(nmark,3);
    dresidual_dmag = zeros(ntilt,1);
    dresidual_drot = zeros(ntilt,1);
    dresidual_ddbeam = 0;
    dresidual_dmagnfoc = 0;
    dresidual_drotfoc = 0;
end;

%% pre-calculate cosines and sinuses
cthe = zeros(ntilt,1);
sthe = zeros(ntilt,1);
magfac = zeros(ntilt,1);
cdpsi = zeros(ntilt,1);
sdpsi = zeros(ntilt,1);
for itilt = 1:ntilt,
    %projection angle
    the           = pi*Matrixmark(1,itilt,1)/180;
    cthe(itilt)   = cos(the);
    sthe(itilt)   = sin(the);
    magfac(itilt) = (1+dmag(itilt));
    cdpsi(itilt) = cos(drot(itilt));
    sdpsi(itilt) = sin(drot(itilt));
end;

%% marker loop
residual = 0;
for imark = 1:nmark,
    % marker positions in 3d model rotated on tilt axis
    xmod = cpsi*mark3d(imark,1) - spsi*mark3d(imark,2);
    ymod = spsi*mark3d(imark,1) + cpsi*mark3d(imark,2);
    zmod = mark3d(imark,3);
    y_proj = ymod;
    
    % for derivatives
    if lgrad
        dxmod_dpsi   = -spsi*mark3d(imark,1) - cpsi*mark3d(imark,2);
        dy_proj_dpsi =  cpsi*mark3d(imark,1) - spsi*mark3d(imark,2);
    end;
    
    if lplot
        figure(imark);hold on;
    end;
    %% tilt loop
    for itilt = 1:ntilt,
        % check for not-clicked markers
        if  (Matrixmark(2,itilt,imark) > -1.0) & ...
            (Matrixmark(3,itilt,imark) > -1.0)%allow overlapping MPs
            ndif= ndif +1;
            
            % rotate clicked markers back and inverse shift
            xmark = Matrixmark(2,itilt,imark) - cent(1);
            ymark = Matrixmark(3,itilt,imark) - cent(2);
            xmark = xmark - trans(itilt,1);
            ymark = ymark - trans(itilt,2);
            
            x_meas_predpsi = ( cpsi*xmark - spsi*ymark);
            y_meas_predpsi = ( spsi*xmark + cpsi*ymark);
            
            % additional variable rotation
            x_meas = cdpsi(itilt)*x_meas_predpsi - sdpsi(itilt)*y_meas_predpsi;
            y_meas = sdpsi(itilt)*x_meas_predpsi + cdpsi(itilt)*y_meas_predpsi;
            
            % additional variable magnification
            x_meas = x_meas * magfac(itilt);
            y_meas = y_meas * magfac(itilt);
             
            % model projection
            if dbeam == 0
                x_proj = cthe(itilt) * xmod - sthe(itilt) *zmod;
            else
                % beam inclination
                x_proj = cthe(itilt)        * xmod - sthe(itilt)*sdbeam              * ymod - sthe(itilt)*cdbeam * zmod;
                y_proj = sthe(itilt)*sdbeam * xmod + (cdbeam^2+sdbeam^2*cthe(itilt)) * ymod + cdbeam*sdbeam*(1-cthe(itilt)) * zmod;
            end;
            
            % x-dependent magnification of model
%             y_proj_dmag = y_proj;
            tmp         = dmagnfoc*x_proj;
            x_proj      = (1+.5*tmp)*x_proj;
            y_proj_dmag = (1+tmp)*y_proj;
            
            % score
            Deltax = x_meas-x_proj;
            Deltay = y_meas-y_proj_dmag;
            
            residual = Deltax^2 + Deltay^2 + residual;
            
            %% derivatives
            if lgrad
                %% dpsi
                dx_meas_predpsi_dpsi = ( -spsi*xmark - cpsi*ymark);
                dy_meas_predpsi_dpsi = (  cpsi*xmark - spsi*ymark);
                dx_meas_dpsi = cdpsi(itilt)*dx_meas_predpsi_dpsi - ...
                    sdpsi(itilt)*dy_meas_predpsi_dpsi * magfac(itilt);
                dy_meas_dpsi = sdpsi(itilt)*dx_meas_predpsi_dpsi + ...
                    cdpsi(itilt)*dy_meas_predpsi_dpsi * magfac(itilt);
                dx_proj_dpsi = cthe(itilt) * dxmod_dpsi;
                dresidual_dpsi = dresidual_dpsi + ...
                    Deltax * (-dx_meas_dpsi + dx_proj_dpsi) + ...
                    Deltay * (-dy_meas_dpsi + dy_proj_dpsi);
%                 dresidual_dpsi = dresidual_dpsi + ...
%                     Deltax * (dx_meas_dpsi - dx_proj_dpsi) + ...
%                     Deltay * (dy_meas_dpsi - dy_proj_dpsi);
                
                %% dmark3d
                % x_proj = cthe(itilt) * (cpsi*mark3d(imark,1) - spsi*mark3d(imark,2)) - sthe(itilt) *zmod;
                % y_proj = spsi*mark3d(imark,1) + cpsi*mark3d(imark,2);
                dx_proj_dmarkx =  cthe(itilt) * cpsi;
                dx_proj_dmarky = -cthe(itilt) * spsi;
                dx_proj_dmarkz = -sthe(itilt);
                dy_proj_dmarkx = spsi;
                dy_proj_dmarky = cpsi;
                dresidual_dmark3d(imark,1) = dresidual_dmark3d(imark,1) - ...
                    Deltax * dx_proj_dmarkx - ...
                    Deltay * dy_proj_dmarkx;
                dresidual_dmark3d(imark,2) = dresidual_dmark3d(imark,2) - ...
                    Deltax * dx_proj_dmarky - Deltay * dy_proj_dmarky;
                dresidual_dmark3d(imark,3) = dresidual_dmark3d(imark,3) - ...
                    Deltax * dx_proj_dmarkz;

                %% dtrans
                tmpx_dx = - cpsi * magfac(itilt);
                tmpx_dy =   spsi * magfac(itilt);
                tmpy_dx = - spsi * magfac(itilt);
                tmpy_dy =   cpsi * magfac(itilt);
                dx_meas_dx = cdpsi(itilt)*tmpx_dx - sdpsi(itilt)*tmpy_dx;
                dx_meas_dy = cdpsi(itilt)*tmpx_dy - sdpsi(itilt)*tmpy_dy;
                dy_meas_dx = sdpsi(itilt)*tmpx_dx + cdpsi(itilt)*tmpy_dx;
                dy_meas_dy = sdpsi(itilt)*tmpx_dy + cdpsi(itilt)*tmpy_dy;
                dresidual_dtrans(itilt,1) = dresidual_dtrans(itilt,1) + ...
                    Deltax * dx_meas_dx + Deltay * dy_meas_dx;
                dresidual_dtrans(itilt,2) = dresidual_dtrans(itilt,2) + ...
                    Deltax * dx_meas_dy + Deltay * dy_meas_dy;
                
                %% dresidual_dmag
                dresidual_dmag(itilt) = dresidual_dmag(itilt) + ...
                    Deltax * x_meas + Deltay * y_meas;

                % ddrot (ddpsi)
                dx_meas_ddpsi = sdpsi(itilt)*x_meas_predpsi + cdpsi(itilt)*y_meas_predpsi;
                dy_meas_ddpsi = cdpsi(itilt)*x_meas_predpsi - sdpsi(itilt)*y_meas_predpsi;
                dresidual_drot(itilt) = dresidual_drot (itilt) - ...
                    Deltax * dx_meas_ddpsi - Deltay * dy_meas_ddpsi;

            end;
            if lplot
                plot(x_meas, y_meas,'b+');hold on;
                plot(x_proj, y_proj,'r+');hold on;
            end;
        end;
    end;
end;
%% normalize and prepare output
normf = 1 /(ndif - size(Matrixmark,3));
%residual = sqrt(residual/(ndif - size(Matrixmark,3)));
residual = residual*normf;
varargout{1}=residual;
if lgrad
    normf = 2*normf;
    varargout{2}=dresidual_dpsi*normf;
    varargout{3}=dresidual_dmark3d*normf;
    varargout{4}=dresidual_dtrans*normf;
    varargout{5}=dresidual_dmag*normf;
    varargout{6}=dresidual_drot*normf;
    varargout{7}=dresidual_ddbeam*normf;
    varargout{8}=dresidual_dmagnfoc*normf;
    varargout{9}=dresidual_drotfoc*normf;
end;
