function alignment = av3_get_tomoalig(marker_file, alignopts, projection_file, myext)
%   av3_get_tomoalig gets alignment from marker file and alignment options
%
%   av3_get_tomoalig(marker_file, alignopts)
%
%
%PARAMETERS
%
%   INPUT
%    marker_file                name of markerfile [string]
%    alignopts                  alignment options (from av3_alignsetup)
%
%   OUTPUT
%    alignment                  alignment of tilt series
%
%
%EXAMPLES
%
%REFERENCES
%
%SEE ALSO
%   av3_align_tiltseries, av3_alignsetup
%
%   FF - 28/03/2011
%
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

file=tom_emread(marker_file);
Matrixmark=file.Value;

if alignopts.ireftilt == 0
    [val, alignopts.ireftilt]=min(abs(Matrixmark(1,:)));
end;
if alignopts.r(1) == 0
    alignopts.r = av3_get_coords_of_referencemarker(Matrixmark,...
                    alignopts.irefmark,alignopts.imdim) + alignopts.imdim/2 + 1;
end;


if alignopts.finealig
    disp('############################################');
    disp('Crude alignment before fine-alignment of marker points:')
    tom_alignment3d(Matrixmark, alignopts.irefmark, alignopts.ireftilt, ...
        alignopts.r);
    Matrixmark = av3_finelocate(Matrixmark, projection_file, myext);
    tom_emwrite([marker_file,'.finealig'],Matrixmark);
    disp('############################################');
    disp('Crude alignment after fine-alignment of marker points:')
    tom_alignment3d(Matrixmark, alignopts.irefmark, alignopts.ireftilt, ...
        alignopts.r);
    disp('');disp('');
    if alignopts.finealigfile
        tom_emwrite(alignopts.finealigfile, Matrixmark);
        disp(['wrote refined Markerfile ', alignopts.finealigfile, ' ...']);
    end;
end;

alignment = av3_align_tiltseries(Matrixmark, alignopts);
if (alignopts.xml_alig)
    if (exist('projection_file') && exist('myext'))
        disp('writing xml alignment file')
        av3_create_tomoaligfile(alignment, projection_file, myext, alignopts.xml_alig);
    else
        error('When xml_alig is specified you must provide projection_file and myext');
    end;
end;
