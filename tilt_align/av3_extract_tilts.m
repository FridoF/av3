


function tilts = av3_extract_tilts(filename)

% av3_extract_tilts extracts tilt angles stored in the header of a SerialEM tomography stack.
%
% tilts = av3_extract_tilts(filename)
%
%PARAMETERS
%
%  INPUT
%   filename            path and filename of stack
%
%  OUTPUT
%   tilts               vector of tiltangles
%    
%
%EXAMPLE
%   tilts = av3_extract_tilts('stack/tomo01_stack.mrc');


    setenv('LD_LIBRARY_PATH',[getenv('LD_LIBRARY_PATH') ':/usr/local/apps/IMOD/lib']);
    [~, ~]=unix(['extracttilts ' filename ' tilts.txt']);

    tilts=importdata('tilts.txt');
    !rm tilts.txt


end