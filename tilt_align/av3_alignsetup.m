function alignopts = av3_alignsetup(varargin)
%   setup tilt series alignment
%
%   alignopts = av3_alignsetup(options,values)
%
%   In this function you may set up the tilt series alignment, i.e.,
%   specify fit parameters.
%
%       'finealig': fine-center gold beads, true/false(default)
%       'finealigfile': filename of re-centered gold beads (only used when
%                       'finealig' is true)
%       'xml_alig': write xml file with alignment parameters (string)
%       'dmag':     true(default)/false
%       'drot':     true(default)/false
%       'dbeam':    beam inclination, true/false(default)
%       'grad':     compute gradients of residual for opti, true/false(default)
%       'irefmark': index of reference marker, integer (default:1)
%       'ireftilt': index of reference tilt angle, integer (default: 0)
%       'imdim':    dimension of images (default: 2048)
%       'r':        coordinates assigned to reference markerC 
%                       [default: (x(ireftilt),y(ireftilt),1025) ]
%                       if [0,0,0] then default values are taken 
%       'handflip': add pi to intially determined tilt axis
%       'MaxIter':  Maximum number of iteration for optimization (default:
%                       2000)
%       'optimizer': optimization algorithm ('UNC' (default), 'UNCGRAD', 'MINFUNC' or
%                       'FMIN')
%       'HessUpdate': 'bfgs' (default), 'dfp', or 'steepdesc'
%
%  EXAMPLE
%   alignopts = av3_alignsetup('dmag',true, 'drot',true, 'grad',false, 'irefmark',1);
%
%  SEE ALSO
%   av3_get_coords_of_referencemarker, av3_align_tiltseries
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
alignopts.finealig  = false;
alignopts.dmag      = true;
alignopts.drot      = true;
alignopts.dbeam     = false;
alignopts.magnfoc   = false;
alignopts.grad      = false;
alignopts.irefmark  = 1;
alignopts.ireftilt  = 0;
alignopts.r         = [0,0,0];
alignopts.handflip  = false;
alignopts.imdim     = 2048;
alignopts.MaxIter   = 2000;
alignopts.finealigfile = false;
alignopts.optimizer = 'UNC';
alignopts.HessUpdate = 'bfgs';
alignopts.xml_alig   = false;

for iarg=1:2:nargin
    switch lower(varargin{iarg})
        case 'finealig'
            if ~islogical(varargin{iarg+1})
                error('finealig must be logical')
            end;
            alignopts.finealig = varargin{iarg+1};
        case 'finealigfile'
            if ~ischar(varargin{iarg+1})
                error('finealigfile must be string')
            end;
            alignopts.finealigfile = varargin{iarg+1};
        case 'xml_alig'
            if ~ischar(varargin{iarg+1})
                error('xml_alig must be a string')
            end;
            alignopts.xml_alig = varargin{iarg+1};
        case 'dmag'
            if ~islogical(varargin{iarg+1})
                error('dmag must be logical')
            end;
            alignopts.dmag = varargin{iarg+1};
        case 'drot'
            if ~islogical(varargin{iarg+1})
                error('drot must be logical')
            end;
            alignopts.drot = varargin{iarg+1};
        case 'dbeam'
            if ~islogical(varargin{iarg+1})
                error('dbeam must be logical')
            end;
            alignopts.dbeam = varargin{iarg+1};
        case 'magnfoc'
            if ~islogical(varargin{iarg+1})
                error('magnfoc must be logical')
            end;
            alignopts.magnfoc = varargin{iarg+1};
        case 'grad'
            if ~islogical(varargin{iarg+1})
                error('grad must be logical')
            end;
            alignopts.grad = varargin{iarg+1};
        case 'irefmark'
            if ( ~isscalar(varargin{iarg+1}) || ischar(varargin{iarg+1}) )
                error('irefmark must be integer');
            end;
            alignopts.irefmark = varargin{iarg+1};
        case 'ireftilt'
            if ( ~isscalar(varargin{iarg+1}) || ischar(varargin{iarg+1}) )
                error('ireftilt must be integer');
            end;
            alignopts.ireftilt = varargin{iarg+1};
        case 'r'
            if ~isvector(varargin{iarg+1})
                error('r must be 3-dim vector');
            end;
            alignopts.r = varargin{iarg+1};
        case 'handflip'
            if ~islogical(varargin{iarg+1})
                error('handflip must be logical')
            end;
            alignopts.handflip = varargin{iarg+1};
        case 'imdim'
            if ( ~isscalar(varargin{iarg+1}) || ischar(varargin{iarg+1}) )
                error('imdim must be integer');
            end;
            alignopts.imdim = varargin{iarg+1};
        case 'optimizer'
            if ~ischar(varargin{iarg+1})
                error('optimizer must be string')
            end;
            alignopts.optimizer = varargin{iarg+1};
        case 'maxiter'
            if ( ~isscalar(varargin{iarg+1}) || ischar(varargin{iarg+1}) )
                error('MaxIter must be integer');
            end;
            alignopts.MaxIter = varargin{iarg+1};
        case 'hessupdate'
            if ((strmatch(varargin{iarg+1},'bfgs')) || ...
                    (strmatch(varargin{iarg+1},'dfp')) || ...
                    (strmatch(varargin{iarg+1},'steepdesc')))
                alignopts.HessUpdate = varargin{iarg+1};
            else
                error('HessUpdate must be "bfgs", "dfp" or "steepdesc"');
            end;
        otherwise
            error(['parameter ', varargin{iarg}, ' not known']);
    end;
end;

alignopts.cent  = [floor(alignopts.imdim/2)+1, ...
    floor(alignopts.imdim/2)+1, floor(alignopts.imdim/2)+1];
%% check that parameters fit
if strcmp(alignopts.optimizer,'UNCGRAD') || strcmp(alignopts.optimizer,'MINFUNC')
    alignopts.grad = true;
    disp('Gradients must be switched on for optimizer UNCGRAD and MINFUNC');
end;
