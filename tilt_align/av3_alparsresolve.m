function [psi, mark3d, trans, dmag, drot, dbeam, magnfoc, rotfoc] = av3_alparsresolve(alpars, Matrixmark, alignopts)
% resolve alignment parameters from input vector
%
%   [psi, mark3d, trans, dmag, drot, dmagnfoc, drotfoc] =
%           av3_alparsresolve(alpars, Matrixmark, alignopts)
%
%   Coordinates of reference marker irefmark are fixed, either to
%   alignopts.r or to default (x,y-coordinates of reference projection
%   ireftilt, z=0). s(ireftilt) and dmag(ireftilt) are fixed to 1 and 0,
%   respectively.
%
%  INPUT
%   alpars       alignment parameters in list
%   Matrixmark   alignment markers (12 column list)
%   alignopts
%
%  OUTPUT
%   Matrixmark   alignment markers (12 column list)
%   irefmark     index of reference marker (default: 1)
%   ireftilt     index of reference tilt (default: index corresponding to 
%                   0 degree tilt)
%   psi          angle of tilt axis
%   mark3d       marker coordinates (3D)
%   trans        translations of projections
%   dmag         Delta magnification in each image
%   drot         Delta rotation in each image
%   magnfoc      constant for focus-dependent magnification
%   rotfoc       constant for focus-dependent rotation
%
%  SEE ALSO
%   av3_alpars_create, av3_get_coords_of_referencemarker
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

psi = alpars(1);
nmark = size(Matrixmark,3);
ntilt = size(Matrixmark,2);
mark3d = zeros(nmark,3);
cent = alignopts.imdim/2 + 1;

if (alignopts.r(1) == 0)
    % now also use ireftilt and imdim in function
    %coords = av3_get_coords_of_referencemarker(Matrixmark, ...
    %            alignopts.irefmark);
    coords = av3_get_coords_of_referencemarker(Matrixmark, ...
                alignopts.irefmark, alignopts.imdim, alignopts.ireftilt);
    mark3d(alignopts.irefmark,:)=coords';
else
    mark3d(alignopts.irefmark,:) = [alignopts.r(1)-cent, ...
        alignopts.r(2)-cent, alignopts.r(3)-cent]';
end;

scaletrans = 1;
scalemag   = 1;

ii = 2;
%% marker 3d coords
% reference marker is fixed to default
for imark=1:alignopts.irefmark-1 
    mark3d(imark,:) = alpars(ii:ii+2);
    ii = ii + 3;
end;
% irefmark is set before
for imark=alignopts.irefmark+1:nmark
    mark3d(imark,:) = alpars(ii:ii+2);
    ii = ii + 3;
end;

%% translations
trans = zeros(ntilt,2);
for itilt=1:ntilt
    trans(itilt,:) = scaletrans*alpars(ii:ii+1);
    ii = ii + 2;
end;

%% focus-dependent magnification and rotation    
% magification and rotation of reference tilt is fixed
dmag = zeros(1,ntilt);
drot = zeros(1,ntilt);
if alignopts.drot
    for itilt=1:alignopts.ireftilt-1
        dmag(itilt) = alpars(ii);
        drot(itilt) = alpars(ii+1);
        ii = ii + 2;
    end;
    for itilt=alignopts.ireftilt+1:ntilt
        dmag(itilt) = alpars(ii);
        drot(itilt) = alpars(ii+1);
        ii = ii + 2;
    end;
end;

%% beam inclination
if alignopts.dbeam
    dbeam=alpars(ii);
    ii = ii + 1;
else
    dbeam = 0;
end;

%% magnification and rotation gradient
if alignopts.magnfoc
    magnfoc = scalemag * alpars(ii);
	ii = ii + 1;
	rotfoc = alpars(ii);
else
    magnfoc = 0;
    rotfoc = 0;
end;