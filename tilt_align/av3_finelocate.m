function [Matrixmark, cumav] = av3_finelocate(Matrixmark, tiltroot, append, dim)
%   fine localization of markers in tilt series
%   
%   Matrixmark = av3_finelocate(Matrixmark, tiltroot, append)
%
%   INPUT
%   Matrixmark   array containing marker coords
%   tiltroot     root of tilt series (files should be called: <tiltroot>_itilt.em)
%   append       appendix of tilt series (e.g., '.em')
%   dim          dimension of box used for correlation (typically 2*dim
%                   gold bead)
%
%  OUTPUT
%   Matrixmark   marker array with re-centered gold beads
%   cumav        cumulative average of all beads
%
if (exist('dim','var') == 0)
    dim = 32;
end;
% disp(num2str(dim))
niter = 10;
lowpass = 0.5;

nmark = size(Matrixmark,3);
ntilt = size(Matrixmark,2);

fmask = fftshift(tom_spheremask(ones(dim,dim), lowpass*dim/2,dim/10));
av = zeros(dim,dim,nmark);
subprojvol = zeros(dim,dim,nmark,ntilt);
delta = zeros(2,nmark,ntilt);
mask = tom_spheremask(ones(dim,dim),dim/3,dim/10);
%   read in data
for imark = 1:nmark,
    for itilt =1:ntilt,
        markposx = Matrixmark(2,itilt,imark);
        markposy = Matrixmark(3,itilt,imark);
        if (markposx > -1) && (markposy > -1)
            ix = round(markposx-dim/2);
            iy = round(markposy-dim/2);
            %disp([num2str(ix),',',num2str(iy)]);
            subproj = tom_emread([tiltroot, '_', num2str(itilt), append], ...
                'subregion', [ix, iy, 1], [dim-1,dim-1,0]);
            subproj=av3_normInMask(subproj.Value, mask);
            subprojvol(:,:,imark,itilt) = subproj;
            av(:,:,imark) = av(:,:,imark) + subproj;
            imagesc(subproj');colorbar;
            imagesc(squeeze(av(:,:,imark))');
        else
            delta(1,imark,itilt) = -1000;
        end;
    end;
    %center average
    %av(:,:,imark) = tom_bandpass(av(:,:,imark),0,lowpass*dim/2,dim/10);
    dr = tom_cm(tom_limit(-av(:,:,imark),0,100000));
    av(:,:,imark) = tom_shift(av(:,:,imark),-[dr(1)-dim/2,dr(2)-dim/2]);
    %imagesc(squeeze(tom_limit(-av(:,:,imark),0,100000)'));
    %colormap gray;
end;


for iter=1:niter,
    newav = zeros(dim,dim,nmark);
    for imark=1:nmark,
        for itilt =1:ntilt,
            if (delta(1,imark,itilt) > -1000)
                %subproj = squeeze(subprojvol(:,:,itilt,imark));
                subproj = squeeze(subprojvol(:,:,imark,itilt));
                % remove projection from average
                tmp = squeeze(av(:,:,imark)) - ...
                    tom_shift(subproj, -[delta(1,imark,itilt), delta(2,imark,itilt)]);
                ccf = real(ifftshift(tom_ifourier(fmask.*tom_fourier(subproj).*conj(tom_fourier(tmp)))));
                %ccf = tom_corr(tmp,subproj);
                [dr, ~] = tom_peak(ccf,'spline');
                delta(1,imark,itilt) = dr(1)-dim/2;
                delta(2,imark,itilt) = dr(2)-dim/2;
                newav(:,:,imark) = newav(:,:,imark) + tom_shift(subproj, ...
                    -[delta(1,imark,itilt), delta(2,imark,itilt)]);
            end;
        end;
        %center average
        %newav(:,:,imark) = tom_bandpass(newav(:,:,imark),0,lowpass*dim/2,dim/10);
        dr = tom_cm(tom_limit(-newav(:,:,imark),0,100000));
        newav(:,:,imark) = tom_shift(newav(:,:,imark),-[dr(1)-dim/2,dr(2)-dim/2]);
        %av(:,:,imark) = tom_spheremask(newav(:,:,imark),dim/3,dim/10);
        av(:,:,imark) = newav(:,:,imark);
        %imagesc(squeeze(tom_limit(-newav(:,:,imark),0,100000)'));
%         colormap gray;
    end;
end;

for imark=1:nmark
    for itilt=1:ntilt
        if (delta(1,imark,itilt) > -1000)
            Matrixmark(2,itilt,imark) = round(Matrixmark(2,itilt,imark))+...
                delta(1,imark,itilt);
            Matrixmark(3,itilt,imark) = round(Matrixmark(3,itilt,imark))+...
                delta(2,imark,itilt);
        end;
    end;
end;

cumav = sum(av,3);