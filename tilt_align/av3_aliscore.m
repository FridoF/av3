function varargout = av3_aliscore(alpars, Matrixmark, alignopts)
%   calculate residual of a marker model given the marker coords
%
%   [residual, dresidual] = av3_aliresidual(Matrixmark, psi, dmag, drot)
%
%   INPUT
%   alpars      alignment parameters to be optimized (psi[1],
%               mark3d[nmark*3], trans[ntilt*2], dmag[ntilt], drot[ntilt],
%               dmagnfoc[ntilt], drotfoc[ntilt] )
%   Matrixmark  marker coordinates, tilt angles
%   alignopts   alignment options (specified using av3_alignsetup)
%
%   OUTPUT
%   residual    residual error of fit
%   dresidual   partial derivatives of residual
%
%   tilt geom:
%   
%   [cos(the_itilt)  0  -sin(the_itilt) ]  [ x_imark ]   
%   [ 0              1             0    ]  [ y_imark ]
%                                          [ z_imark ]
%
%
%   = [ cos(-psi+dpsi_itilt-pi/2) sin(-psi+dpsi_itilt-pi/2)] s_itilt [markx_itilt,imark] - [dx_itilt]
%     [-sin(-psi+dpsi_itilt-pi/2) cos(-psi+dpsi_itilt-pi/2)]         [marky_itilt,imark]   [dy_itilt]
%
%
%  SEE ALSO
%   av3_alignsetup, av3_aliresidual, av3_align_tiltseries,
%   av3_create_weipros
%
%   Copyright (c) 2005-2012
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%%
[psi, mark3d, trans, dmag, drot, dbeam, magnfoc, rotfoc] = av3_alparsresolve(...
                        alpars, Matrixmark, alignopts);
%% gradients or not
if alignopts.grad,
    [residual, dresidual_dpsi, dresidual_dmark3d, dresidual_dtrans, ...
            dresidual_dmag, dresidual_drot, dresidual_dbeam, dresidual_dmagnfoc, ...
            dresidual_drotfoc] = av3_aliresidual(Matrixmark, ...
            alignopts.cent, psi, ...
            mark3d, trans, dmag, drot, dbeam, magnfoc, rotfoc, true);
    varargout{1}=residual;
    dresidual = av3_alpars_create(Matrixmark, alignopts, ...
            dresidual_dpsi, ...
            dresidual_dmark3d, dresidual_dtrans, dresidual_dmag, ...
            dresidual_drot, dresidual_dbeam, dresidual_dmagnfoc, ...
            dresidual_drotfoc);
    varargout{2}=dresidual;
else
    residual = av3_aliresidual(Matrixmark, alignopts.cent, psi, mark3d, trans, dmag, ...
                        drot, dbeam, magnfoc, rotfoc, false);
    varargout{1}=residual;
end;
