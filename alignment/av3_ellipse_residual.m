function chi = av3_ellipse_residual(paras, x, y, z)
%   calculate residual to ellipsoid
%   function is used to get initial estimates for normal on ellipsoidal
%   surface
%   
%   chi = av3_ellipse_residual(paras)
%
%   INPUT
%       paras(1:3)     vector of offset coordinates (3-dim vector)
%       Paras(4:6)     equatorial radii (3-dim vector)
%       x,y,z          coordinates used for fitting
%
%   functional form of ellipsoid
%       ((r(1)-x_0)/a)^2 + ((r(2)-y_0)/b)^2 + ((r(3)-y_0)/c)^2 - 1 = 0 
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

chi = 0.0;
a = paras(4)^2;
b = paras(5)^2;
c = paras(6)^2;
for ii=1:size(x,2)
    val =   ((x(ii)-paras(1))^2)/a + ...
            ((y(ii)-paras(2))^2)/b + ...
            ((z(ii)-paras(3))^2)/c - 1.;
    tmp = abs(paras);
    val = val^2;
    % penalties for very large values of parameters
    if ( (tmp(1) > 10000))
        val = val + (tmp(1) -1000.)^2;
    end;
    if ( (tmp(2) > 10000))
        val = val + (tmp(2) -1000.)^2;
    end;
    if ( (tmp(3) > 10000))
        val = val + (tmp(3) -1000.)^2;
    end;
    if ( (tmp(4) > 100000000))
        val = val + (tmp(4) -100000000.)^2;
    end;
    if ( (tmp(5) > 100000000))
        val = val + (tmp(5) -100000000.)^2;
    end;
    if ( (tmp(6) > 100000000))
        val = val + (tmp(6) -100000000.)^2;
    end;
    chi = chi + val;
end;
chi = chi / size(x,2);
