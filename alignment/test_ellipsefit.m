% test ellipse fitting
motl = tom_emread('/Users/frido/npc/motl_4.em');
x = motl.Value(8,:);
y = motl.Value(9,:);
z = motl.Value(10,:);
%vol = tom_emread('/Users/frido/npc/nuc_4_recon_bin2.vol');
% choose these values so that they roughly make sense
x0=-1000.0;
y0=200.;
z0=100.;
% 
a0=200.;
b0=200.;
c0=100.;
[optiparas, Chi] = fminsearch(@(paras) av3_ellipse_residual(paras, x, y, z),...
    [x0,y0,z0,a0,b0,c0]);
% optiparas keep the optimized values for x0, y0, z0, a, b, and c
gradvecs= zeros(3,size(motl.Value,2))
gradvecs(1,:) = (x-optiparas(1))./(optiparas(4)^2);
gradvecs(2,:) = (y-optiparas(2))./(optiparas(5)^2);
gradvecs(3,:) = (z-optiparas(3))./(optiparas(6)^2);
% gradvecs(1,:) = (x-optiparas(1)).*(optiparas(5)^2).*(optiparas(6)^2);
% gradvecs(2,:) = (y-optiparas(2)).*(optiparas(6)^2).*(optiparas(4)^2);
% gradvecs(3,:) = (z-optiparas(3)).*(optiparas(4)^2).*(optiparas(5)^2);
rand('seed',1);
for ii=1:size(gradvecs,2)
    [the, psi, rho] = av3_cart2sph(gradvecs(:,ii));
    the = 180./pi * the
    psi = 180./pi * psi;
    motl.Value(17,ii) = 360*rand(1);
    motl.Value(18,ii) = psi;
    motl.Value(19,ii) = the;
end;
