function ccf = av3_local_corr(feature, vol, mask, bandpass)
%calculate local correlation function
%
%   ccf = av3_local_corr(feature, vol, mask, bandpass)
%
%   INPUT
%   feature   feature to be aligned
%   vol       volume or image
%   mask      local mask
%   bandpass  bandpass for x-correlation
%
%   OUTPUT
%   ccf       cross-correlation function
%
%   FF 08/17/2009
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%apodizing of vol
%vol = tom_spheremask(vol,size(vol,1)/2-size(vol,1)/10,size(vol,1)/20);
%figure(2);tom_imagesc(vol);

%apply bandpass to feature and vol
if exist('bandpass','var')
    feature = tom_ifourier(tom_fourier(feature).*bandpass);
    vol = tom_ifourier(tom_fourier(vol).*bandpass);
end;

%subtract mean from feature and divide by variance
feature = feature.*mask;
npix = sum(sum(sum(mask)));
mn = sum(sum(sum(feature)))/npix;
feature=feature-mask*mn;
fvari = sqrt(sum(sum(sum(feature.*feature)))/npix);
feature=feature/fvari;

%calculate local mean of volume
fmask   = conj(tom_fourier(mask));
fvol    = tom_fourier(vol);
meanvol = tom_ifourier(fvol.*fmask)/npix;

%calculate local variance
volsq = vol.*vol;
volsq = tom_ifourier(tom_fourier(volsq).*fmask);
meanvol= meanvol.*meanvol;
volsq  = sqrt((volsq - npix*meanvol)/npix);

%non-normalized CCF
ccf = tom_ifourier(conj(tom_fourier(feature)).*fvol);

%normalized CCF
ccf = ifftshift(ccf ./volsq)/npix;%(size(vol,1)*size(vol,2)*size(vol,3));

