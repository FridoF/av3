function motl_shifted = av3_shiftparticles_in_motl(motl, newcent, oldcent, scalefactor)
% shift center of particles in motiflist
%   motl_shifted = av3_shiftparticles_in_motl(motl, newcent, oldcent, binningfactor)
%
% INPUT
%   motl                motiflist
%   newcent             new center in box (3d coordinates in pixels)
%   oldcent             old center in box (e.g., [33,33,33] for 64-box
%   scalefactor         are picking coordinates in a downscaled volume
%                       (e.g., 4 for 4x de-magnified volume)
%
%   procedure assumes that subtomograms are unbinned (motl columns 11-16) 
%   and overview tomogram (columns 8-10) potentially downscaled. newcent
%   and oldcent are in subtomograms (=unbinned).
%
% OUTPUT
%   motl_shifted        motl with new particle coordinates
%
% SEE ALSO
%   av3_fastrecparticles_prescale for reconstruction of subvolumes
%
%   FF 05/21/2019
%
motl_shifted = motl;
diffvector = oldcent - newcent; % to move the center of a box to the new center
nparts = size(motl,2); % amount of particles to investigate

% This loop moves particles in the direction of the membrane (stored in the
% euler angles motl 17 to 19), over the vector diffvector. Then, the new coordinates are
% saved in motl 8 to 10). 
for ii=1:nparts
    newcoord = motl(8:10,ii) - ...
        tom_pointrotate(diffvector/scalefactor,  motl(17,ii), motl(18,ii), motl(19,ii))';
    motl_shifted(8:10,ii) = newcoord;
    newcoord = motl(11:13,ii) - ...
        tom_pointrotate(diffvector,  motl(17,ii), motl(18,ii), motl(19,ii))';
    motl_shifted(11:13,ii) = newcoord;
    newcoord = motl(14:16,ii) - ...
        tom_pointrotate(diffvector,  motl(17,ii), motl(18,ii), motl(19,ii))';
    motl_shifted(14:16,ii) = newcoord;
end

