function [res] = PlotHalfsetFSC(fsc,numofshells,fscp,fscpcv,Objectpixelsize,TitleText)
%%%%%%%%%%%%
%%%%%%%%%%%%
% This program can be used to compare the both-halfsets fsc 
% of different averages.
%
% INPUT
% fsc --- structure which contains the fsc with fieldnames:
%              --- fsc(x).fsc --- fourier shell correlation
%              --- fsc(x).com --- legend text for the plot
% numofshells --- number of fourier shells
% fscp --- fsc parameter is a vector with four elements:
%                    --- fsc figure index
%                    --- interpolation method
%                           --- 0 no interpolation
%                           --- 1 'nearest'
%                           --- 2 'linear'
%                           --- 3 'spline'
%                           --- 4 'pchip'
%                    --- number of elements
%                    if interpolation method == 0 no interpolation is done, number of elements is ignored
%                    and the fsc is plotted over numofshells
% fscpcv --- fsc plot color vector is a cell array which contains the color codes for the fsc curves
% Objectpixelsize --- /in nm
% TitleText --- string with the text of the fsc figure
%
% OUTPUT
% res --- resolution structure

% Number of fsc
numoffsc = size(fsc,2);

% Choose FSC interpolation method
if fscp(2) == 0
    InterpolM = 'none';
elseif fscp(2) == 1
    InterpolM = 'nearest';
elseif fscp(2) == 2
    InterpolM = 'linear';
elseif fscp(2) == 3
    InterpolM = 'spline';
elseif fscp(2) == 4
    InterpolM = 'pchip';
end

% FSC spacing
if ~strcmp(InterpolM,'none')
     fscspacing = (numofshells-1)./(fscp(3)-1);
else
    fscspacing = 1;
end

% Calculate resolution /in Angstrom
f = (1:fscspacing:numofshells)./(2.*Objectpixelsize.*numofshells);
x = (round((1./f).*100)./100).*10;

% FSC interpolation y-axis values
for k=1:numoffsc
    if ~strcmp(InterpolM,'none')
         res(k).fscx = x;
         res(k).fscy = interp1(1:numofshells,fsc(k).fsc',1:fscspacing:numofshells,InterpolM);
    else
         res(k).fscx = x;
         res(k).fscy = fsc(k).fsc';
    end
end

% Plot FSC
figure(fscp(1));
for k=1:numoffsc
         plot(res(k).fscy,fscpcv{k},'LineWidth',2);hold on;
end
box on;

% Find resolution at 0.5 criterion
disp('Resolution at 0.5 criterion');
for k=1:numoffsc
    dval = res(k).fscy-0.5; % bug fixed by Y. Chen
    [nn indx0(k)] = min(dval(dval>0));
    res(k).res0 = x(indx0(k));
    disp(['avg' num2str(k) ' --- ' num2str(res(k).res0)]);
end

% % Find resolution at 0.33 criterion
% disp('Resolution at 0.33 criterion');
% for k=1:numoffsc
%     dval = res(k).fscy-0.33; % bug fixed by Y. Chen
%     [nn indx1(k)] = min(dval(dval>0));
%     res(k).res1 = x(indx1(k));
%     disp(['avg' num2str(k) ' --- ' num2str(res(k).res1)]);
% end

% Add legend
for k=1:numoffsc
    if isfield(fsc(k),'com') && ~isempty(fsc(k).com)
        %LegendText{k} = [fsc(k).com ' --- ' num2str(res(k).res0) ' - ' num2str(res(k).res1)];
        LegendText{k} = [fsc(k).com ' --- ' num2str(res(k).res0)];
    else
        %LegendText{k} = ['avg' num2str(k) ' --- ' num2str(res(k).res0) ' - ' num2str(res(k).res1)];
        LegendText{k} = ['avg' num2str(k) ' --- ' num2str(res(k).res0)];
    end    
end
legend(LegendText);

% Add resolution values
set(gca,'XLim',[1 size(x,2)]);
set(gca,'XTick',2:size(x,2)/8:size(x,2));
set(gca,'XTickLabel',x(2:size(x,2)/8:size(x,2)));
set(gcf,'Color', [1 1 1]);

% Adjust y-axis
allfscy = zeros(numoffsc,size(x,2));
for k=1:numoffsc
    allfscy(k,:) = res(k).fscy;
end
minfsc = min(allfscy(:));
set(gca,'YLim',[minfsc-0.1 1]);

% Add zero line
plot(zeros(1,size(x,2)),'k--');

% Add 0.5 line
plot(0.5.*ones(1,size(x,2)),'k--');

% Add 0.33 line
%plot(0.33.*ones(1,size(x,2)),'k--');

% Add nyquist line
plot([size(x,2)/2 size(x,2)/2],[minfsc-0.1 1],'k--');

% Add title and axes text
title(TitleText);
xlabel('Resolution [A]');
ylabel('Fourier shell correlation');

