function emmap = tom_pdb2em(pdbdata, pixelsize, dim)
%AV3_PDB2EM creates a density map based on pdb-structure
%   from the pdb database: www.pdb.org.
%
%   emmap = tom_pdb2em(pdbdata, pixelsize, dim)
%
%PARAMETERS
%
%  INPUT
%   PDBDATA             expected as structure as derived from TOM_PDBREAD
%                       or pdbread
%   PIXELSIZE           desired pixelsize in Angstrom
%   DIM                 dimension of desired cube
%  
%  OUTPUT
%   emmap               volume
%
%EXAMPLE
%   read the Thermoplasma 20S proteasome from the database.
%   proteasome = pdbread('http://www.rcsb.org/pdb/files/1pma.pdb');
%   create a EM density map of 96x96x96 voxel and a objectpixelsize
%   of 2.1 A.
%   emmap = av3_pdb2em(proteasome, 2.1, 96);
%
%REFERENCES
%
%SEE ALSO
%   tom_pdb2em, www.pdb.org
%
%   created by FF 2003
%   updated by RK 2006 bugfixed - now centering the protein model prior to
%   mapping
%

%centering
minx=min([pdbdata.ATOM.X]);
miny=min([pdbdata.ATOM.Y]);
minz=min([pdbdata.ATOM.Z]);
maxx=max([pdbdata.ATOM.X]);
maxy=max([pdbdata.ATOM.Y]);
maxz=max([pdbdata.ATOM.Z]);
for i=1:size(pdbdata.ATOM,2)
    pdbdata.ATOM(i).X = pdbdata.ATOM(i).X - ((maxx + minx) / 2);
    pdbdata.ATOM(i).Y = pdbdata.ATOM(i).Y - ((maxy + miny) / 2);
    pdbdata.ATOM(i).Z = pdbdata.ATOM(i).Z - ((maxz + minz) / 2);
end;
%---


%% formfactors in solvation
%             f0[k] = c + [SUM a_i*EXP(-b_i*(k^2)) ]
%UD                i=1,5
%UD  a1  a2  a3  a4  a5  c  b1  b2  b3  b4  b5  excl_vol
%H        0.493002   0.322912   0.140191   0.040810   0.0     0.003038   10.5109   26.1257     3.14236   57.7997     1.0      5.15
%HE       0.489918   0.262003   0.196767   0.049879   0.0     0.001305   20.6593    7.74039   49.5519     2.20159    1.0      5.15
%C        2.31000    1.02000    1.58860    0.865000   0.0     0.215600   20.8439   10.2075     0.568700  51.6512     1.0      16.44
%N       12.2126     3.13220    2.01250    1.16630    0.0   -11.529       0.005700  9.89330   28.9975     0.582600   1.0      2.49
%O        3.04850    2.28680    1.54630    0.867000   0.0     0.250800   13.2771    5.70110    0.323900  32.9089     1.0      9.13
%NE       3.95530    3.11250    1.45460    1.12510    0.0     0.351500    8.40420   3.42620    0.230600  21.7184     1.0      9.
%SOD+     4.76260    3.17360    1.26740    1.11280    0.0     0.676000    3.28500   8.84220    0.313600 129.424      1.0      9.
%MG2+     5.42040    2.17350    1.22690    2.30730    0.0     0.858400    2.82750  79.2611     0.380800   7.19370    1.0      9.
%P        6.43450    4.17910    1.78000    1.49080    0.0     1.11490     1.90670  27.1570     0.526000  68.1645     1.0      5.73
%S        6.90530    5.20340    1.43790    1.58630    0.0     0.866900    1.46790  22.2151     0.253600  56.1720     1.0      19.86
%CAL2+   15.6348     7.95180    8.43720    0.853700   0.0   -14.875      -0.00740   0.608900  10.3116    25.9905     1.0      9.
%FE2+    11.0424     7.37400    4.13460    0.439900   0.0     1.00970     4.65380   0.305300  12.0546    31.2809     1.0      9.
%ZN2+    11.9719     7.38620    6.46680    1.39400    0.0     0.780700    2.99460   0.203100   7.08260   18.0995     1.0      9.
%SE      17.0006     5.81960    3.97310    4.35430    0.0     2.84090     2.40980   0.272600   15.2372   43.8163     1.0      9.
%AU      16.8819    18.5913    25.5582     5.86000    0.0     12.0658     0.461100  8.62160     1.48260  36.3956     1.0      19.86
f(1) = 0.493002 + 0.322912 + 0.140191 + 0.040810 + 0.003038  - rho_solv * 5.15; % H
f(2) = 0.489918 + 0.262003 + 0.196767 + 0.049879 + 0.001305  - rho_solv * 5.15; % He
f(3) = 2.31000  + 1.02000  + 1.58860  + 0.865000 + 0.215600  - rho_solv *16.44; % C
f(4) = 12.2126  + 3.13220  + 2.01250  + 1.16630  - 11.529    - rho_solv * 2.49;   % N
f(5) = 3.04850  + 2.28680  + 1.54630  + 0.867000 + 0.250800  - rho_solv * 9.13; % O
f(6) = 3.95530  + 3.11250  + 1.45460  + 1.12510  + 0.351500  - rho_solv * 9.; % Ne
f(7) = 5.42040  + 2.17350  + 1.22690  + 2.30730  + 0.858400  - rho_solv * 9.; % Mg
f(8) = 6.43450  + 4.17910  + 1.78000  + 1.49080  + 1.11490   - rho_solv * 5.73;  % P
f(9) = 6.90530  + 5.20340  + 1.43790  + 1.58630  + 0.866900  - rho_solv * 19.86; % S
f(10)= 15.6348  + 7.95180  + 8.43720  + 0.853700 - 14.875    - rho_solv * 9.; % Ca
f(11)= 17.0006  + 5.81960  + 3.97310  + 4.35430;


emmap = zeros(dim,dim,dim);
x = round([pdbdata.ATOM.X]/pixelsize)+ floor(dim/2); 
y = round([pdbdata.ATOM.Y]/pixelsize)+ floor(dim/2);
z = round([pdbdata.ATOM.Z]/pixelsize)+ floor(dim/2);
ATOM = reshape([pdbdata.ATOM.AtomName],4,size([pdbdata.ATOM.AtomName],2)/4);
for iATOM = 1:size(x,2)
    if ((x(iATOM) > 0) &  (y(iATOM) > 0) &  (z(iATOM) > 0))
        if (strmatch(ATOM(2,iATOM),'C') )  
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+6;
        elseif (strmatch(ATOM(2,iATOM),'N')) 
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+7;
        elseif (strmatch(ATOM(2,iATOM),'O')) 
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+8;
        elseif (strmatch(ATOM(2,iATOM),'H')) 
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+1;
        elseif (strmatch(ATOM(2,iATOM),'P')) 
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+15;
        elseif (strmatch(ATOM(2,iATOM),'S')) 
            emmap(x(iATOM),y(iATOM),z(iATOM))=emmap(x(iATOM),y(iATOM),z(iATOM))+16;
        end;
    end;
end;