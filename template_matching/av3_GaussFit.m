function residual = av3_GaussFit(gaupars, xhist, yhist)
%
%   residual = av3_GaussFit(gaupars, xhist, yhist)
%   
%   gaupars      3 parameters for Gauss fit: [N_parts, mean, sigma]
%   xhist        x-values of histogram (=bins)
%   yhist        y-values of histogram (actual histogram)
%
%  SEE ALSO
%   av3_fitgauss
%
%   03/2011 FF
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
N = size(xhist,1);
Y = av3_gauss( xhist, gaupars(1), gaupars(2), gaupars(3));
residual = sum((yhist-Y).^2)/N;