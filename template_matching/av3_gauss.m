function Y = av3_gauss( x, N, meanv, sigma)
%
%   Y = av3_gauss( x, N, meanv, sigma)
%
%   x      x-values
%   N      integral of gaussian
%   meanv  mean of gaussian
%   sigma  width of gaussian
%
dx = x(2)-x(1);
Y = dx*N/(sqrt(2*pi)*sigma) .*exp(-((x-meanv).^2)/(2*sigma.^2));