function filtered = av3_prepmask(invol, thres, smooth)
% generate binarized mask
%
%   filtered = av3_prepmask(invol, thres, smooth)
filtered = tom_filter(invol,smooth,'circ');
kk = find(filtered > thres);
filtered = 0*filtered;
filtered(kk) = 1;

