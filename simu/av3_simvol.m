dens = av3_simvol(pdbfile, pixsize, boxsize)
%   create density of pdb file
%
%   dens = av3_simvol(pdbfile, pixsize, boxsize)
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%


pdb = tom_pdbread(pdbfile);
dens = tom_pdb2em(pdb, pixsize, boxsize);
%anti-aliasing
dens = tom_bandpass(th_open_fil,0,boxsize/2-2,(boxsize/2)/10);
