function simtomo = av3_simutomo(dens, def, pixelsize, tiltrange, tiltincr, ... 
                                SNR, the, psi, phi, globalsigma)
%
%   simtomo = av3_simutomo(dens, tiltrange, tiltincr, SNR, the, psi, phi, globalsigma)
%   
%   simulate a tomogram of a given volume realistically.
%
%   fixed CTF parametersacceleration voltage=300kV, C_s=2, b-factor=0.4
%
%   INPUT
%   dens        density (3 dim volme)
%   def         defocus (in mum)
%   pixelsize   pixel size (in A)
%   tiltrange   tilt range ([-60,60]) (in deg)
%   tiltincr    tilt increment (in deg)
%   SNR         signal-to-noise ratio (vari_signal/var_noise)
%   the         rotation of dens - theta angle
%   psi         psi angle
%   phi         phi angle
%   globalsigma approximate stdev of projs by mini-tilt series? - takes
%               much longer ...
%
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
 

%% rotate volume according to specified euler angles
rotvol = double(tom_rotate(dens,[phi psi the]));

%% calculate CTF
[ctf, amplitude] = tom_create_ctf(def, squeeze(sum(dens,3)), pixelsize/10, 300, 2, 0.4);
ctf=-ctf;
amplitude=-amplitude;
ctf = 0.93*ctf+0.07*amplitude;
%ctf = -tom_create_ctf(def, squeeze(sum(dens,3)), pixelsize/10, 300, 2, 0.4);
mask = tom_spheremask(ones(size(dens)),size(dens,1)/2-3,(size(dens,1)/2-3)/20);
sigma = 0;
irun=0;

%% pre-calculate average stdv for a whole tilt series
if (exist('globalsigma'))
    for ipro=-90:tiltincr:90
        irun = irun+1;
        %calc projection
        proj = squeeze(sum(tom_rotate(rotvol,[270,90,ipro]),3));
        %multiply by ctf
        proj = real(tom_ifourier(ifftshift(ctf.*fftshift(tom_fourier(proj)))));
        [mv mn mx stv tmp] = tom_dev(proj,'noinfo');
        sigma = sigma+ tmp;
    end;
    sigma = sigma/irun;
else
    %calc 0 deg projection
    proj = squeeze(sum(rotvol,3));
    %multiply by ctf
    proj = real(tom_ifourier(ifftshift(ctf.*fftshift(tom_fourier(proj)))));
    [mv mn mx stv sigma] = tom_dev(proj,'noinfo');
end;

%% take care of signal reduction due to ctf
% ratio of white noise with and without mult by ctf
[mv mn mx stv corrfac]=tom_dev(real(tom_ifourier(ifftshift(ctf.*tom_spheremask(fftshift(...
    tom_fourier(tom_error(zeros(size(ctf,1),size(ctf,2)),'G',0,1.))),10,1)))));
[mv mn mx stv whitenoise]    =tom_dev(real(tom_ifourier(ifftshift(tom_spheremask(fftshift(...
    tom_fourier(tom_error(zeros(size(ctf,1),size(ctf,2)),'G',0,1.))),10,1)))));
corrfac = whitenoise/corrfac;
sigma = sigma *corrfac;


%% generate weighting function for reconstruction
s1 = size(rotvol,1);
s2 = size(rotvol,2);
[x,y]=ndgrid(-s1/2:s1/2-1,-s2/2:s2/2-1);
mask = (abs(x).*tom_spheremask(ones(size(rotvol,1),size(rotvol,2)),s1/2-1,1));

%% simulate tomogram
simtomo = zeros(size(rotvol,1),size(rotvol,2),size(rotvol,3),'single');
for ipro=tiltrange(1):tiltincr:tiltrange(2)
    proj = squeeze(sum(tom_rotate(rotvol,[270,90,ipro]),3));
    % ctf dependent contribution of noise
    noisy = tom_error(proj,'G',0,0.5/SNR*sigma);
    % ctf independent contribution of noise
    bg = tom_bandpass(tom_error(zeros(size(noisy)),'G',0,0.5/SNR*sigma),0,1,0.2*size(noisy,1));
    % add both contributions (ratio 1:1) in Fourier space
    tmp = fftshift(tom_fourier(noisy)).*ctf + fftshift(tom_fourier(bg));
    tmp = ifftshift(tmp.*mask);
    noisy = real(tom_ifourier(tmp));
    % back projection
    tom_backproj3dc(simtomo,single(noisy), single(0), single(-ipro), [single(0),single(0),single(0)]);
end;

%% normalize to stdv
[mnv, maxv, minv, stv] = tom_dev(simtomo,'noinfo'); 
simtomo = (simtomo-mnv)/stv;
