function autocorr = av3_autocorr(vol)
%   compute autocorrelation function
%
%   autocorr = av3_autocorr(vol)
%
%   INPUT
%   vol       volume
%
%   OUTPUT
%   autocorr  autocorrelation function
%
%   SEE ALSO
%
%   AV3_PR
%
%   FF 08/16/10
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

fvol = tom_fourier(vol);
autocorr = real(ifftshift(tom_ifourier(fvol.*conj(fvol))))/...
            (size(vol,1)*size(vol,2)*size(vol,3));