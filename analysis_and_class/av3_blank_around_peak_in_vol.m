function [vol, CCC, newr] = av3_blank_around_peak_in_vol(vol, r, radius)
%
%   blank volume (e.g., corr volume) at peak in vicinity of vector r
%
%   [vol, CCC, newr] = av3_blank_around_peak_in_vol(vol, r, radius)
%
%   vol     volume
%   r       vector (x,y,z)
%   radius  radius in which volume is zeroed
%
%   vol     volume with empty spheres at peaks
%   CCC     value of vol at maximum
%   newr    position of peak (vector)
%
%   FF 04/19/2010
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

x=r(1);y=r(2);z=r(3);
box = tom_spheremask(vol(x-radius:x+radius,y-radius:y+radius,z-radius:z+radius),radius);
[rs, CCC] = tom_peak(box);
x = rs(1) + x - radius; y = rs(2) + y - radius; z = rs(3) + z - radius;
box = vol(x-radius:x+radius,y-radius:y+radius,z-radius:z+radius);
box = box - tom_spheremask(box,radius);
vol(x-radius:x+radius,y-radius:y+radius,z-radius:z+radius) = box;
newr = [x,y,z];