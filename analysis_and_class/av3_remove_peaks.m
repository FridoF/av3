function ccvol = av3_remove_peaks(ccvol, motl, radius);
%   remove peaks from ccvol according to motl
%
%   ccvol   correlation volume
%   motl    motive list
%
%   ccvol   correlation volume with peaks removed
%
%   FF 04/19/2010
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
for i = 1:size(motl,2),
    x = motl(8,i); y = motl(9,i);z = motl(10,i);
    [ccvol, CCC, newr] = av3_blank_around_peak_in_vol(ccvol, [x,y,z], radius);
end;