function pr = av3_fastpr(vol)
%   calculate radial distribution function (using FFT)
%
%   pr = av3_fastpr(vol)
%
%   INPUT
%   vol     volume
%   pr      radial distribution function P(r)
%
%   SEE ALSO
%
%   AV3_PR
%
%   10/01/10 FF
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
autocorr = av3_autocorr(vol);
pr = zeros(floor(sqrt(3)*size(vol,1)),1);
cent = size(vol);
cent(1) = floor( cent(1) /2 + 1);
cent(2) = floor( cent(2) /2 + 1);
cent(3) = floor( cent(3) /2 + 1);
for ix=1:size(vol,1)
    for iy=1:size(vol,2)
        for iz=1:size(vol,3)
            fhist = sqrt( (ix-cent(1))^2 + (iy-cent(2))^2 + (iz-cent(3))^2 ) +1;
            ilow = floor(fhist);
            ihigh = ceil(fhist);
            d = fhist - ilow;
            % linear interpolation
            if d > 0
                pr(ilow) = pr(ilow) + (1-d) * autocorr(ix,iy,iz);
                pr(ihigh) = pr(ihigh) + d * autocorr(ix,iy,iz);
            else
                pr(ilow) = pr(ilow) + autocorr(ix,iy,iz);
            end;
        end;
    end;
end;
