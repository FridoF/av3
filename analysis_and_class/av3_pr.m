function pr = av3_pr(vol, nbin)
%   calculate radial distribution function of a volume
%
%   pr = av3_pr(vol)
%
nvox = size(vol,1)*size(vol,2)*size(vol,3);
if (exist('nbin','var') ~= true)
    nbin = floor(nvox/2);
    scf = 1;
else
    if nbin> floor(nvox/2)
        nbin = floor(nvox/2);
        scf = 1;
        disp('Warning: nbin must not exceed floor(ndim/2)!');
        disp('         set nbin = floor(ndim/2)!');
    else
        scf = nbin/floor(nvox/2);
    end;
end;
pr = zeros(nbin,1);
nz = size(vol,3);ny = size(vol,2);nx = size(vol,1);
for ivox=1:nvox-1
    z1 = ceil( ivox/(nx*ny));
    y1 = ceil((ivox - (z1-1)*nx*ny ) / nx );
    x1 = ceil((ivox - (z1-1)*nx*ny - (y1-1)*nx ) );
    for jvox=ivox+1:nvox
        z2 = ceil(jvox/(nx*ny));
        y2 = ceil((jvox - (z2-1)*nx*ny ) / nx );
        x2 = jvox - (z2-1)*nx*ny - (y2-1)*nx ;
        ibin = round(scf*sqrt( (x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2 ));
        if ibin > nbin*sqrt(3)
            disp('problem!');
        end;
        if ( (ibin <= nbin) && (ibin > 0) )
            pr(ibin) = pr(ibin) + vol(ivox)*vol(jvox);
        end;
    end;
end;