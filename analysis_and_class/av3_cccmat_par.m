function [ccc, covar, xc] = av3_cccmat_par(motl, particlefilename, wedgelist, mask, hipass, lowpass, iclass, ibin)
% AV3_CCCMAT computes correlation- and covariance matrix of particles -
% parallel mode. Particles MUST all have same wedge for parallel mode!
%
%   [ccc covar xc] = av3_cccmat_par(motl, particlefilename, wedgelist,mask,hipass,lowpass,iclass,ibin)
%
% PARAMETERS
%  INPUT
%   motl                MOTive List
%   particlefilename    filename of 3D-particles to be aligned and averaged
%                           'particlefilename'_#no.em
%   wedgelist           array containing the tilt range - 1st: no of
%                           tomogram, 2nd: minimum tilt, 3rd: max tilt
%   mask                mask - make sure dims are all right!
%   hipass              hipass - for X-corr
%   lowpass             lowpass - for X-corr
%   iclass              class of particles - fixed to 1 or 2
%   ibin                binning - default 0
%   dispflag            flag for display - if set to 'nodisp' then no
%                       display, e.g. for batch mode - otherwise display.
%
%  OUTPUT
%   ccc                 constrained cross correlation matrix
%   covar               unconstrained covariance matrix
%   xc                  unconstrained correlation matrix
%
%   The cross correlation coefficient is calculated in real space, thus the
%   translation is NOT determined. The CCC that is calculated here is
%   constrained in the following sense: For computing the correlation of
%   any pair of particles, the data is constrained to the part of Fourier
%   space that is sampled by both data. This can be achieved by
%   multiplying a mask in Fourier space. This normalization is only done
%   when FLAG is set to 'norm'. If FLAG is 'unchanged', then the
%   CCC is computed without normalizing the particles to the standard
%   deviation. The default is 'norm' where normalization is performed.
%
%   As an additional property the covariance matrix is computed. 
%   For subsequent evaluation the eigenvalues and vectors should be
%   computed using EIG of EIGS.
%
%   Procedure was formerly called TOM_PCA.
%
% EXAMPLE
%
%    matlabpool open local 8;
%
% SEE ALSO
%   TOM_CORR, TOM_ORCD
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
%
%   changes
%   12/20/06 FF - removed tom_spheremasks for speedup
%   01/05/07 FF - precompute indexgrid for shifts
%   05/06/10 FF - matlab parallelization

if nargin < 8
    ibin = 0;
end;
if ibin>0
    mask = tom_bin(mask,ibin);
    % account for binning in rotation center
    rcent = floor(size(mask)./2)-0.25;% so far only 'experimentally' checked for even dim
end;
% prep for index grid
[indxgridx, indxgridy, indxgridz]=...
    ndgrid( -floor(size(mask,1)/2):-floor(size(mask,1)/2)+(size(mask,1)-1),...
    -floor(size(mask,2)/2):-floor(size(mask,2)/2)+size(mask,2)-1, ...
    -floor(size(mask,3)/2):-floor(size(mask,3)/2)+size(mask,3)-1);
% end prep
if nargin < 7 
    iclass = 0;
end;
% itomo_old = -1;
npixels = sum(sum(sum(mask)));
mask_bandpass = tom_spheremask(ones(size(mask)),lowpass,2) - tom_spheremask(ones(size(mask)),hipass,2);
%particles MUST have same wedge for parallel mode
minangle= wedgelist(2,1);
maxangle= wedgelist(3,1);
wedge = av3_wedge(mask,minangle,maxangle);
wedge2 = wedge;

npart = size(motl,2);
covar = zeros(npart,npart);
xc = zeros(npart,npart);
ccc = zeros(npart,npart);
for indpart1 = 1:npart
    ifile1 = motl(4,indpart1);
    if ( (motl(20,indpart1) == iclass) | (motl(20,indpart1) == 1) | (motl(20,indpart1) == 2) )
%         itomo = motl(5,indpart1);
        xshift = motl(11,indpart1);
        yshift = motl(12,indpart1);
        zshift = motl(13,indpart1);
        tshift = [xshift yshift zshift]/(2^ibin);
        phi = motl(17,indpart1);
        psi = motl(18,indpart1);
        the = motl(19,indpart1);
        name = [particlefilename '_' num2str(ifile1) '.em'];
        particle = tom_emread(name);
        if ibin > 0
            particle = tom_bin(particle.Value,ibin);
        else
            particle = particle.Value;
        end;
%         if itomo_old ~= itomo %wedge stuff - exact weighting according to list
%             xx = find(wedgelist(1,:) == itomo);
%             minangle= wedgelist(2,xx);
%             maxangle= wedgelist(3,xx);
%             wedge = av3_wedge(particle,minangle,maxangle);
%             itomo_old = itomo;
%         end;
        if (ibin == 0)
             rpart1 = double(tom_rotate(av3_shift(particle,-tshift,...
                 indxgridx,indxgridy,indxgridz),[-psi,-phi,-the]));
        else
             rpart1 = double(tom_rotate(av3_shift(particle,-tshift,...
                 indxgridx,indxgridy,indxgridz),[-psi,-phi,-the],'linear',rcent));
        end;
        frpart1 = fftshift(tom_fourier(rpart1)).*mask_bandpass;
        tmpwei1 = tom_maskrotate(wedge,[-psi,-phi,-the],'linear');
%         itomo_old2=0;
        parfor indpart2 =indpart1:npart
            if ((motl(20,indpart2) == iclass) | (motl(20,indpart2) == 1) | (motl(20,indpart2) == 2))
%                 itomo2 = motl(5,indpart2);
                xshift = motl(11,indpart2);
                yshift = motl(12,indpart2);
                zshift = motl(13,indpart2);
                tshift = [xshift yshift zshift]/(2^ibin);
                phi = motl(17,indpart2);
                psi = motl(18,indpart2);
                the = motl(19,indpart2);
                ifile2 = motl(4,indpart2);
                name = [particlefilename '_' num2str(ifile2) '.em'];
                particle2 = tom_emread(name);
                if ibin > 0
                    particle2 = tom_bin(particle2.Value,ibin);
                else
                    particle2 = particle2.Value;
                end;
%                 if itomo_old2 ~= itomo2 %wedge stuff - exact weighting according to list
%                     xx = find(wedgelist(1,:) == itomo2);
%                     minangle2= wedgelist(2,xx);
%                     maxangle2= wedgelist(3,xx);
%                     wedge2 = av3_wedge(particle2,minangle2,maxangle2);
%                     itomo_old2 = itomo2;
%                 end;
                if (ibin == 0)
                    rpart2 = double(tom_rotate(av3_shift(particle2,-tshift,...
                        indxgridx,indxgridy,indxgridz),[-psi,-phi,-the]));
                else
                    rpart2 = double(tom_rotate(av3_shift(particle2,-tshift,...
                        indxgridx,indxgridy,indxgridz),[-psi,-phi,-the],'linear',rcent));
                end;
                covar(indpart1,indpart2) = sum(sum(sum(( (rpart1 ).*rpart2.*mask))));
                tmp1 = rpart1.*mask;
                tmp2 = rpart2.*mask;
                mn1 = (sum(sum(sum(tmp1))))/npixels;
                mn2 = (sum(sum(sum(tmp2))))/npixels;
                tmp1 = tmp1 - mask.*mn1;
                tmp2 = tmp2 - mask.*mn2;
                stv1 = sqrt(sum(sum(sum(tmp1.*tmp1))));tmp1 = tmp1/stv1;
                stv2 = sqrt(sum(sum(sum(tmp2.*tmp2))));tmp2 = tmp2/stv2;
                xc(indpart1,indpart2) = sum(sum(sum((tmp1.*tmp2))));
                tmpwei2 = 2*tom_limit(tom_limit(double(tom_rotate(wedge2,[-psi,-phi,-the])),0.5,1,'z'),0,0.5);
                tmpwei = tmpwei1.*tmpwei2;
                %wpix = sum(sum(sum(tmpwei)));
                cpart1 = frpart1.*tmpwei;
                cpart1 = real(tom_ifourier(ifftshift(cpart1)));
                cpart2 = fftshift(tom_fourier(rpart2)).*tmpwei.*mask_bandpass;
                cpart2 = real(tom_ifourier(ifftshift(cpart2)));
                cpart1 = cpart1.*mask;
                mn1 = (sum(sum(sum(cpart1))))/npixels;
                cpart1 = cpart1 - mask.*mn1;
                stv1 = sqrt(sum(sum(sum(cpart1.*cpart1))));
                cpart1 = cpart1/stv1;
                cpart2 = cpart2.*mask;mn2 = (sum(sum(sum(cpart2))))/npixels;
                cpart2 = cpart2 - mask.*mn2;
                stv2 = sqrt(sum(sum(sum(cpart2.*cpart2))));
                cpart2 = cpart2/stv2;
                ccc(indpart1,indpart2) = sum(sum(sum(cpart1.*cpart2)));
%                 if (strcmp(dispflag,'nodisp')~= 1 )
%                     imagesc(ccc);colorbar;drawnow;
%                 end;
            else
                ccc(indpart1,indpart2) = 0;%bugfix 4.08.04 FF
                xc(indpart1,indpart2) = 0;%
                covar(indpart1,indpart2) = 0;
            end;
        end;%if - class
    end;
    disp(['Correlation and covariance computed for particle no ' num2str(ifile1) ' ...']);
end;
%symmetrize matrices
for indpart1 = 1:npart
    ccc(indpart1,indpart1) = 1;
    xc(indpart1,indpart1) = 1;
    for kk = indpart1:npart-1
        indpart2 = kk + 1;
        covar(indpart2,indpart1) = covar(indpart1,indpart2);
        xc(indpart2,indpart1)    = xc(indpart1,indpart2);
        ccc(indpart2,indpart1)   = ccc(indpart1,indpart2);
    end;
end;
%ccc = ccc+eye(size(motl,2));
