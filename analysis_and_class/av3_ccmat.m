function cc = av3_ccmat(particlefilename, appendix, npart, mask,hipass,lowpass,ibin)
% AV3_CCMAT computes correlation- matrix of densities - designed for
% densities not affected by missing wedge etc. The primary use of this
% function is to compare desnities resulting from single particle
% classification.
%
%   cc = av3_ccmat(particlefilename, appendix, npart, mask,hipass,lowpass,ibin)
%
% PARAMETERS
%  INPUT
%   particlefilename    filename of 3D-particles to be correlated
%                           'particlefilename'_#no.em or
%                           'particlefilename'_#no.mrc
%   appendix            appendix for files - em or mrc
%   npart               number of densities (=last index)
%   mask                mask - make sure dims are all right!
%   hipass              hipass - for X-corr
%   lowpass             lowpass - for X-corr
%   ibin                binning - default 0
%
%  OUTPUT
%   ccc                 constrained cross correlation matrix
%
%
% EXAMPLE
% mask = tom_spiderread('/fs/sandy03/lv04/pool/pool-titan1/CWT/combined/rec/projm_29_31__34_HiATPyS1mcl_39/output/models/mask_refine.spi');
% mask=mask.Value;
% cc = av3_ccmat('/fs/sandy03/lv04/pool/pool-titan1/CWT/combined/rec/projm_29_31__34_HiATPyS1mcl_39/output/models/align/model_aligned', ...
%        'mrc', 7, mask,0,100,2);
% tree = linkage(squareform(1-cc),'average');
% dendrogram(tree, 'ColorThreshold', .08);
%
% SEE ALSO
%   av3_cccmat, TOM_CORR, TOM_ORCD
%
%   Copyright (c) 2005-2012
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
%

if nargin < 7
    ibin = 0;
end;
if ibin>0
    mask = tom_bin(mask,ibin);
end;

npixels = sum(sum(sum(mask)));
cc = zeros(npart,npart);
for indpart1 = 1:npart
    if strcmp(appendix,'em')
        name = [particlefilename '_' num2str(indpart1) '.em'];
        part1 = tom_emread(name);
    elseif strcmp(appendix,'mrc')
        name = [particlefilename '_' num2str(indpart1) '.mrc'];
        part1 = tom_mrcread(name);
    else
        error('appendix must be em or mrc');
    end;
    if ibin > 0
        part1 = tom_bin(part1.Value,ibin);
    else
        part1 = part1.Value;
    end;
    part1 = tom_bandpass(part1,hipass,lowpass);
    tmp1 = part1.*mask;
    mn1 = (sum(sum(sum(tmp1))))/npixels;
    tmp1 = tmp1 - mask.*mn1;
    stv1 = sqrt(sum(sum(sum(tmp1.*tmp1))));tmp1 = tmp1/stv1;
    for indpart2 =indpart1:npart
        if strcmp(appendix,'em')
            name = [particlefilename '_' num2str(indpart2) '.em'];
            part2 = tom_emread(name);
        else
            name = [particlefilename '_' num2str(indpart2) '.mrc'];
            part2 = tom_mrcread(name);
        end;
        if ibin > 0
            part2 = tom_bin(part2.Value,ibin);
        else
            part2 = part2.Value;
        end;
        part2 = tom_bandpass(part2,hipass,lowpass);
        tmp2 = part2.*mask;
        mn2 = (sum(sum(sum(tmp2))))/npixels;
        tmp2 = tmp2 - mask.*mn2;
        stv2 = sqrt(sum(sum(sum(tmp2.*tmp2))));tmp2 = tmp2/stv2;
        cc(indpart1,indpart2) = sum(sum(sum((tmp1.*tmp2))));
    end;
    disp(['Correlation computed for particle no ' num2str(indpart1) ' ...']);
end;
%symmetrize matrices
for indpart1 = 1:npart
    cc(indpart1,indpart1) = 1;
    for kk = indpart1:npart-1
        indpart2 = kk + 1;
        cc(indpart2,indpart1) = cc(indpart1,indpart2);
    end;
end;