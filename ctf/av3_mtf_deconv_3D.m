function im_deconv = av3_mtf_deconv_3D(im, mtf)
% im_deconv = av3_mtf_deconv_3D(im, mtf)
%
% This function deconvolutes an image using the mtf/psf of the (respective)
% CCD. Usage: im        : image (3D volume)
%             mtf       : modulation transfer function (3D)
%             im_deconv : deconvoluted output volume
%
% Prepare psf as follows :
% xxx=tom_rawread('CM300_mtf.dat','float','le',[1 64 1],0,0);
% mcf = zeros(64,256,128);
% for ii=1:256, for jj=1:128, mtf(:,ii,jj) = xxx; end;end;
% mtf_cart = tom_sph2cart(mtf);
% mtf_four = fftshift(mtf_cart);
% psf = fftshift(tom_ifourier(mtf_four));
% psf_256 = zeros(256,256,256);
% psf_256(65:128+64,65:128+64,65:128+64) = real(psf);
% psf_256 = ifftshift(psf_256);% final psf for 2K array
% tom_emwrite('psf_CM300_3D_256.em',psf_256);
% mtf_256 = abs(tom_fourier(psf_256));
% im_deconv = av3_mtf_deconv_3D(im, mtf_256);
% % 1 time binned
% mtf_128 = fftshift(mtf_256);
% mtf_128 = ifftshift(mtf_128(65:64+128,65:64+128,65:64+128));
%
% 20.05.05 FF, modyfied by MB
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%imdeconv = deconvwnr(im.Value,psf);
%mcf_four = abs(tom_fourier(psf));
%mcf_four = ifftshift(tom_spheremask(fftshift(abs(mcf_four)),800));
% low pass inconsistant with tom_limit, see below
%ii = find(abs(mcf_four)>0.005);jj = find(abs(mcf_four)<=0.005);
fim = tom_fourier(im);
fim = (fim./abs(fim)) .* (abs(fim)./(tom_limit(mtf,0.17,1)));
%  phase (is image/amp) x     amplitude/MTF
im_deconv = real(tom_ifourier(fim));% tom_imagesc(fim);
%fimfil = tom_bin(tom_bandpass(fim,0,240,5),2);
