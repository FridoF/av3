function ctf_sum = av3_effective_ctf(motl, Dz_mean, pixsize, npix, tiltangles)
%   ctf_sum = av3_effective_ctf(motl, Dz_mean, npix, tiltangles)
%
%   motl       motif list
%   Dz_mean    mean defocus in tilt series
%   pixsize    pixel size in nm
%   
%   
npart=size(motl,2);
meanz = mean(motl(10,:));
z = (motl(10,:) - meanz) * pixsize;
tiltangles = tiltangles/180*pi;
ctf_sum = zeros(1,ceil(npix/2));
for ii=1:npart
    dx = motl(9,ii) * pixsize;
    for iang=1:max(size(tiltangles))
        Dz = Dz_mean + 1/1000 * (z(ii) + dx*sin(tiltangles(iang))); % convert to mn
        %disp(['Local defocus: ', num2str(Dz)])
        [ctf_out, ~] = av3_ctf(Dz, .07, pixsize, 300, npix, 2);
        ctf_sum = ctf_sum + abs(ctf_out);
    end;
    disp(['particle ' num2str(ii) ' done']);
end;

    