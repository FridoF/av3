function n = av3_ps_noisebg(s,n1,n2,n3,n4)
%   empirical formula to approximate noise background in power spectra
%
%   n = av3_ps_noisebg(n1,n2,n3)
%
%   n = n1*exp(n2*s+n3*s.^2+n4*sqrt(s))
%   Saad et al, JMB 2001 (B-factor in EM)

n = n1*exp(n2*s+n3*s.^2+n4*sqrt(s));