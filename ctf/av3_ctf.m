function [y, zeropos] = av3_ctf(Dz, phase_amp, pix_size, voltage, pixs, Cs, alpha, Cc, deltaE)
%AV3_CTF calculates CTF (pure phase contrast)
%
%   [ctf_out, zeropos] = av3_ctf(Dz, phase_amp, pix_size, voltage, pixs, Cs, alpha, Cc, deltaE)
%
%   The one-dimensional CTF is calculated and plotted. In case no figure is
%   displayed open one (see also example). The function can be used to get
%   a quick overview of the CTF for cterian imaging conditions.
%
%PARAMETERS
%   INPUT
%   Dz       : Defocus (<0 underfocus, >0 overfocus) (in \mu m)
%   phase_amp: ratio amplitude / phase constrast
%   pix_size : pixel size (in nm) (default: 0.47 nm)
%   voltage  : accelerating Voltage (in keV) (default: 300 kV)
%   pixs     : Number of pixels used (default: 2048)
%   Cs       : sperical aberration in mm(default: 2 mm)
%   alpha    : illumination aperture in mrad (default 0.02 - reasonable for FEG)
%   Cc       : chrmatic aberration in mm -(default 2.2 mm)
%   deltaE   : energy width in eV (default 0.8 eV)
%
%   OUTPUT
%   ctf_out  : vector of dim pixs/2 containig the ctf (if pixs is 
%                                    not even: dim = ceil(pixs/2))
%   zeropos  : positions of ctf zeros
%
%EXAMPLE
%   [ctf_out, zeropos] = av3_ctf(-4,.1, .47, 300,2048,2);
%
% SEE ALSO
%    TOM_CREATE_CTF TOM_FOURIER TOM_IFOURIER 
%
%    Copyright (c) 2004
%    TOM toolbox for Electron Tomography
%    Max-Planck-Institute for Biochemistry
%    Dept. Molecular Structural Biology
%    82152 Martinsried, Germany
%    http://www.biochem.mpg.de/tom
%
%
%   10/09/02 FF
error(nargchk(1,10,nargin));

if nargin<9
  deltaE=0.8; 
end;
if nargin<8
  Cc=2.2/1000; 
else
   Cc=Cc/1000; 
end;
if nargin<7
  alpha=0.02/1000; 
else
   alpha=alpha/1000; 
end;
if nargin<6
  Cs=2*10^(-3); 
else
   Cs=Cs*10^(-3); 
end;
if nargin<5
    pixs=2048;
end;
if nargin<4
    voltage=300000; 
else
    voltage = voltage * 1000;
end;
if nargin <3
    pix_size=0.47*10^(-9);
else
    pix_size=pix_size*10^(-9);
end;
if nargin <2
    phase_amp=.07;
end;
Dz=Dz*10^(-6); %from micrometer to meter
voltagest=voltage*(1+voltage/1022000); %for relativistic calc
lambda=sqrt(150.4/voltagest)*10^-10;
nyqvist = 1/(2*pix_size);
q=0:1/((pixs-1)*pix_size):nyqvist;% von, Increment, Nyqvist
phase = pi/2* (Cs*lambda.^3.*q.^4 - 2*Dz*lambda*q.^2);
y = sqrt(1-phase_amp.^2)*sin(phase) + phase_amp*cos(phase);
% new function: include also envelope function:
% 1) spatial coherence
%  alpha: illumination aperture - assume 0.02mrad
Ks = exp(- ((pi*(Cs.*lambda.^2.*q.^3 - Dz.*q)*alpha).^2)/log(2));
% 2) temporal coherence
delta = Cc*deltaE/voltage;
%Kt = exp(- (pi*lambda*delta*q.^2/2));% alter according to Reimer
Kt = exp(- (pi*lambda*delta*q.^2/(4*log(2))).^2);
K = Kt.*Ks;
ctf_sq = K.^2.*y.^2;
%approximate CTF zeros
ctf_zero1 = 1/sqrt(lambda*abs(Dz));
ctf_zero2 = sqrt(2)*ctf_zero1;
ctf_zero3 = sqrt(3)*ctf_zero1;
ctf_zero4 = sqrt(4)*ctf_zero1;
if ctf_zero1 > nyqvist
    error('1st CTF zero > Nyquist!')
end;
if ctf_zero2 > nyqvist
    error('2nd CTF zero > Nyquist!')
end;
if ctf_zero3 > nyqvist
    error('3th CTF zero > Nyquist!')
end;
if ctf_zero4 > nyqvist
    error('4th CTF zero > Nyquist!')
end;

[dum, q1] = min((q-ctf_zero1).^2);
[dum, q2] = min((q-ctf_zero2).^2);
[dum, q3] = min((q-ctf_zero3).^2);
[dum, q4] = min((q-ctf_zero4).^2);
% get zeros accurately
[dum, z1] = min(ctf_sq(floor(q1-.5*(q2-q1)):ceil(q1+.5*(q2-q1)) ));
[dum, z2] = min(ctf_sq(floor(q2-.5*(q3-q2)):ceil(q2+.5*(q3-q2)) ));
[dum, z3] = min(ctf_sq(floor(q3-.5*(q4-q3)):ceil(q3+.5*(q4-q3)) ));
zeropos = [floor(q1-.5*(q2-q1))+z1-1,floor(q2-.5*(q3-q2))+z2-1,floor(q3-.5*(q4-q3))+z3-1];
