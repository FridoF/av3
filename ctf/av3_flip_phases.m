function image = av3_flip_phases(image, pixelsize, tilt_angle, cent_def, tilt_axis)
%
%   image = av3_flip_phases(image, pixelsize, tilt_angle, cent_def, tilt_axis);
%
%  INPUT
%   image       image to be phase-flipped
%   pixelsize   pixelsize in image in nm
%   tilt_angle  tiltangle in degree
%   cent_def    defocus in center of image
%   tilt_axis   tilt axis ('x', 'y', or 'z')
%
%  OUTPUT
%   image       image with phases flipped between 1st and 2nd CTF crossing
%
%  SEE ALSO
%   av3_ctf
%
if ~exist(tilt_axis)
    tilt_axis = 'y';
end;
tilt_angle = tilt_angle * pi/180;
sina = sin(tilt_angle);
tan_inv = sin(tilt_angle)/cos(tilt_angle);
ndim = size(image,1);
fimage = fftshift(tom_fourier(image));

%% compute number of stripes needed
[~, zeropos_cent] = av3_ctf( cent_def, .1, pixelsize, 300., ndim);
[~, zeropos_edge] = av3_ctf( cent_def+ pixelsize/1000*ndim/2*tan_inv, ...
    .1, pixelsize, 300., ndim);
nstripes = abs(zeropos_edge(1)-zeropos_cent(1));
nstripes = nstripes * 2 + 1; %oversample
dpix = ceil(ndim/4/(nstripes-1));

%% pre-compute radii
center=[floor(size(image,1)/2)+1, floor(size(image,2)/2)+1];
[x,y]=ndgrid(0:size(image,1)-1,0:size(image,2)-1);
r = sqrt((x+1-center(1)).^2+(y+1-center(2)).^2);
clear x y;

%% loop over stripes
for istripe=0:nstripes-1
    centpix = round(istripe/(nstripes-1) * ndim /2);
    dz = centpix * pixelsize * tan_inv/1000; % z-difference in mu m
    % is defocus change + or -? test different signs
    loc_def = cent_def + dz; 
    [~, zeropos] = av3_ctf( loc_def, .1, pixelsize, 300., ndim);
    ii = find( (r > zeropos(1)) & (r < zeropos(2)) );
    % flip
    re = real(fimage);
    im = imag(fimage);
    re(ii) = -re(ii);
    im(ii) = -im(ii);
    tmp = tom_ifourier(ifftshift(re + 1i* im));
    xi = max(ndim/2+1+centpix-dpix,1);
    xe = min(ndim/2+1+centpix+dpix,ndim);
    disp(['flip freq ', num2str(xi), ':', num2str(xe)]);
    image(xi:xe,:) = tmp(xi:xe,:);
    % negative side as well
    if istripe > 0
        loc_def = cent_def - dz; 
        [~, zeropos] = av3_ctf( loc_def, .1, pixelsize, 300., ndim);
        ii = find(x > zeropos(1) & x<zeropos(2));
        % flip
        re = real(fimage);
        im = imag(fimage);
        re(ii) = -re(ii);
        im(ii) = -im(ii);
        tmp = tom_ifourier(ifftshift(re + 1i* im));
        xi = max(ndim/2+1-centpix-dpix,1);
        xe = min(ndim/2+1-centpix+dpix,ndim);
        disp(['flip freq ', num2str(xi), ':', num2str(xe)]);
        image(xi:xe,:) = tmp(xi:xe,:);
    end;
end;


