function av3_flip_series( projfile, myext, defocus, pixelsize, first, last, outfile)
%   flip phases in a tilt series taking into account of gradients in images
%
%   av3_flipseries( projname, myext, defocus, pixelsize, first, last)
%
%  INPUT
%   projfile        projection filename (<projfile>_<indx>.<myext>)
%   myext           extension
%   defocus         nominal defocus in center of images
%   pixelsize       pixelsize in image in nm
%   first           index of first projection
%   last            index of last projection
%   outfile         name of phase flipped images
%                   (<outfile>_<indx>.em)
%   
%   04/2011 FF
%
%   Copyright (c) 2005-2011
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
for ii=first:last
    disp([' ... flipping ',projfile, '_', num2str(ii), myext]);
    image = tom_emread([projfile, '_', num2str(ii), myext]);
    tilt_angle = image.Header.Tiltangle;
    image_flip = av3_flip_phases(image.Value, pixelsize, tilt_angle, defocus, 'y');
    image.Value = image_flip;
    tom_emwrite([outfile, '_', num2str(ii), '.em'], image);
end;