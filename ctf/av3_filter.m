function filtered_vol = av3_filter(vol, fil1d)
%   filter volume with 1-d filter
%
%   filtered_vol = av3_filter(vol, fil1d)
%
%   INPUT
%   vol             volume to be filtered (in real space)
%   fil1d           1-dimensional filter in Fourier space
%
%   OUTPUT
%   filtered_vol    3D volume with origin in center
%
npix = max(size(fil1d));
% check that dimensions of filter and volume match
if npix*2 ~= size(vol,1)
    error('Dimensions of vol and fil1d do not match')
end;
if (size(vol,1) ~= size(vol,2)) or (size(vol,2) ~= size(vol,3))
    error('vol is not cubic!')
end;

%% design filter
filtervol = zeros(npix,npix*4,npix*2);
for iphi=1:4*npix
    for ithe=1:2*npix
        filtervol(:,iphi,ithe) = squeeze(fil1d);
    end;
end;
filtervol = tom_sph2cart(filtervol);
filtervol = tom_spheremask(filtervol, npix-1);

%% apply filter to vol
filtered_vol = real(tom_ifourier(tom_fourier(vol).*ifftshift(filtervol)));
