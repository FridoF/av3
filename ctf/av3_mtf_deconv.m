function im_deconv = av3_mtf_deconv(im, psf)
% im_deconv = av3_mtf_deconv(im, psf)
%
% This function deconvolutes an image using the mtf/psf of the (respective)
% CCD. Usage: im        : image
%             psf       : point spread function
%             im_deconv : deconvoluted output image
%
% Prepare psf as follows :
% xxx=tom_rawread('CM300_mtf.dat','float','le',[1 64 1],0,0);
% mcf = zeros(64,128);
% for ii=1:128, mcf(:,ii) = xxx; end;
% mcf_cart = tom_polar2cart(mcf);
% mcf_four = fftshift(mcf_cart);
% psf = fftshift(real(tom_ifourier(mcf_four)));
% psf_2048 = zeros(2048,2048);
% psf_2048(1025-63:1025+64,1025-63:1025+64) = real(psf);
% psf_2048 = fftshift(psf_2048);% final psf for 2K array
% tom_emwrite('psf_CM300_2K.em',psf_2048);
%
% 20.05.05 FF, modyfied by MB
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%

%imdeconv = deconvwnr(im.Value,psf);
mcf_four = abs(tom_fourier(psf));
%mcf_four = ifftshift(tom_spheremask(fftshift(abs(mcf_four)),800));
% low pass inconsistant with tom_limit, see below
%ii = find(abs(mcf_four)>0.005);jj = find(abs(mcf_four)<=0.005);
fim = tom_fourier(im);
fim = (fim./abs(fim)) .* (abs(fim)./(tom_limit(mcf_four,0.17,1)));
%  phase (is image/amp) x     amplitude/MTF
im_deconv = real(tom_ifourier(fim));% tom_imagesc(fim);
%fimfil = tom_bin(tom_bandpass(fim,0,240,5),2);

