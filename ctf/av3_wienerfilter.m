function filter = av3_wienerfilter(ctf, eps)
%   Wiener filter for Amplitude restoration
%
%   filter = av3_wienerfilter(ctf, eps)
%
%   INPUT
%   ctf           theoretical contrast transfer function
%   eps           regularization epsilon - default: 0.1
%
%   OUTPUT
%   filter        finter function in Fourier space
%
%   FF 12/2011
%error(nargchk(0, 4, nargin, 'struct'))
if ~exist('eps','var')
    eps = 0.1;
end;
filter = (abs(ctf)./(abs(ctf).^2+ eps*mean(abs(ctf))));