% Parameter
Objectpixelsize = 0.4712;%nm
Voltage = 300;%kV
Cs = 2.0;%mm
Imdim = 4096;%pixel
ibin = 2; %binning for rec
% fs = 256;%pixel
% gs = 16;%pixel

% Uncorrected projections
PathTS{1} = 'sorted';
NameTS{1} = 'PL_02_sorted_';

% % Corrected projections
% mkdir sorted_ctf
% PathTScor{1} = 'sorted_ctf';
% NameTScor{1} = 'PL_02_sorted_ctf_';
% 
% % Markerfiles
% Markerfiles{1} = 'reconstruction/markerfile.em';
% 
% % Defocus measurement
% 
%     % Load markerfile
%     mf = tom_emreadc3f(Markerfiles{1});
% 
%     % Calculate alignment
%     [alg(1).alg0] = CalculateAlignment(mf,Imdim,2,Imdim,1);
% 
%     % FernandezPS
%     [ps,nof,sw] = FernandezPS(PathTS{1},NameTS{1},alg(1).alg0,128,4,0.4,Objectpixelsize,Imdim);
%     tom_emwrite('ps_ctf.em',ps);
%     ps=tom_emread('ps_ctf.em');ps=ps.Value;
% 
%     % Measure defocus
%     [psf,dz] = InspectDZgui(ps,Objectpixelsize,Voltage,Cs,-5,6,8);
%     %  9.7 // 8, 10
% 
%     % Calculate defocus / projections
%     dzf = ShiftMeanDefocus(dz,Objectpixelsize,Imdim,alg(1).alg0.Tiltangles,alg(1).alg0.Tiltaxis,alg(1).alg0.txgmm,alg(1).alg0.tygmm);
% 
%     
% % Defocus correction
% 
%     % Correct tiltseries
%     CorrectTiltseries(dzf,dzf,zeros(1,size(alg(1).alg0.Tiltangles,2)),alg(1).alg0,PathTS{1},NameTS{1},PathTScor{1},NameTScor{1},gs,fs,Objectpixelsize,Voltage,Cs);
% 
%     % Add tiltangles
%     AddTiltangles(PathTScor{1},NameTScor{1},PathTScor{1},NameTScor{1},alg(1).alg0.Tiltangles);



%% reconstruct particles

 % refine alignment and write optimized projections (unbinned)
mkdir temp

alignopts = av3_alignsetup('finealig',false, ...
    'finealigfile', './newmarker_6del_refined.em', ...
    'dmag',true, 'drot',true, ...
    'dbeam',false, ...
    'grad',false, ...
    'irefmark',1, 'ireftilt',41, 'r', [0,0,0], 'MaxIter',200, 'handflip', true, ...
    'imdim', 4096);
av3_create_weiprojs('../sorted/sorted', '.em', './temp/temp', ...
                .9, 0, './newmarker_6del_refined.em', alignopts, ibin);    
                %.9, 0, './newmarker_6del.em', alignopts, ibin);

vol = av3_rec3dbackproj('./temp/temp_', '.em', 81, [1024 1024 300], [0 0 0 ]);
tom_emwrite('vol_3bin2alit.em', vol);
