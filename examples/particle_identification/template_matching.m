
%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
% first run standalone molmatch, e.g.:
%
% molmatch.exe my_tomogram.em my_template Out ...
%         ... 0 350 10 0 350 10 0 350 10
%         ... psf.em mask-file.em 128

%^ now generate motivelist from correlation volume and average
corrvol = tom_emread('Out.ccf');
anglevol = 'Out.ang';
nparticles = 1000;
radius = 20;
edge = 30;
phi_end = 350.;
psi_end = 350.;
theta_end= 180.;
angular_incr = 10.;
phi_start = 0.;
psi_start = 0.;
theta_start = 0.;
semiangle = 30.;
motivelist = av3_createmotl(corrvol.Value, anglevol, nparticles, radius,  edge ...
       phi_end, psi_end, theta_end, angular_incr, phi_start, psi_start, theta_start);
[average, average_wei] = av3_average_wei(motivelist, 'my_tomogram.em', radius, 'var', 0,semiangle);
tom_emwrite('my_averaged_subvol.em', average_wei)
