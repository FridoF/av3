

%
%   Copyright (c) 2005-2010
%   Max-Planck-Institute for Biochemistry
%   Dept. Molecular Structural Biology
%   82152 Martinsried, Germany
%   http://www.biochem.mpg.de/foerster
%
motl = tom_emread('../../test/motl_test.em');motl=motl.Value;
threshold = -0.01;
wedgelist=zeros(3,1);
wedgelist(1,:)=1:1;
wedgelist(2,:)=-60;
wedgelist(3,:)= 60;
mask=tom_spheremask(ones(64,64,64),22,4);
neig=5;% specify number of eigenvectors
particlename = ['../../test/test_SNR0.01wedge30'];
nclass=5;% number of classes for k-means classification
ibin=1;

[motl_class, ccc, coeff] = av3_classify(motl, particlename, mask, wedgelist, threshold, neig, nclass, ibin);
